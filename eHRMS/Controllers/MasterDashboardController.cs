﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Global;

namespace eHRMS.Controllers
{
    public class MasterDashboardController : Controller
    {
        // GET: MasterDashboard
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.MasterAdmin)]
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}