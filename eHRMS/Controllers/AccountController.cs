﻿using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;
using System.Collections.ObjectModel;

namespace eHRMS.Controllers
{
    public class AccountController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();

        

        [HttpGet]
        public ActionResult Register()
        {            
            return View();
        }

        #region Company Registration
        [HttpPost]
        public ActionResult Register(CompanyRegistration fc)
        {
            try
            {
                fc.IsAgree = true;
                if (ModelState.IsValid)
                {
                    if (!context.tbl_LoginContact.Any(x => x.Email.Trim() == fc.Email.Trim()))
                    {
                        if (fc.Password != fc.Confirm_Password)
                        {
                            ViewBag.ErrorMsg = "Passwords did not match !";
                        }
                        else
                        {
                            if (SaveDetails(fc))
                            {
                                ViewBag.SuccessMsg = "Thank you for registering. Check email for login details !";
                            }
                            else
                            {
                                ViewBag.ErrorMsg = "Inetrnal error !";
                            }
                        }
                        return View();
                    }

                    else
                    {
                        ViewBag.ErrorMsg = "This email already registered with us !";
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }
        #endregion

        #region Saving Registration details and send Mail
        private bool SaveDetails(CompanyRegistration fc)
        {
            using (DbContextTransaction dbTran = context.Database.BeginTransaction())
            {
                try
                {
                    string CompanyRegistrationID = System.Guid.NewGuid().ToString().ToUpper();
                    tbl_CompanyRegistration objComp = new tbl_CompanyRegistration()
                    {
                        CompanyRegistrationID = CompanyRegistrationID,
                        CompanyName = fc.CompanyName,
                        IsAgree = fc.IsAgree,
                        IsActive = true,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = fc.Email
                    };
                    context.tbl_CompanyRegistration.Add(objComp);
                    context.SaveChanges();

                    CompanyRegistrationID = objComp.CompanyRegistrationID;

                    tbl_Login objLogin = new tbl_Login()
                    {

                        LoginID = System.Guid.NewGuid().ToString().ToUpper(),
                        Username = fc.Email,
                        Password = objglobal.Encrypt(fc.Password),
                        UserRole = Convert.ToInt32(AppsConstants.UserRole.Admin),
                        IsActive = true,
                        FK_CompanyRegistrationID = CompanyRegistrationID,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = fc.Email
                    };
                    context.tbl_Login.Add(objLogin);
                    context.SaveChanges();

                    string LoginID = objLogin.LoginID;

                    tbl_LoginContact objLoginContact = new tbl_LoginContact()
                    {
                        LoginContactID = System.Guid.NewGuid().ToString().ToUpper(),
                        ContactPerson = fc.ContactPerson,
                        Email = fc.Email,
                        IsActive = true,
                        FK_LoginID = LoginID,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = fc.Email
                    };
                    context.tbl_LoginContact.Add(objLoginContact);
                    context.SaveChanges();

                    tbl_Subscription objsub = new tbl_Subscription()
                    {
                        ActivatedOn = System.DateTime.Now,
                        ExpiredDate = System.DateTime.Now.AddDays(7),
                        NoOfUser = 1,
                        UserLimit = 3,
                        SubscriptionType = Convert.ToInt32(AppsConstants.SubscriptionType.Free),
                        FK_CompanyRegistrationID = CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = fc.Email
                    };
                    context.tbl_Subscription.Add(objsub);
                    context.SaveChanges();
                    // Send Email for Login Details
                    string FK_CompanyRegistrationID = null;
                    objglobal.SendMailRegistration("eHRMS-Registration Successful", fc.ContactPerson, fc.Email, fc.Email, fc.Confirm_Password, FK_CompanyRegistrationID);
                    dbTran.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    objError.LogError(ex);
                    return false;
                }
            }
        }
        #endregion

        [HttpGet]
        public ActionResult SignIn()
        {
            Session["User_ID"] = null;
            Session["ContactPerson"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Session.Abandon();
            return View();
        }

        #region Login
        [HttpPost]
        public ActionResult SignIn(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                objglobal.Decrypt("+2LMDtndIpCUbol8O59L9ypGT8Iq9+gvbMPfE23siko=");
                try
                {
                    tbl_Login LoginTbl = AuthenticateUser(model.Username, objglobal.Encrypt(model.Password));
                    if (LoginTbl != null)
                    {
                        model.Password = null;
                        model.LoginID = LoginTbl.LoginID;
                        model.Username = LoginTbl.Username;
                        model.UserRole = Convert.ToInt32(LoginTbl.UserRole);
                        
                        model.FK_CompanyRegistrationID = LoginTbl.FK_CompanyRegistrationID;
                        
                        if (LoginTbl.tbl_LoginContact.Any(x => x.FK_LoginID == LoginTbl.LoginID))
                        {
                            model.ContactPerson = LoginTbl.tbl_LoginContact.Where(x => x.FK_LoginID == LoginTbl.LoginID).FirstOrDefault().ContactPerson.ToUpper();
                            model.IsActive = true;
                        }
                        //Loged User Details
                        Session["User_ID"] = model.Username;
                        Session["ContactPerson"] = model.ContactPerson;                                                
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "Username/Password is incorrect or You are not an active user !";
                        return View(model);
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Sorry! Please try again later !";
                    return View(model);
                }
                return GetUserAuthentication(model);
            }
            else
            {
                ViewBag.ErrorMsg = "Please enter your Username / Password";
                return View(model);
            }
        }
        private tbl_Login AuthenticateUser(string Username, string Password)
        {
            var result = context.tbl_Login.Where(x => x.IsActive == true
                                            && (x.Username.Trim().ToUpper() == Username.Trim().ToUpper() || x.tbl_LoginContact.Where(w => w.Email == Username.Trim()).Select(r => r.Email).FirstOrDefault().Trim() == Username)
                                            && (x.Password.Trim().ToUpper() == Password.Trim().ToUpper())).FirstOrDefault();
            return result;
        }

        private ActionResult GetUserAuthentication(LoginModel model)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            1, // Ticket version
            model.ContactPerson,// Username to be associated with this ticket
            DateTime.Now, // Date/time ticket was issued
            DateTime.Now.AddHours(5), // Date and time the cookie will expire
            false, // if user has chcked rememebr me then create persistent cookie
            model.Username.ToString(), // store the user data, in this case userId of the user
            FormsAuthentication.FormsCookiePath); // Cookie path specified in the web.config file in <Forms> tag if any. 
            // To give more security it is suggested to hash it
            string hashCookies = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket 
            Session[SessionConstants.UserProfileObject] = model;
            // Add the cookie to the response, user browser 
            Response.Cookies.Add(cookie);
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            if (model.UserRole == Convert.ToDecimal(AppsConstants.UserRole.MasterAdmin))
            {
                return RedirectToAction("Dashboard", "MasterDashboard", new { area = "" });
            }
            else if (model.UserRole == Convert.ToDecimal(AppsConstants.UserRole.SuperAdmin))
            {
                return RedirectToAction("Dashboard", "MasterDashboard", new { area = "" });
            }
            else if (model.UserRole == Convert.ToDecimal(AppsConstants.UserRole.Admin))
            {
                return RedirectToAction("Dashboard", "AdminDashboard", new { area = "Admin" });
            }
            else if (model.UserRole == Convert.ToDecimal(AppsConstants.UserRole.HR))
            {
                return RedirectToAction("Dashboard", "HRDashboard", new { area = "HR" });
            }
            else
            {
                return RedirectToAction("Dashboard", "EmployeeDashboard", new { area = "Employee" });
            }
        }
        #endregion

        [HttpGet]
        public ActionResult Forgot_Password()
        {
            return View();
        }

        #region Forgot Password and send Mail
        [HttpPost]
        public ActionResult Forgot_Password(ForgotPassword fc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (context.tbl_LoginContact.Any(x => x.Email.Trim() == fc.Email.Trim() && x.IsActive == true))
                    {
                        ForgotPassword objfrgtpass = new ForgotPassword();

                        var frgtPass = (from LC in context.tbl_LoginContact
                                        join L in context.tbl_Login on LC.FK_LoginID equals L.LoginID
                                        where LC.Email == fc.Email && L.IsActive == true
                                        select new { LC.ContactPerson, LC.Email, L.Username, L.Password, L.FK_CompanyRegistrationID }).ToList();
                        if (frgtPass.Count > 0)
                        {
                            foreach (var item in frgtPass)
                            {
                                // Send Email for Login Details
                                string FK_CompanyRegistrationID = null;
                                objglobal.SendMailForgotPassword("eHRMS-Forgot Password", item.ContactPerson, item.Email, item.Username, objglobal.Decrypt(item.Password), FK_CompanyRegistrationID);
                            }
                            ViewBag.SuccessMsg = "Check your email for login details !";
                        }
                        else
                        {
                            ViewBag.ErrorMsg = "Sorry, you are not an active user ! ";
                        }
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "This email is not registered with us !";
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Required field cannot be blank !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }
        #endregion


        public ActionResult ChangePassword()
        {
            return View();
        }

        #region LogOut
        [HttpGet]
        public ActionResult LogOut()
        {
            Session["User_ID"] = null;
            Session["ContactPerson"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Session.Abandon();

            return RedirectToAction("SignIn", "Account", new { area = "" });
        }
        #endregion
    }
}