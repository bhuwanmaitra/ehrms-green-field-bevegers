﻿using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Net.Security;
using eHRMS.Areas.HR.Models;
using Newtonsoft.Json;
using eHRMS.Areas.Admin.Models;
using System.Globalization;


namespace eHRMS.Global
{
    public class GlobalFunction
    {
        eHRMSEntities context = new eHRMSEntities();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        public static string constr = @"Data Source=ITSERVER\EKESTO;Initial Catalog=GeenFizzBeverages;User Id=sa;password=Gabbar@98765";

        public string JsonSerialize(object ListItem)
        {
            return JsonConvert.SerializeObject(ListItem);
        }

        public string getTimeZone()
        {
            SubscriptionModel objSubs = GetSubscriptionDetails();
            //var tbl_Emp_Joining_Probation = context.tbl_Emp_Joining_Probation.FirstOrDefault<tbl_Emp_Joining_Probation>(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true);
            var tbl_CompanyLocation = context.tbl_CompanyLocation.FirstOrDefault(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);

            if (tbl_CompanyLocation == null)
            {
                return null;
            }
            return tbl_CompanyLocation.TimeZone;
        }

        public static List<int> getSundays(int year, int month)
        {

            CultureInfo ci = new CultureInfo("en-US");

            DayOfWeek dayname = DayOfWeek.Sunday;
            List<int> lstSundays = new List<int>();
            for (int i = 1; i <= ci.Calendar.GetDaysInMonth(year, month); i++)
            {
                if (new DateTime(year, month, i).DayOfWeek == dayname)
                    lstSundays.Add(i);
            }

            return lstSundays;

        }

        public SubscriptionModel GetSubscriptionDetails()
        {
            LoginModel objLogin = GetLoginDetails();
            SubscriptionModel SubsTbl = new SubscriptionModel();
            if (objLogin != null)
            {
                try
                {
                    SubsTbl = context.tbl_Subscription.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID).Select(s => new SubscriptionModel
                    {
                        Username = objLogin.Username,
                        CompanyName = context.tbl_CompanyRegistration.Where(x => x.IsActive == true && x.CompanyRegistrationID == objLogin.FK_CompanyRegistrationID).FirstOrDefault().CompanyName,
                        UserRole = objLogin.UserRole,
                        SubscriptionID = s.SubscriptionID,
                        ActivatedOn = s.ActivatedOn,
                        ExpiredDate = s.ExpiredDate,
                        NoOfUser = s.NoOfUser,
                        UserLimit = s.UserLimit,
                        SubscriptionType = s.SubscriptionType,
                        FK_CompanyRegistrationID = s.FK_CompanyRegistrationID,
                        IsActive = s.IsActive,
                    }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                }
            }
            return SubsTbl;
        }


        public ItemForTAXComputation GetITDetails(string Employee_Code, string FK_PackageID, string CTC)
        {
            LoginModel objLogin = GetLoginDetails();
            List<SlabMaster> Slab_List = new List<SlabMaster>();
            tbl_SlabMaster objSlab = new tbl_SlabMaster();
            List<PaySlipTemplate> Salary_Cmp_List = new List<PaySlipTemplate>();
            List<EmployeeInvestment> Emp_Invest_List = new List<EmployeeInvestment>();
            ItemForTAXComputation ITSetdetails = new ItemForTAXComputation();
            int ctc = Convert.ToInt32(CTC);

            if (objLogin != null)
            {
                try
                {
                    // Employee Basic Details for AGE and SEX 
                    var BasicResult = context.tbl_Emp_Contact_Basic.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == Employee_Code).FirstOrDefault();
                    // All IT Slab Details List
                    Slab_List = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID).Select(s => new SlabMaster
                    {
                        SlabMasterID = s.SlabMasterID,
                        IncomeFrom = s.IncomeFrom,
                        IncomeTo = s.IncomeTo,
                        TAX = s.TAX,
                        Category = s.Category,
                    }).ToList();
                    // Employee Age
                    var AGE = (System.DateTime.Now.Year) - (BasicResult.DOB.Value.Year);
                    // Selecting the Suitable SlabMaster for Employee
                    foreach (var item in Slab_List)
                    {
                        int SlabMasterID = Convert.ToInt32(item.SlabMasterID);
                        int incomefrom = Convert.ToInt32(item.IncomeFrom);
                        int incometo = Convert.ToInt32(item.IncomeTo);
                        if ((AGE >= 60 && AGE <= 80) && (ctc >= incomefrom && ctc <= incometo))
                        {
                            //if (ctc >= incomefrom && ctc <= incometo)
                            //{
                            objSlab = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Category == "Senior Citizen (60-80 years)" && x.SlabMasterID == SlabMasterID).FirstOrDefault();
                            //}
                        }
                        else if (AGE > 80 && (ctc >= incomefrom && ctc <= incometo))
                        {
                            //if (ctc >= incomefrom && ctc <= incometo)
                            //{
                            objSlab = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Category == "Senior Citizen (Above 80 years)" && x.SlabMasterID == SlabMasterID).FirstOrDefault();
                            //}
                        }
                        else if (BasicResult.Sex == "Male" && (ctc >= incomefrom && ctc <= incometo))
                        {
                            //if (ctc >= incomefrom && ctc <= incometo)
                            //{
                            objSlab = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Category == "Others" && x.SlabMasterID == SlabMasterID).FirstOrDefault();
                            //}
                        }
                        else if (ctc >= incomefrom && ctc <= incometo)
                        {
                            //if (ctc >= incomefrom && ctc <= incometo)
                            //{
                            objSlab = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Category == "Woman" && x.SlabMasterID == SlabMasterID).FirstOrDefault();
                            //}
                        }
                    }
                    // Employee All Salary Components List for ComponentID
                    Salary_Cmp_List = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.ComponentType == "Earning" && x.PackageID == FK_PackageID).Select(s => new PaySlipTemplate
                    {
                        FK_TypeID = s.FK_TypeID,
                    }).ToList();
                    // Sum of All Earnings and Taxable Components of the Employee
                    foreach (var item in Salary_Cmp_List)
                    {
                        int type = Convert.ToInt32(item.FK_TypeID);
                        var prcntg = context.tbl_SlaryComponents.Where(x => x.TypeID == type && x.IsTaxable == true).Select(s => s.Percentage).FirstOrDefault();
                        ITSetdetails.ComponentTotalPercentage += Convert.ToInt32(prcntg);
                    }
                    // Investment of Employee
                    Emp_Invest_List = context.tbl_Employee_Investment.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == Employee_Code).Select(s => new EmployeeInvestment
                    {
                        Public_Provident_Fund_PPF_80C = s.Public_Provident_Fund_PPF_80C,
                        Life_Insurance_LIC_80C = s.Life_Insurance_LIC_80C,
                        National_Savings_Certificate_Purchased_NSC_80C = s.National_Savings_Certificate_Purchased_NSC_80C,
                        Unit_Linked_Insurance_Plan_ULIP_80C = s.Unit_Linked_Insurance_Plan_ULIP_80C,
                        Mutual_Funds_Notified_US_10_23D = s.Mutual_Funds_Notified_US_10_23D,
                        Contribution_To_Pension_Fund_80C = s.Contribution_To_Pension_Fund_80C,
                        Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C = s.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C,
                        Senior_Citizens_Savings_Scheme_80C = s.Senior_Citizens_Savings_Scheme_80C,
                        Equity_Linked_Savings_Scheme_ELSS_80C = s.Equity_Linked_Savings_Scheme_ELSS_80C,
                        Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C = s.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C,
                        Five_Years_Of_Post_Office_Deposits_80C = s.Five_Years_Of_Post_Office_Deposits_80C,
                        Education_Tution_Fee_Upto_Two_Children_80C = s.Education_Tution_Fee_Upto_Two_Children_80C,
                        Rajiv_Gandhi_Equity_Saving_Schemes = s.Rajiv_Gandhi_Equity_Saving_Schemes,
                        Medical_Insurance_Premium_Individual_Spouse_And_Children = s.Medical_Insurance_Premium_Individual_Spouse_And_Children,
                        Medical_Insurance_Premium_Parents = s.Medical_Insurance_Premium_Parents,
                        Preventive_Health_Check_Up = s.Preventive_Health_Check_Up,
                        Medical_Treatment_For_Dependent_Handicap = s.Medical_Treatment_For_Dependent_Handicap,
                        Medical_Treatment_For_Specified_Disease = s.Medical_Treatment_For_Specified_Disease,
                        Medical_Treatment_For_Specified_Disease_For_Senior_Citizen = s.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen,
                        Permanent_Physical_Disability = s.Permanent_Physical_Disability,
                        Interest_On_Education_Loan = s.Interest_On_Education_Loan,
                        Interest_On_Housing_Loan = s.Interest_On_Housing_Loan,
                    }).ToList();
                    // Sum of all Investment of the Employee
                    foreach (var item in Emp_Invest_List)
                    {
                        ITSetdetails.Investment += Convert.ToInt32(item.Public_Provident_Fund_PPF_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Life_Insurance_LIC_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.National_Savings_Certificate_Purchased_NSC_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Unit_Linked_Insurance_Plan_ULIP_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Mutual_Funds_Notified_US_10_23D);
                        ITSetdetails.Investment += Convert.ToInt32(item.Contribution_To_Pension_Fund_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Senior_Citizens_Savings_Scheme_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Equity_Linked_Savings_Scheme_ELSS_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Five_Years_Of_Post_Office_Deposits_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Education_Tution_Fee_Upto_Two_Children_80C);
                        ITSetdetails.Investment += Convert.ToInt32(item.Rajiv_Gandhi_Equity_Saving_Schemes);
                        ITSetdetails.Investment += Convert.ToInt32(item.Medical_Insurance_Premium_Individual_Spouse_And_Children);
                        ITSetdetails.Investment += Convert.ToInt32(item.Medical_Insurance_Premium_Parents);
                        ITSetdetails.Investment += Convert.ToInt32(item.Preventive_Health_Check_Up);
                        ITSetdetails.Investment += Convert.ToInt32(item.Medical_Treatment_For_Dependent_Handicap);
                        ITSetdetails.Investment += Convert.ToInt32(item.Medical_Treatment_For_Specified_Disease);
                        ITSetdetails.Investment += Convert.ToInt32(item.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen);
                        ITSetdetails.Investment += Convert.ToInt32(item.Permanent_Physical_Disability);
                        ITSetdetails.Investment += Convert.ToInt32(item.Interest_On_Education_Loan);
                        ITSetdetails.Investment += Convert.ToInt32(item.Interest_On_Housing_Loan);
                    }
                    if (ITSetdetails.Investment > 150000)
                    {
                        ITSetdetails.Investment = 150000;
                    }
                    // Other Details for ItemForTAXComputation
                    //ITSetdetails.TaxableAmount = (((Convert.ToInt32(CTC) * ITSetdetails.ITPercentage)-ITSetdetails.Investment)*Convert.ToInt32(objSlab.TAX)) / 100;
                    ITSetdetails.FK_SlabMasterID = objSlab.SlabMasterID;
                    ITSetdetails.TaxableAmount = (Convert.ToInt32(CTC) * ITSetdetails.ComponentTotalPercentage) / 100;
                    ITSetdetails.ITPercentage = Convert.ToInt32(objSlab.TAX);
                    ITSetdetails.YearlyTAX = ((ITSetdetails.TaxableAmount - ITSetdetails.Investment) * ITSetdetails.ITPercentage) / 100;
                    ITSetdetails.MonthlyTAX = ITSetdetails.YearlyTAX / 12;
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                }
            }
            return ITSetdetails;
        }



        public string NumberToWords(int number)
        {
            if (number == 0)
                return "ZERO";

            if (number < 0)
                return "MINUS " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 10000000) > 0)
            {
                words += NumberToWords(number / 10000000) + " CRORE ";
                number %= 10000000;
            }

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " LAKH ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " HUNDRED ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "AND ";

                var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
                var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }


        public ItemForTAXComputation UpdateInvestmentDetails(tbl_TAXComputation objTAX)
        {
            ItemForTAXComputation ITSetdetails = new ItemForTAXComputation();

            return ITSetdetails;
        }


        public Emp_Details GetEmployeeDetails()
        {
            LoginModel objLogin = GetLoginDetails();
            Emp_Details SubsTbl = new Emp_Details();
            if (objLogin != null)
            {
                try
                {

                    #region old code
                    //SubsTbl = context.tbl_Emp_Contact_Basic.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Email == objLogin.Username).Select(s => new Emp_Details
                    //{
                    //    Name = s.Name,
                    //    Email = s.Email,
                    //    Number = s.Number,
                    //    DOB = s.DOB,
                    //    Sex = s.Sex,
                    //    UserImagePath = context.tbl_UserDetails.Where(x => x.FK_LoginID == objLogin.LoginID && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.IsActive == true).FirstOrDefault().UserImagePath,
                    //    Employee_Code = s.Employee_Code,
                    //    FK_CompanyRegistrationID = objLogin.FK_CompanyRegistrationID,
                    //    JoiningDate = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().JoiningDate,
                    //    ResignDate = context.tbl_Emp_Resign.Where(x => x.IsAccepted == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().ResignDate,
                    //    FK_ProbationPolicyID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().FK_ProbationPolicyID,
                    //    DepartmentID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().FK_DepartmentID,
                    //    Department = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Department,
                    //    Category = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Category,
                    //    Grade = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Grade,
                    //    CTC = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().CTC,
                    //    Skills = context.tbl_Emp_Skill_Employment.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Skills,
                    //    OfficeLocationID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().FK_OfficeLocationID,
                    //    TimeZone = context.tbl_CompanyLocation.Where(x => x.OfficeLocationID == (context.tbl_Emp_Joining_Probation.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && w.Employee_Code == s.Employee_Code).FirstOrDefault().FK_OfficeLocationID)).FirstOrDefault().TimeZone,
                    //    //NoOfMadicalLeave = context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Department == SubsTbl.Department && x.Type == "Medical Leave").FirstOrDefault().NoOfLeave,
                    //    //CanApplyMadicalLeave = context.tbl_Leave_Category.Where(x => x.IsActive==true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.ApplyTwo == "Medical Leave").FirstOrDefault().ApplyTwo_After,
                    //    //NoOfCasualLeave = context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Department == SubsTbl.Department && x.Type == "Casual Leave").FirstOrDefault().NoOfLeave,
                    //    //CanApplyCasualLeave = context.tbl_Settings_AttLeave.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.ApplyOne == "Casual Leave").FirstOrDefault().ApplyOne_After,
                    //    //ResignPolicyID = context.tbl_JobLeavePolicy.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.FK_DepartmentID == SubsTbl.DepartmentID).FirstOrDefault().LeavePolicyID,
                    //    facebooklink = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Facebooklink,
                    //    twitterlink = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Twitterlink,
                    //    linkedinlink = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Linkedinlink,
                    //    googlelink = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code).FirstOrDefault().Googlelink,
                    //    IsActive = s.IsActive,
                    //    //   IsAppraiser =  context.tbl_Emp_Joining_Probation.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == s.Employee_Code ).FirstOrDefault().IsAppraiser
                    //}).FirstOrDefault(); 
                    #endregion


                    var tbl_Emp_Contact_Basic = context.tbl_Emp_Contact_Basic.FirstOrDefault(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Email == objLogin.Username);
                    if (tbl_Emp_Contact_Basic != null)
                    {
                        SubsTbl.Name = tbl_Emp_Contact_Basic.Name;
                        SubsTbl.Email = tbl_Emp_Contact_Basic.Email;
                        SubsTbl.Number = tbl_Emp_Contact_Basic.Number;
                        SubsTbl.DOB = tbl_Emp_Contact_Basic.DOB;
                        SubsTbl.Sex = tbl_Emp_Contact_Basic.Sex;
                        SubsTbl.IsActive = tbl_Emp_Contact_Basic.IsActive;
                        SubsTbl.Employee_Code = tbl_Emp_Contact_Basic.Employee_Code;

                    }
                    var tbl_UserDetails = context.tbl_UserDetails.Where(x => x.FK_LoginID == objLogin.LoginID && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.IsActive == true).FirstOrDefault();
                    if (tbl_UserDetails != null)
                    {
                        SubsTbl.UserImagePath = tbl_UserDetails.UserImagePath;
                        SubsTbl.FK_CompanyRegistrationID = objLogin.FK_CompanyRegistrationID;
                    }
                    var tbl_Emp_Joining_Probation = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault();
                    if (tbl_Emp_Joining_Probation != null)
                    {
                        SubsTbl.JoiningDate = tbl_Emp_Joining_Probation.JoiningDate;
                        SubsTbl.FK_ProbationPolicyID = tbl_Emp_Joining_Probation.FK_ProbationPolicyID;
                        SubsTbl.DepartmentID = tbl_Emp_Joining_Probation.FK_DepartmentID;
                        SubsTbl.Department = tbl_Emp_Joining_Probation.Department;
                        SubsTbl.Category = tbl_Emp_Joining_Probation.Category;
                        SubsTbl.Grade = tbl_Emp_Joining_Probation.Grade;
                        SubsTbl.OfficeLocationID = tbl_Emp_Joining_Probation.FK_OfficeLocationID;
                        SubsTbl.IsAppraiser = tbl_Emp_Joining_Probation.IsAppraiser;

                    }
                    var tbl_Emp_Resign = context.tbl_Emp_Resign.Where(x => x.IsAccepted == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault();
                    if (tbl_Emp_Resign != null)
                    {
                        SubsTbl.ResignDate = tbl_Emp_Resign.ResignDate;
                    }
                    var tbl_Emp_Salary = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault();
                    if (tbl_Emp_Salary != null)
                    {
                        SubsTbl.CTC = tbl_Emp_Salary.CTC;
                    }
                    var tbl_Emp_Skill_Employment = context.tbl_Emp_Skill_Employment.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault();
                    if (tbl_Emp_Skill_Employment != null)
                    {
                        SubsTbl.CTC = tbl_Emp_Skill_Employment.Skills;
                    }

                    var tbl_CompanyLocation = context.tbl_CompanyLocation.Where(x => x.OfficeLocationID == (context.tbl_Emp_Joining_Probation.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && w.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault().FK_OfficeLocationID)).FirstOrDefault();
                    if (tbl_CompanyLocation != null)
                    {
                        SubsTbl.TimeZone = tbl_CompanyLocation.TimeZone;
                    }
                    var tbl_EmpSocialLink = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objLogin.FK_CompanyRegistrationID && x.Employee_Code == tbl_Emp_Contact_Basic.Employee_Code).FirstOrDefault();
                    if (tbl_EmpSocialLink != null)
                    {
                        SubsTbl.facebooklink = tbl_EmpSocialLink.Facebooklink;
                        SubsTbl.twitterlink = tbl_EmpSocialLink.Twitterlink;
                        SubsTbl.linkedinlink = tbl_EmpSocialLink.Linkedinlink;
                        SubsTbl.googlelink = tbl_EmpSocialLink.Googlelink;
                    }



                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                }
            }

            return SubsTbl;
        }

        //public AttLeave_Settings AttLeaveStngsDetails()
        //{
        //    LoginModel objLogin = GetLoginDetails();
        //    AttLeave_Settings SubsTbl = new AttLeave_Settings();
        //    if (objLogin != null)
        //    {
        //        try
        //        {
        //            SubsTbl = context.tbl_Settings_AttLeave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new AttLeave_Settings
        //            {
        //                ID = s.ID,
        //                LateEntryLimit = s.LateEntryLimit,
        //                LimitFor = s.LimitFor,
        //                ApplyOne = s.ApplyOne,
        //                ApplyOne_After = s.ApplyOne_After,
        //                ApplyTwo = s.ApplyTwo,
        //                ApplyTwo_After = s.ApplyTwo_After,
        //                Working_HoursLimit = s.Working_HoursLimit,
        //                Working_HoursLimit_For = s.Working_HoursLimit_For
        //            }).FirstOrDefault();
        //        }
        //        catch (Exception ex)
        //        {
        //            objError.LogError(ex);
        //        }
        //    }
        //    return SubsTbl;
        //}


        public LoginModel GetLoginDetails()
        {
            LoginModel LoginTbl = new LoginModel();
            LoginTbl = (LoginModel)HttpContext.Current.Session[SessionConstants.UserProfileObject];
            return LoginTbl;
        }


        public DateTime TimeZone(string timezone)
        {
            DateTime serverTime = DateTime.Now; // gives you current Time in server timeZone
            DateTime utcTime = serverTime.ToUniversalTime(); // convert it to Utc using timezone setting of server computer

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);

            return localTime;
        }



        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public string Decrypt(string cipherText)
        {
            string DecryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes decryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(DecryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                decryptor.Key = pdb.GetBytes(32);
                decryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, decryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        private string PopulateBody(int TempId, string fullname, string uname, string pwd)
        {
            var TempPath = context.tbl_EmailTemplate.Where(x => x.ID == TempId).FirstOrDefault();
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(TempPath.TemplatePath.ToString())))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{eName}", fullname);
            body = body.Replace("{UserName}", uname);
            body = body.Replace("{Pwd}", pwd);
            return body;
        }
        public void SendMailRegistration(string Subject, string Fullname, string MailTo, string Username, string Password, string FK_CompanyRegistrationID)
        {
            try
            {
                tbl_EmailSettings objSetting = context.tbl_EmailSettings.Where(x => x.FK_CompanyRegistrationID == FK_CompanyRegistrationID).FirstOrDefault();
                if (objSetting == null)
                {
                    objSetting = context.tbl_EmailSettings.Where(x => x.FK_CompanyRegistrationID == null).FirstOrDefault();
                }
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(objSetting.SMTPServer.ToString(), Convert.ToInt32(objSetting.SMTPPort));
                SmtpServer.Port = Convert.ToInt32(objSetting.SMTPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(objSetting.UserID, objSetting.Password);
                SmtpServer.EnableSsl = false;
                mail.From = new MailAddress(objSetting.FromEmail);
                var toAddress = MailTo;
                mail.To.Add(toAddress);
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                string fullname = Fullname;
                string un = Username;
                string pw = Password;
                string htmlBody;
                htmlBody = PopulateBody(Convert.ToInt32(1), fullname, un, pw);
                mail.Body = htmlBody;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }

        public void SendMailForgotPassword(string Subject, string Fullname, string MailTo, string Username, string Password, string FK_CompanyRegistrationID)
        {
            try
            {
                tbl_EmailSettings objSetting = context.tbl_EmailSettings.Where(x => x.FK_CompanyRegistrationID == FK_CompanyRegistrationID).FirstOrDefault();
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(objSetting.SMTPServer.ToString(), Convert.ToInt32(objSetting.SMTPPort));
                SmtpServer.Port = Convert.ToInt32(objSetting.SMTPPort);
                SmtpServer.Credentials = new System.Net.NetworkCredential(objSetting.UserID, objSetting.Password);
                SmtpServer.EnableSsl = false;
                mail.From = new MailAddress(objSetting.FromEmail);
                var toAddress = MailTo;
                mail.To.Add(toAddress);
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                string fullname = Fullname;
                string un = Username;
                string pw = Password;
                string htmlBody;
                htmlBody = PopulateBody(Convert.ToInt32(2), fullname, un, pw);
                mail.Body = htmlBody;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }
        //dbConnection cn = new dbConnection();
        //public void SendMail(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath)
        //{
        //    //bool result = false;
        //    try
        //    {

        //        //MailMessage mail = new MailMessage(FromMail, ToMail, Subject, Body);
        //        //System.Net.NetworkCredential auth = new System.Net.NetworkCredential("mailto:rrdev1987@gmail.com%22,%20%22srivastava1987", "srivastava1987");
        //        //SmtpClient client = new SmtpClient("gmail.com", 587);
        //        //client.EnableSsl = false;
        //        //client.UseDefaultCredentials = false;
        //        //mail.IsBodyHtml = true;
        //        //client.Credentials = auth;
        //        //client.Send(mail);


        //        MailMessage mail = new MailMessage();
        //        //put your SMTP address and port here.
        //        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
        //        //Put the email address
        //        mail.From = new MailAddress(FromMail);
        //        //Put the email where you want to send.
        //        mail.To.Add(ToMail);

        //        mail.Subject = Subject;

        //        mail.Body = Body;

        //        if (AttachmentPath != "")
        //        {
        //            //Your log file path
        //            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

        //            mail.Attachments.Add(attachment);
        //        }

        //        //Your username and password!
        //        SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
        //        //Set Smtp Server port
        //        SmtpServer.Port = 25;
        //        //SmtpServer.EnableSsl = true;

        //        SmtpServer.Send(mail);
        //        //MessageBox.Show("Mail sent! :)");

        //        //result = true;

        //        //return result;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //   // return result;
        //}



        //public void SendingMail( string ToMail)
        //{
        //   try
        //    {
        //        //string ToMail;

        //        //Create Mail Message for sending Activation Link to Active the Account
        //        MailMessage objMailMessage = new MailMessage();

        //        objMailMessage.From = new MailAddress(ConfigurationManager.AppSettings["from"].ToString(), "webmaster");

        //        objMailMessage.To.Add(new MailAddress(ToMail));

        //        objMailMessage.Subject = "ActivationLink::UserRegistration";




        //        StringBuilder sbMailBody = new StringBuilder();

        //        sbMailBody.Append("<table style='width:100%';borde:5px solid blue>");

        //        //sbMailBody.Append("<tr><td><img src='" + ConfigurationManager.AppSettings["InternetUrl"].ToString() + "Images/Ads1.jpg' width='100%' height='150' alt='Image'/></td> </tr>");

        //        sbMailBody.Append("<tr><td><b>Dear User,</b><br/></td></tr>");

        //        sbMailBody.Append("<tr><td><p style='text-align:justify'><b>Greeting from Web User Master</b><br/>Thanks You for Registring with us!!!<br/><br/>Please Click below link to Active the Account in order to use our services:</p></td></tr>");

        //        //sbMailBody.Append("<tr><td><a href='" + ConfigurationManager.AppSettings["InternetUrl"].ToString() + "ActivateAccount.aspx?eml=" + txtEmail.Text.Trim() + "'target='_blank'>" + ConfigurationManager.AppSettings["InternetUrl"].ToString() + "ActivateAccount.aspx?eml=" + txtEmail.Text.Trim() + "</a></td><br/><br/><br/></tr>");

        //        sbMailBody.Append("<tr><td><b>Thanks & Best Regards,</b><br/>Management Team<br/>Web Master</td></tr>");

        //        sbMailBody.Append("</table>");

        //        objMailMessage.Body = sbMailBody.ToString();
        //        objMailMessage.IsBodyHtml = true;
        //        objMailMessage.Priority = MailPriority.High;



        //        SmtpClient objSmtpClint = new SmtpClient();
        //        objSmtpClint.Host = ConfigurationManager.AppSettings["Host"].ToString();
        //        objSmtpClint.Port = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
        //        objSmtpClint.Send(objMailMessage);


        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //}



        //public bool CheckIfExistsOneParamWithoutDelFlag(string TableName, string ColumnName, string ColumnValue)
        //{
        //    bool result = false;
        //    try
        //    {
        //        cn.con.Open();

        //        //string sqlstr = "select count(*) as no from " + TableName + " where " + ColumnName + "='" + ColumnValue + "'";
        //        string sqlstr = "select count(*) as no from " + TableName + " where " + ColumnName + "='" + ColumnValue + "' ";
        //        //NpgsqlCommand cmd = new NpgsqlCommand(sqlstr, GlobalVariables.gConn);
        //        //NpgsqlDataReader sdr = cmd.ExecuteReader();
        //        cn.cmd = new SqlCommand(sqlstr, cn.con);

        //        dr = cn.cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            if (Convert.ToInt32(dr["no"].ToString()) > 0)
        //            {
        //                result = true;
        //            }
        //        }
        //        cn.con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Global.ErrorHandlerClass.LogError(ex);
        //    }

        //    return result;
        //}
    }
}