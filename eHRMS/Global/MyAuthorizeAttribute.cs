﻿using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;

namespace eHRMS.Global
{
    public class SessionAuthorizeFilter : ActionFilterAttribute
    {
        private readonly int _UserRole;
        public SessionAuthorizeFilter(int UserRole)
        {
            this._UserRole = UserRole;
        }
        //public string UserRole { get; set; }
        //public SessionAuthorizeFilter(string roles)
        //{            
        //    this.UserRole = roles;
        //}        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //&& System.DateTime.Now>=context.tbl_Emp_Resign.Where(m=>m.Employee_Code==objEmp.Employee_Code).FirstOrDefault().ResignDate
            
            eHRMSEntities context = new eHRMSEntities();
            GlobalFunction objglobal = new GlobalFunction();
            LoginModel objLogin = objglobal.GetLoginDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            if (objLogin != null)
            {
                if (objLogin.UserRole == Convert.ToInt32(_UserRole))
                {
                    if (objLogin.UserRole == Convert.ToInt32(AppsConstants.UserRole.SuperAdmin) || objLogin.UserRole == Convert.ToInt32(AppsConstants.UserRole.MasterAdmin))
                    {

                    }
                    else
                    {
                        SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

                        if (objSubs.UserRole == Convert.ToInt32(AppsConstants.UserRole.HR) && objSubs.ExpiredDate < (DateTime?)DateTime.Now)
                        {
                            string redirectTo = "~/Account/SignIn";
                            ActionResult result = new RedirectResult(redirectTo);
                            filterContext.Result = result;
                        }
                        else if (objSubs.UserRole == Convert.ToInt32(AppsConstants.UserRole.Employee))
                        {
                            if (objEmp!=null)
                            {
                                if (objSubs.ExpiredDate < (DateTime?)DateTime.Now || objEmp.ResignDate < (DateTime?)DateTime.Now)
                                {
                                    string redirectTo = "~/Account/SignIn";
                                    ActionResult result = new RedirectResult(redirectTo);
                                    filterContext.Result = result;
                                }
                            }
                            else if(objSubs.ExpiredDate < (DateTime?)DateTime.Now)
                            {
                                string redirectTo = "~/Account/SignIn";
                                ActionResult result = new RedirectResult(redirectTo);
                                filterContext.Result = result;
                            }
                        }
                    }
                }
                else
                {
                    string redirectTo = "~/Account/SignIn";
                    ActionResult result = new RedirectResult(redirectTo);
                    filterContext.Result = result;
                }
            }
            else
            {
                string redirectTo = "~/Account/SignIn";
                ActionResult result = new RedirectResult(redirectTo);
                filterContext.Result = result;
            }
            base.OnActionExecuting(filterContext);
        }
    }
    public class MyAuthorizeAttribute
    {
        private bool AuthorizeUser(AuthorizationContext filterContext)
        {
            bool isAuthorized = false;

            if (filterContext.RequestContext.HttpContext != null)
            {
                var context = filterContext.RequestContext.HttpContext;

                if (context.Session["User_ID"] != null)
                    isAuthorized = true;

            }
            return isAuthorized;
        }
    }

}