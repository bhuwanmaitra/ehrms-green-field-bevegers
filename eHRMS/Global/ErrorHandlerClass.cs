﻿using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace eHRMS.Global
{
    public class ErrorHandlerClass
    {
        
        eHRMSEntities context = new eHRMSEntities();
        public void LogError(Exception ex)
        {
            try
            {
                tbl_ExceptionErrors objErrors = new tbl_ExceptionErrors()
                {
                    stack_trace = ex.StackTrace,
                    exception_message = ex.Message,
                    inner_exception = ex.InnerException.Message,
                    created_on = DateTime.Now,
                    created_by = "ErrorHandler",
                };
                context.tbl_ExceptionErrors.Add(objErrors);
                context.SaveChanges();
            }
            catch (Exception)
            {
            }
            finally
            {
            }
        } 
    }
}