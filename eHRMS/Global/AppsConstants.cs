﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Global
{
    public class AppsConstants
    {
        public enum UserRole
        {
            MasterAdmin = 1,
            SuperAdmin = 2,
            Admin = 3,
            HR = 4,
            Employee = 5,
        }
        public enum SubscriptionType
        {
            Free = 1,
            Default = 2,
            Manual = 3
        }

        public enum OfficeType
        {
            HeadOffice = 1,
            BrachOffice = 2,
            CityOffice = 3
        }
    }

    public class SessionConstants
    {
        public const string ProfilImage = "ProfilImage";
        public const string ImageName = "ImageName";
        public const string UserProfileObject = "UserProfileObject";
        public const string RoleId = "RoleId";
        public const string UserId = "UserId";
        public const string ContactPerson = "ContactPerson";
    }

    public class Messages
    {
        //public const string RegistrationWelcomeMessage = "Welcome to Artistwala.";
        //public const string RegistrationWelcomeMessageBody = "On the Insert tab, the galleries include items that are designed to coordinate with the overall look of your document. You can use these galleries to insert tables, headers, footers, lists, cover pages, and other document building blocks. When you create pictures, charts, or diagrams, they also coordinate with your current document look.";
    }
}