﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;

namespace eHRMS.Global
{
    public class GlobalVariables
    {

        public static string gCompanyName;

        public static string gUserName;

        public static string gEmployeeId;

        public static string gEmployeeName;

        public static string gCompanyWebsite;

        public static string gCompanyType;

        public static string gConnectionString;

        public static int gErrorCount;

        
    }
}