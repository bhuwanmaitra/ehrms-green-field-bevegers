﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eHRMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "SignIn", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "UserEdit",
            //    url: "{controller}/{action}/{LoginID}",
            //    defaults: new { controller = "AdminUserManagement", action = "EditUsers", LoginID = UrlParameter.Optional }
            //).DataTokens.Add("area", "Admin");

        }
    }
}
