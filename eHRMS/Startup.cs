﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eHRMS.Startup))]
namespace eHRMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
