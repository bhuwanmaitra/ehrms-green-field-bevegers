﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class AttLeave_Settings
    {
        public int SettingsID { get; set; }
        [Required]
        public string AttendanceCanNotUpdate { get; set; }
        [Required]
        public Nullable<bool> NoDeductionHalfdayApprove { get; set; }
        [Required]
        public Nullable<int> LateEntryInMonth { get; set; }
        [Required]
        public Nullable<int> WeeklyHours { get; set; }
        [Required]
        public Nullable<int> CountLateEntryAfter { get; set; }
        [Required]
        public string NoDeductionIfCompleteWorkingHours { get; set; }
        [Required]
        public string PayOvertime { get; set; }
        [Required]
        public string AmountForOvertime { get; set; }
        [Required]
        public Nullable<int> ApplyLeaveBefore { get; set; }
        [Required]
        public string UnapprovedLeaveDeduct { get; set; }
        [Required]
        public string PendingLeaves { get; set; }
        [Required]
        public string AmountForPendingLeaves { get; set; }
        [Required]
        public string HO_WorkingEmployeeGet { get; set; }
        [Required]
        public string AmountFor_HO { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public string test { get; set; }
    }
}