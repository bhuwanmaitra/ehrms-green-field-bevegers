﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class LeavePolicy
    {
        public int LeavePolicyID { get; set; }
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public Nullable<int> FK_DepartmentID { get; set; }
        [Required]
        public Nullable<int> NoticePeriod { get; set; }
        [Required]
        public string Remarks { get; set; }
        [Required]
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        [Required]
        public string ApplicableTo { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}