﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Previous_Employee_TAX
    {
        public int PEInvestID { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        [Required]
        public string GrossSalary { get; set; }
        [Required]
        public string Exemptions_Under_Section_Ten { get; set; }
        [Required]
        public string PF { get; set; }
        [Required]
        public string ProfessionalTax { get; set; }
        [Required]
        public string TotalTaxPaid { get; set; }
        [Required]
        public string TotalIncome { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}