﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace eHRMS.Areas.HR.Models
{
    public class WeeklyOffs
    {     
        public int ID { get; set; }
        [Required]
        public string Day { get; set; }
        [Required]
        public string Type { get; set; }
        public bool First { get; set; }
        public bool Second { get; set; }
        public bool Third { get; set; }
        public bool Fourth { get; set; }
        public bool Fifth { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}