﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class CompanyProject
    {
        public int ProjectID { get; set; }
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public string ProjectCode { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Required]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpadatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}