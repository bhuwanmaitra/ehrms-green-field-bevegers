﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class PaySlipTemplate
    {
        public int PayslipID { get; set; }
        [Required]
        public string PackageID { get; set; }
        [Required]
        public string PackageName { get; set; }
        [Required]
        public string CTCFrom { get; set; }
        [Required]
        public string CTCTo { get; set; }
        [Required]
        public string MonthlyFrom { get; set; }
        [Required]
        public string MonthlyTo { get; set; }
        public Nullable<int> FK_TypeID { get; set; }
        public string TypeName { get; set; }
        public string ComponentType { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}