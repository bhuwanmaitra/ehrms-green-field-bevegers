﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class PayrollSettings
    {
        public int PaySettingsID { get; set; }
        [Required]
        public Nullable<bool> GratuityApplicable { get; set; }
        public string PercentageOfGratuity { get; set; }
        [Required]
        public Nullable<bool> LWFApplicable { get; set; }
        public string PercentageOfLWF { get; set; }
        public Nullable<int> MonthlyWorkingDays { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}