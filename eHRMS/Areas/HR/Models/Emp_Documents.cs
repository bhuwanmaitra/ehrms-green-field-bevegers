﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Documents
    {
        public int EmpDocID { get; set; }
        [Required]
        public string DocType { get; set; }
        [Required]
        public string DocName { get; set; }        
        public string DocPath { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }


        public List< Emp_Documents> Emp_Documents_List { get; set; }
    }
}