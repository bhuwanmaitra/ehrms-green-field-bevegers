﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class CompanyLocation
    {
        public int OfficeLocationID { get; set; }
        [Required]
        public string OfficeType { get; set; }
        [Required]
        public string OfficeCode { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public Nullable<decimal> ContactNo { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")]
        public string Email { get; set; }
        public Nullable<decimal> FAX { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string TimeZone { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}