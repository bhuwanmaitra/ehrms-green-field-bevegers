﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class SalarySlip
    {
        public string SalarySlipID { get; set; }
        public string BannerImagePath { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyWebsite { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public Nullable<decimal> EmployeeNumber { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}