﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class SalaryComponents
    {
        public int TypeID { get; set; }
        [Required]
        public string TypeName { get; set; }
        
        public string ComponentType { get; set; }
        [Required]
        public string Percentage { get; set; }
        
        public Nullable<bool> IsTaxable { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        //[Required]
        public string DeductType { get; set; }
        //[Required]
        public string DeductFrom { get; set; }
    }
}