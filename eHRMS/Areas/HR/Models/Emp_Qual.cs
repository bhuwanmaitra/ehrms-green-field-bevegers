﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Qual
    {
        public int QualID { get; set; }
        
        public string HighestDegreeName { get; set; }
        [Required]
        public int HighestDegree { get; set; }
        public string College_University { get; set; }
        public Nullable<System.DateTime> Duration_from { get; set; }
        public Nullable<System.DateTime> Duration_to { get; set; }
        public string Marks_Grade { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<Emp_Qual> Emp_Qual_list {  get; set; }
    }
}