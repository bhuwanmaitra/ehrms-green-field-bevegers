﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class CompanyProfile
    {
        public int CompanyProfileID { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string CompanyDetails { get; set; }
        [Required]
        public string CompanyType { get; set; }
        [Required]
        public string CompanyWebsite { get; set; }
        [Required]
        public string IndustryType { get; set; }
        //[Required]
        public string BannerImagePath { get; set; }
        [Required]
        public Nullable<System.DateTime> IncorporationDate { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        
    }
}