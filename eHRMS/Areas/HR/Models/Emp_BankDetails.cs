﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_BankDetails
    {
        public int AccountID { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string PAN { get; set; }
        [Required]
        public string AADHAR_Number { get; set; }
        [Required]
        public string NomineeName { get; set; }
        [Required]
        public string RelationwithNominee { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}