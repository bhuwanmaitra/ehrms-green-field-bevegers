﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Joining
    {
        public int JoiningID { get; set; }
        [Required]
        public Nullable<System.DateTime> JoiningDate { get; set; }
        [Required]
        public Nullable<int> FK_OfficeLocationID { get; set; }
        [Required]
        public string OfficeLocation { get; set; }
        [Required]
        public Nullable<int> FK_ProbationPolicyID { get; set; }
        [Required]
        public string ProbationPolicy { get; set; }
        [Required]
        public Nullable<int> FK_DepartmentID { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        public Nullable<int> FK_CategoryID { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public Nullable<int> FK_GradeID { get; set; }
        [Required]
        public string Grade { get; set; }
        [Required]
        public string Shift { get; set; }
        [Required]
        public Nullable<int> FK_ShiftID { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<bool> IsAppraiser { get; set; }
        public List<Emp_Joining> Emp_Joining_List { get; set; }
    }
}