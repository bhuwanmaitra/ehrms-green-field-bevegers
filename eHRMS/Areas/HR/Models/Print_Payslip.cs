﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Print_Payslip
    {
        [Required]
        public string Month { get; set; }
        [Required]
        public string Year { get; set; }
        [Required]
        public string FK_Employee_Code { get; set; }

        // Print the Details
        public string SalarySlipID { get; set; }
        public string BannerImagePath { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyWebsite { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public Nullable<decimal> EmployeeNumber { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string AmountInWords { get; set; }
        public decimal TotalEarnings { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal NetAmount { get; set; }

        public decimal OT { get; set; }



        public string LeaveType { get; set; }
        public string Taken { get; set; }
        public string Left { get; set; }




        public decimal Holiday { get; set; }
        public decimal TotalDaysPresent { get; set; }
        public int   TotalWorkingDay { get; set; }
        public decimal TotalAbsent { get; set; }
    }
}