﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class CompanyStatutory
    {
        public int CompanyStatutoryID { get; set; }
        [Required]
        public string TAN { get; set; }
        [Required]
        public string PAN { get; set; }
        [Required]
        public Nullable<bool> ESIApplicable { get; set; }
        public string ESINumber { get; set; }
        [Required]
        public Nullable<bool> PTApplicable { get; set; }
        public string PTNumber { get; set; }
        [Required]
        public Nullable<bool> PFApplicable { get; set; }
        public string PFNumber { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}