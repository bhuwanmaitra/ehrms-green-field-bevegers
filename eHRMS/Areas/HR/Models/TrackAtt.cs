﻿using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class TrackAtt
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string WorkingHours { get; set; }
        public decimal WorkingHoursInMin { get; set; }
        
        public string FK_CompanyRegistrationID { get; set; }
        
        [Required]
        public string Employee_Code { get; set; }
        [Required]
        public string Month { get; set; }
        [Required]
        public string Year { get; set; }

        public string Status { get; set; }

        public string OTHours { get; set; }
        public string ApprovedHours { get; set; }
        public bool LateApproved { get; set; }
    }
}