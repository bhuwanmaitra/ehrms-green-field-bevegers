﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class TaxComputation
    {
        public int TAXID { get; set; }
        public string Employee_Code { get; set; }
        public Nullable<int> TaxableAmount { get; set; }
        public Nullable<int> Investment { get; set; }
        public Nullable<int> YearlyTAXAmount { get; set; }
        public Nullable<int> MonthlyTAXAmount { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<int> ForYear { get; set; }
        public Nullable<int> FK_SlabMasterID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}