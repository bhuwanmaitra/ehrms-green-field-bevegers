﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class EmployeeInvestment
    {
        public int InvestID { get; set; }
        public string Public_Provident_Fund_PPF_80C { get; set; }
        public string Life_Insurance_LIC_80C { get; set; }
        public string National_Savings_Certificate_Purchased_NSC_80C { get; set; }
        public string Unit_Linked_Insurance_Plan_ULIP_80C { get; set; }
        public string Mutual_Funds_Notified_US_10_23D { get; set; }
        public string Contribution_To_Pension_Fund_80C { get; set; }
        public string Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C { get; set; }
        public string Senior_Citizens_Savings_Scheme_80C { get; set; }
        public string Equity_Linked_Savings_Scheme_ELSS_80C { get; set; }
        public string Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C { get; set; }
        public string Five_Years_Of_Post_Office_Deposits_80C { get; set; }
        public string Education_Tution_Fee_Upto_Two_Children_80C { get; set; }
        public string Rajiv_Gandhi_Equity_Saving_Schemes { get; set; }
        public string Medical_Insurance_Premium_Individual_Spouse_And_Children { get; set; }
        public string Medical_Insurance_Premium_Parents { get; set; }
        public string Preventive_Health_Check_Up { get; set; }
        public string Medical_Treatment_For_Dependent_Handicap { get; set; }
        public string Medical_Treatment_For_Specified_Disease { get; set; }
        public string Medical_Treatment_For_Specified_Disease_For_Senior_Citizen { get; set; }
        public string Permanent_Physical_Disability { get; set; }
        public string Interest_On_Education_Loan { get; set; }
        public string Interest_On_Housing_Loan { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}