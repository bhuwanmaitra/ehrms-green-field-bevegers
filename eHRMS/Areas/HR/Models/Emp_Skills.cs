﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Skills
    {
        public int SkillID { get; set; }
        [Required]
        public string Skills { get; set; }
        public Nullable<int> TotalExperience { get; set; }
        public string PreviousCompany { get; set; }
        public Nullable<System.DateTime> WorkedFrom { get; set; }
        public Nullable<System.DateTime> WorkedTo { get; set; }
        public string LastDrawnSalary { get; set; }
        public string PreviousCompanyAddress { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<Emp_Skills> Emp_Skills_List { get; set; }

        public List<JobSkill> JobSkill_list { get; set; }
    }
}