﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Passport
    {
        public int PassportID { get; set; }
        public string PassportNumber { get; set; }
        public string IssuingAuthority { get; set; }
        public Nullable<System.DateTime> IssueFrom { get; set; }
        public Nullable<System.DateTime> IssueTo { get; set; }
        public string Citizenship { get; set; }
        public string VisaNumber { get; set; }
        public string TypeofVisa { get; set; }
        public Nullable<System.DateTime> VisaValidFrom { get; set; }
        public Nullable<System.DateTime> VisaValidTo { get; set; }
        public string VisaCitizenship { get; set; }
        public string Remarks { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<Emp_Passport> Emp_Passport_List{ get; set; }
    }
}