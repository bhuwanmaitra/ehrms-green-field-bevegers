﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class LeaveBalance
    {
        public string AppliedFor { get; set; }
        public Nullable<decimal> LeaveAllowed { get; set; }
        public Nullable<decimal> LeaveTaken { get; set; }
        public Nullable<decimal> LeaveDue { get; set; }
    }
}