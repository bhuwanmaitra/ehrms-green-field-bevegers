﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class ItemForTAXComputation
    {        
        public int ComponentTotalPercentage { get; set; }
        public int TaxableAmount { get; set; }
        public int Investment { get; set; }
        public int ITPercentage { get; set; }
        public int YearlyTAX { get; set; }
        public int MonthlyTAX { get; set; }
        public int FK_SlabMasterID { get; set; }
    }
}