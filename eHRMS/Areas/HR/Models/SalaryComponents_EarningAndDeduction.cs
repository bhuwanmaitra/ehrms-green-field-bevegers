﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class SalaryComponents_EarningAndDeduction
    {
        public string ComponentType { get; set; }
        public string Component { get; set; }
        public Nullable<decimal> Amount { get; set; }
    }


    //public class SalaryComponents_EarningAndDeduction_Test
    //{
    //    public List<SalaryComponents_EarningAndDeduction> Salary_Component_List { get; set; }
    //}
}