﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class dashboardLeave
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> LeaveFrom { get; set; }
        public Nullable<System.DateTime> LeaveTo { get; set; }
        public Nullable<int> LeaveDays { get; set; }
        public string AppliedFor { get; set; }
        public string Reason { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string FilterBy{ get; set; }

    }
  
}