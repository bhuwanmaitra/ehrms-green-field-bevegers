﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class AssignLeave
    {
        public int ID { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public Nullable<int> NoOfLeave { get; set; }
        [Required]
        public Nullable<int> FK_DepartmentID { get; set; }
        [Required]
        public string Department { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}