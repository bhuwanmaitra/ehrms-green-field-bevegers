﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Track_Leave
    {
        [Required]
        public string Employee_Code { get; set; }

        [Required]
        public string Year { get; set; }
    }
}