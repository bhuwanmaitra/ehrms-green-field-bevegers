﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Holiday
    {
        public int ID { get; set; }
        [Required]
        public Nullable<System.DateTime> Date { get; set; }
        public string Year { get; set; }
        [Required]
        public string Title { get; set; }
        
        public string Details { get; set; }
        [Required]
        public Nullable<bool> SameDayEveryYear { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}