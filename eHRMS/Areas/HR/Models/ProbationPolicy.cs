﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace eHRMS.Areas.HR.Models
{
    public class ProbationPolicy
    {
        public int ProbationPolicyID { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Duration { get; set; }
        [Required]
        public Nullable<bool> PeriodExtendable { get; set; }
        [Required]
        public string ConfirmationReminder { get; set; }
        [Required]
        public string FK_ContractPolicyID { get; set; }
        public Nullable<bool> SendConfirmLetter { get; set; }
        [Required]
        public string Remarks { get; set; }
        [Required]
        public Nullable<bool> ResignApplicable { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}