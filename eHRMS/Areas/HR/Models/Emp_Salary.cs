﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class Emp_Salary
    {
        public int SalaryID { get; set; }
        [Required]
        public string FK_PackageID { get; set; }
        [Required]
        public string CTC { get; set; }
        [Required]
        public string Monthly { get; set; }
        public string PFNumber { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public List<Emp_Salary> Emp_Salary_list { get; set; }
    }
}