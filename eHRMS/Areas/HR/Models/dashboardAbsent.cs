﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.HR.Models
{
    public class DashboardAbsent
    {
        public string Employee_Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}