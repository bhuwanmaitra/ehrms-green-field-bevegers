﻿using eHRMS.Models;
using eHRMS.Areas.HR.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;
using System.IO;
using System.Drawing;
using System.Text;
using System.Drawing.Drawing2D;
using System.Collections.ObjectModel;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRCompanyController : Controller
    {
        
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/HRCompany
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Details(CompanyProfile SubsTbl)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {
                SubsTbl = context.tbl_CompanyProfile.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyProfile
                {
                    CompanyName = s.CompanyName,
                    CompanyDetails = s.CompanyDetails,
                    CompanyType = s.CompanyType,
                    CompanyWebsite = s.CompanyWebsite,
                    IndustryType = s.IndustryType,
                    BannerImagePath = s.BannerImagePath,
                    IncorporationDate = s.IncorporationDate,
                    FK_CompanyRegistrationID = s.FK_CompanyRegistrationID,
                    IsActive = s.IsActive,
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return View(SubsTbl);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Company_Details_View(CompanyProfile SubsTbl)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {
                SubsTbl = context.tbl_CompanyProfile.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyProfile
                {
                    CompanyName = s.CompanyName,
                    CompanyDetails = s.CompanyDetails,
                    CompanyType = s.CompanyType,
                    CompanyWebsite = s.CompanyWebsite,
                    IndustryType = s.IndustryType,
                    BannerImagePath = s.BannerImagePath,
                    IncorporationDate = s.IncorporationDate,
                    FK_CompanyRegistrationID = s.FK_CompanyRegistrationID,
                    IsActive = s.IsActive,
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return PartialView(SubsTbl);
        }

        [HttpPost]
        public ActionResult Company_Details(CompanyProfile model, HttpPostedFileBase file)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                if (ModelState.IsValid && file != null)
                {
                    // Upload Image
                    bool flag = false;
                    Stream stream = file.InputStream;
                    Bitmap sourceImage = new Bitmap(stream);
                    string myfile = string.Empty;
                    var allowedExtensions = new[] { ".JPG", ".PNG", ".GIFS", ".JPEG" };
                    var ext = Path.GetExtension(file.FileName).ToUpper();
                    if (allowedExtensions.Contains(ext))
                    {
                        myfile = "Banner-" + objSubs.FK_CompanyRegistrationID + ext;
                        //resize the image
                        int smallImageHeight = 268;
                        int smallImageWidth = 687;
                        System.Drawing.Image resizedImage = new System.Drawing.Bitmap(smallImageWidth, smallImageHeight, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
                        Graphics gr = Graphics.FromImage(resizedImage);
                        gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        gr.DrawImage(sourceImage, 0, 0, smallImageWidth, smallImageHeight);
                        var path = Path.Combine(Server.MapPath("~/Content/hrms_images/CompanyBanner"), myfile);
                        resizedImage.Save(path);
                        flag = true;
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "Please choose only Image file";
                    }
                    // End Image Upload
                    if (flag)
                    {
                        if (!context.tbl_CompanyProfile.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                        {
                            SaveDetails(model, myfile);
                            ViewBag.SuccessMsg = "Data saved successfully !";
                            //ModelState.Clear();
                            return View();
                        }
                        else
                        {
                            UpdateDetails(model, myfile);
                            ViewBag.SuccessMsg = "Data updated successfully !";
                            //ModelState.Clear();
                            return View();
                        }

                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }


        #region Saving Details
        private void SaveDetails(CompanyProfile model, string myfile)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyProfile objCmpPrfl = new tbl_CompanyProfile()
                {
                    CompanyName = model.CompanyName,
                    CompanyDetails = model.CompanyDetails,
                    CompanyType = model.CompanyType,
                    CompanyWebsite = model.CompanyWebsite,
                    IndustryType = model.IndustryType,
                    BannerImagePath = myfile,
                    IncorporationDate = model.IncorporationDate,
                    FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                    IsActive = true,
                    CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                    CreatedBy = objSubs.Username
                };
                context.tbl_CompanyProfile.Add(objCmpPrfl);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }
        #endregion

        #region Update Details
        private void UpdateDetails(CompanyProfile model, string myfile)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyProfile Profiledetails = context.tbl_CompanyProfile.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();

                Profiledetails.CompanyName = model.CompanyName;
                Profiledetails.CompanyDetails = model.CompanyDetails;
                Profiledetails.CompanyType = model.CompanyType;
                Profiledetails.CompanyWebsite = model.CompanyWebsite;
                Profiledetails.IndustryType = model.IndustryType;
                Profiledetails.BannerImagePath = myfile;
                Profiledetails.IncorporationDate = model.IncorporationDate;
                Profiledetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                Profiledetails.UpdatedBy = objSubs.Username;
                context.Entry(Profiledetails).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Location()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<CompanyLocation> LocationList = new List<CompanyLocation>();
            try
            {
                LocationList = context.tbl_CompanyLocation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyLocation
                {
                    OfficeLocationID = s.OfficeLocationID,
                    OfficeType = s.OfficeType,
                    OfficeCode = s.OfficeCode,
                    ContactPerson = s.ContactPerson,
                    ContactNo = s.ContactNo,
                    Email = s.Email,
                    Address = s.OfficeAddress,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(LocationList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Location_Create(string id)
        {
            ViewData["AllTimeZoneList"] = AllTimeZoneList();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetLocDetails(id));
            }
            else
            {
                return View();
            }

        }
        
        public List<SelectListItem> AllTimeZoneList()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
                foreach (TimeZoneInfo timeZoneInfo in timeZones)
                {
                    objSelLst.Add(new SelectListItem { Text = timeZoneInfo.DisplayName, Value = timeZoneInfo.Id });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        
        #region Get Location Details
        private CompanyLocation GetLocDetails(string id)
        {
            int OfficeLocationID = Convert.ToInt32(id);
            tbl_CompanyLocation objLocdetails = context.tbl_CompanyLocation.Where(x => x.IsActive == true && x.OfficeLocationID == OfficeLocationID).FirstOrDefault();

            CompanyLocation objdetails = new CompanyLocation();
            if (objLocdetails != null)
            {
                objdetails = new CompanyLocation()
                {
                    OfficeLocationID = objLocdetails.OfficeLocationID,
                    OfficeType = objLocdetails.OfficeType,
                    OfficeCode = objLocdetails.OfficeCode,
                    ContactPerson = objLocdetails.ContactPerson,
                    ContactNo = objLocdetails.ContactNo,
                    Email = objLocdetails.Email,
                    FAX = objLocdetails.FAX,
                    TimeZone=objLocdetails.TimeZone,
                    Address = objLocdetails.OfficeAddress,
                    FK_CompanyRegistrationID = objLocdetails.FK_CompanyRegistrationID
                };
                return objdetails;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong or Location is not active !";
                return objdetails;
            }
        }
        #endregion


        [HttpPost]
        public ActionResult Company_Location_Create(CompanyLocation model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int OfficeLocationID = Convert.ToInt32(id);
                        tbl_CompanyLocation objLocDetails = context.tbl_CompanyLocation.First(d => d.OfficeLocationID == OfficeLocationID);
                        // Update Table
                        objLocDetails.OfficeType = model.OfficeType;
                        objLocDetails.OfficeCode = model.OfficeCode;
                        objLocDetails.ContactPerson = model.ContactPerson;
                        objLocDetails.ContactNo = model.ContactNo;
                        objLocDetails.Email = model.Email;
                        objLocDetails.FAX = model.FAX;
                        objLocDetails.TimeZone = model.TimeZone;
                        objLocDetails.OfficeAddress = model.Address;
                        objLocDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objLocDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objLocDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Office location updated successfully !";
                    }
                    else
                    {
                        tbl_CompanyLocation objloc = new tbl_CompanyLocation()
                        {
                            OfficeType = model.OfficeType,
                            OfficeCode = model.OfficeCode,
                            ContactPerson = model.ContactPerson,
                            ContactNo = model.ContactNo,
                            Email = model.Email,
                            FAX = model.FAX,
                            TimeZone=model.TimeZone,
                            OfficeAddress = model.Address,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_CompanyLocation.Add(objloc);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Office location added successfully !";
                    }
                    ModelState.Clear();
                    ViewData["AllTimeZoneList"] = AllTimeZoneList();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["AllTimeZoneList"] = AllTimeZoneList();
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["AllTimeZoneList"] = AllTimeZoneList();
                return View();
            }
        }


        #region Delete Location
        public JsonResult DeleteLocation(string OfficeLocationID)
        {
            OfficeLocationID = HttpUtility.UrlDecode(OfficeLocationID);
            if (OfficeLocationID != null)
            {
                int id = Convert.ToInt32(OfficeLocationID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyLocation objLocDetails = context.tbl_CompanyLocation.First(d => d.OfficeLocationID == id);
                try
                {
                    // Update Table
                    objLocDetails.IsActive = false;
                    objLocDetails.UpdatedBy = objSubs.Username;
                    objLocDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objLocDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Office location deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Statutory()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<CompanyStatutory> StatutoryList = new List<CompanyStatutory>();
            try
            {
                StatutoryList = context.tbl_CompanyStatutory.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyStatutory
                {
                    CompanyStatutoryID = s.CompanyStatutoryID,
                    TAN = s.TAN,
                    PAN = s.PAN,
                    ESINumber = s.ESINumber,
                    PTNumber = s.PTNumber,
                    PFNumber = s.PFNumber,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(StatutoryList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Statutory_Create(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetStatutoryDetails(id));
            }
            else
            {
                return View();
            }

        }

        #region Get Statutory Details
        private CompanyStatutory GetStatutoryDetails(string id)
        {
            int CompanyStatutoryID = Convert.ToInt32(id);
            tbl_CompanyStatutory objStatdetails = context.tbl_CompanyStatutory.Where(x => x.IsActive == true && x.CompanyStatutoryID == CompanyStatutoryID).FirstOrDefault();

            CompanyStatutory objdetails = new CompanyStatutory();
            if (objStatdetails != null)
            {
                objdetails = new CompanyStatutory()
                {
                    CompanyStatutoryID = objStatdetails.CompanyStatutoryID,
                    TAN = objStatdetails.TAN,
                    PAN = objStatdetails.PAN,
                    ESIApplicable = objStatdetails.ESIApplicable,
                    ESINumber = objStatdetails.ESINumber,
                    PTApplicable = objStatdetails.PTApplicable,
                    PTNumber = objStatdetails.PTNumber,
                    PFApplicable = objStatdetails.PFApplicable,
                    PFNumber = objStatdetails.PFNumber
                };
                return objdetails;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong or Statutory is not active !";
                return objdetails;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Company_Statutory_Create(CompanyStatutory model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int CompanyStatutoryID = Convert.ToInt32(id);
                        tbl_CompanyStatutory objStatDetails = context.tbl_CompanyStatutory.First(d => d.CompanyStatutoryID == CompanyStatutoryID);
                        // Update Table
                        objStatDetails.TAN = model.TAN;
                        objStatDetails.PAN = model.PAN;
                        objStatDetails.ESIApplicable = model.ESIApplicable;
                        objStatDetails.ESINumber = model.ESINumber;
                        objStatDetails.PTApplicable = model.PTApplicable;
                        objStatDetails.PTNumber = model.PTNumber;
                        objStatDetails.PFApplicable = model.PFApplicable;
                        objStatDetails.PFNumber = model.PFNumber;
                        objStatDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objStatDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objStatDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Company Statutory updated successfully !";
                    }
                    else
                    {
                        tbl_CompanyStatutory objStat = new tbl_CompanyStatutory()
                        {
                            TAN = model.TAN,
                            PAN = model.PAN,
                            ESIApplicable = model.ESIApplicable,
                            ESINumber = model.ESINumber,
                            PTApplicable = model.PTApplicable,
                            PTNumber = model.PTNumber,
                            PFApplicable = model.PFApplicable,
                            PFNumber = model.PFNumber,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_CompanyStatutory.Add(objStat);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Company statutory added successfully !";
                    }
                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Statutory
        public JsonResult DeleteStatutory(string OfficeStatutoryID)
        {
            OfficeStatutoryID = HttpUtility.UrlDecode(OfficeStatutoryID);
            if (OfficeStatutoryID != null)
            {
                int id = Convert.ToInt32(OfficeStatutoryID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyStatutory objStatDetails = context.tbl_CompanyStatutory.First(d => d.CompanyStatutoryID == id);
                try
                {
                    // Update Table
                    objStatDetails.IsActive = false;
                    objStatDetails.UpdatedBy = objSubs.Username;
                    objStatDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objStatDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Company statutory deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Bank()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<CompanyBank> BankList = new List<CompanyBank>();
            try
            {
                BankList = context.tbl_CompanyBank.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyBank
                {
                    CompanyBankID = s.CompanyBankID,
                    BankName = s.BranchName,
                    BranchName = s.BranchName,
                    AccountNumber = s.AccountNumber,
                    SignAuthority = s.SignAuthority,
                    IFSCCode = s.IFSCCode,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(BankList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Company_Bank_Create(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetBankDetails(id));
            }
            else
            {
                return View();
            }

        }

        #region Get Bank Details
        private CompanyBank GetBankDetails(string id)
        {
            int BankID = Convert.ToInt32(id);
            tbl_CompanyBank objBankdetails = context.tbl_CompanyBank.Where(x => x.IsActive == true && x.CompanyBankID == BankID).FirstOrDefault();

            CompanyBank objdetails = new CompanyBank();
            if (objBankdetails != null)
            {
                objdetails = new CompanyBank()
                {
                    CompanyBankID = objBankdetails.CompanyBankID,
                    BankName = objBankdetails.BankName,
                    BranchName = objBankdetails.BranchName,
                    BranchAddress = objBankdetails.BranchAddress,
                    AccountNumber = objBankdetails.AccountNumber,
                    SignAuthority = objBankdetails.SignAuthority,
                    IFSCCode = objBankdetails.IFSCCode,
                    FK_CompanyRegistrationID = objBankdetails.FK_CompanyRegistrationID
                };
                return objdetails;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong or Bank is not active !";
                return objdetails;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Company_Bank_Create(CompanyBank model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int BankID = Convert.ToInt32(id);
                        tbl_CompanyBank objBankDetails = context.tbl_CompanyBank.First(d => d.CompanyBankID == BankID);
                        // Update Table
                        objBankDetails.BankName = model.BankName;
                        objBankDetails.BranchName = model.BranchName;
                        objBankDetails.BranchAddress = model.BranchAddress;
                        objBankDetails.SignAuthority = model.SignAuthority;
                        objBankDetails.AccountNumber = model.AccountNumber;
                        objBankDetails.IFSCCode = model.IFSCCode;
                        objBankDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objBankDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objBankDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Bank details updated successfully !";
                    }
                    else
                    {
                        tbl_CompanyBank objbank = new tbl_CompanyBank()
                        {
                            BankName = model.BankName,
                            BranchName = model.BranchName,
                            BranchAddress = model.BranchAddress,
                            AccountNumber = model.AccountNumber,
                            IFSCCode = model.IFSCCode,
                            SignAuthority = model.SignAuthority,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_CompanyBank.Add(objbank);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Bank details added successfully !";
                    }
                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }


        #region Delete Bank
        public JsonResult DeleteBank(string CompanyBankID)
        {
            CompanyBankID = HttpUtility.UrlDecode(CompanyBankID);
            if (CompanyBankID != null)
            {
                int id = Convert.ToInt32(CompanyBankID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyBank objBankDetails = context.tbl_CompanyBank.First(d => d.CompanyBankID == id);
                try
                {
                    // Update Table
                    objBankDetails.IsActive = false;
                    objBankDetails.UpdatedBy = objSubs.Username;
                    objBankDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objBankDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Bank details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion
    }
}