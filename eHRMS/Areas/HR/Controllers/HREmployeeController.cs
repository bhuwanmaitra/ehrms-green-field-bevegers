﻿using eHRMS.Areas.Admin.Models;
using eHRMS.Areas.Employee.Models;
using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Models;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace eHRMS.Areas.HR.Controllers
{
    public class HREmployeeController : Controller
    {
        
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/HREmployee

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Management()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_List()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Contact_Basic> EmpList = new List<Emp_Contact_Basic>();
            try
            {
                EmpList = context.tbl_Emp_Contact_Basic.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Emp_Contact_Basic
                {
                    Employee_Code = s.Employee_Code,
                    Name = s.Name,
                    Email = s.Email,
                    Number = s.Number,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Details()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Add()
        {

            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Contact_Basic()
        {
            Emp_Contact_Basic Emp_Contact_Basic_Tbl = new Emp_Contact_Basic();
            try
            {


                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobEmployeeCode objSetting = context.tbl_JobEmployeeCode.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.IsActive == true).FirstOrDefault();
                if (objSetting.EmpCodeType == "System Generated" && objSetting.Prefix == "Company Wise")
                {
                    Emp_Contact_Basic_Tbl.Employee_Code = objSubs.CompanyName + "-" + System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper();
                }
                else if (objSetting.EmpCodeType == "System Generated" && objSetting.Prefix == "Branch Wise")
                {
                    Emp_Contact_Basic_Tbl.Employee_Code = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper();
                }

            }
            catch (Exception ex)
            {
                objError.LogError(ex);

            }
            return PartialView(Emp_Contact_Basic_Tbl);
        }

        [HttpPost]
        public ActionResult Employee_Contact_Basic(Emp_Contact_Basic model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

                    if (!context.tbl_Emp_Contact_Basic.Any(x => x.Email.Trim() == model.Email.Trim() && x.Number == model.Number))
                    {
                        tbl_Emp_Contact_Basic objBasic = new tbl_Emp_Contact_Basic()
                        {
                            Name = model.Name,
                            Email = model.Email,
                            Number = model.Number,
                            DOB = model.DOB,
                            Sex = model.Sex,
                            Employee_Code = model.Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Contact_Basic.Add(objBasic);
                        context.SaveChanges();
                        TempData["SuccessMsg"] = "Contact and Basic details of employee saved successfully !";
                        ModelState.Clear();
                        return RedirectToAction("Employee_Add");
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "This Email or Contact Number already registered with us !";
                        return RedirectToAction("Employee_Add");
                    }
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Qual_Emp()
        {
            ViewData["Employee_Qual_List"] = EmployeeCodeListQual();
            ViewData["Qualification_List"] = QualificationList();
            return PartialView();
        }
        public List<SelectListItem> EmployeeCodeListQual()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Qual_Employment.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        public List<SelectListItem> QualificationList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobQualification.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.JobQualificationID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Employee_Qual_Emp(Emp_Qual model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_Emp_Qual_Employment objQual = new tbl_Emp_Qual_Employment()
                    {
                        HighestDegree = model.HighestDegree,
                        College_University = model.College_University,
                        Duration_from = model.Duration_from,
                        Duration_to = model.Duration_to,
                        Marks_Grade = model.Marks_Grade,
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_Emp_Qual_Employment.Add(objQual);
                    context.SaveChanges();
                    TempData["SuccessMsg"] = "Qualification details of employee saved successfully !";
                    ModelState.Clear();
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Skills_Emp()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
            ViewData["Employee_Code_List"] = EmployeeCodeList();
            return PartialView();
        }

        public List<SelectListItem> EmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Skill_Employment.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).OrderBy(s => (s.Employee_Code)).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Employee_Skills_Emp(Emp_Skills model, FormCollection fc)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {
                int Skillcount = default(int);
                int.TryParse(fc["hidedvSkillsCount"], out Skillcount);
                for (int i = 1; i <= Skillcount; i++)
                {
                    int SkillID = default(int);
                    int.TryParse(fc["chkSkill" + i], out SkillID);
                    tbl_JobSkill objdetails = context.tbl_JobSkill.Where(x => x.SkillID == SkillID && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                    if (SkillID > 0)
                    {
                        model.Skills += objdetails.Type + ", ";
                    }
                }
                if (model.Employee_Code != null && model.Skills != null)
                {
                    tbl_Emp_Skill_Employment objSkill = new tbl_Emp_Skill_Employment()
                    {
                        Skills = model.Skills,
                        TotalExperience = model.TotalExperience,
                        PreviousCompany = model.PreviousCompany,
                        WorkedFrom = model.WorkedFrom,
                        WorkedTo = model.WorkedTo,
                        LastDrawnSalary = model.LastDrawnSalary,
                        PreviousCompanyAddress = model.PreviousCompanyAddress,
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_Emp_Skill_Employment.Add(objSkill);
                    context.SaveChanges();
                    ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
                    TempData["SuccessMsg"] = "Skills and Employment details of employee saved successfully !";
                    ModelState.Clear();
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
                return RedirectToAction("Employee_Add");
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Probation_Joining()
        {
            ViewData["Code_List_Join"] = JoinCodeList();
            ViewData["Location_List"] = LocationList();
            ViewData["Probation_Type_List"] = ProbationTypeList();
            ViewData["Department_List"] = DepartmentList();
            ViewData["Category_List"] = CategoryList();
            ViewData["Grade_List"] = GradeList();
            ViewData["Shift_List"] = ShiftList();
            return PartialView();
        }

        public List<SelectListItem> JoinCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Joining_Probation.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> LocationList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_CompanyLocation.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.OfficeCode, Value = item.OfficeLocationID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        public List<SelectListItem> ProbationTypeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobProbationPolicy.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Category, Value = item.ProbationPolicyID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> DepartmentList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobDepartment.Where(c => c.Fk_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.DepartmentID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> CategoryList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobCategory.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.CategoryID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> GradeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobGrade.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.GradeID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> ShiftList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSftLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_WorkShift.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSftLst.Add(new SelectListItem { Text = item.ShiftName, Value = item.ShiftID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSftLst;
        }

        [HttpPost]
        public ActionResult Employee_Probation_Joining(Emp_Joining model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_Emp_Joining_Probation objSkill = new tbl_Emp_Joining_Probation()
                    {
                        JoiningDate = model.JoiningDate,
                        ProbationPolicy = model.ProbationPolicy,
                        FK_ProbationPolicyID = model.FK_ProbationPolicyID,
                        OfficeLocation = model.OfficeLocation,
                        FK_OfficeLocationID = model.FK_OfficeLocationID,
                        Department = model.Department,
                        FK_DepartmentID = model.FK_DepartmentID,
                        Category = model.Category,
                        FK_CategoryID = model.FK_CategoryID,
                        Grade = model.Grade,
                        FK_GradeID = model.FK_GradeID,
                        Shift = model.Shift,
                        FK_ShiftID = Convert.ToInt32(model.FK_ShiftID),
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username,
                        IsAppraiser = model.IsAppraiser
                    };
                    context.tbl_Emp_Joining_Probation.Add(objSkill);
                    context.SaveChanges();
                    TempData["SuccessMsg"] = "Probation and Joining details of employee saved successfully !";
                    ModelState.Clear();
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Salary()
        {
            ViewData["Code_List_Salary"] = SalaryCodeList();
            ViewData["CTC_List_Salary"] = SalaryCTCList();
            return PartialView();
        }

        public List<SelectListItem> SalaryCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Salary.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> SalaryCTCList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_PayslipTemplate.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).GroupBy(g => new { g.PackageID, g.PackageName }).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Key.PackageName, Value = item.Key.PackageID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Employee_Salary(Emp_Salary model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_Emp_Salary objSkill = new tbl_Emp_Salary()
                    {
                        FK_PackageID = model.FK_PackageID,
                        CTC = model.CTC,
                        Monthly = model.Monthly,
                        PFNumber = model.PFNumber,
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_Emp_Salary.Add(objSkill);
                    context.SaveChanges();

                    ItemForTAXComputation objIT = objglobal.GetITDetails(model.Employee_Code, model.FK_PackageID, model.CTC);

                    tbl_TAXComputation objTC = new tbl_TAXComputation()
                    {
                        Employee_Code = model.Employee_Code,
                        TaxableAmount = objIT.TaxableAmount,
                        Investment = objIT.Investment,
                        YearlyTAXAmount = objIT.YearlyTAX,
                        MonthlyTAXAmount = objIT.MonthlyTAX,
                        FK_SlabMasterID = objIT.FK_SlabMasterID,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        ForYear = System.DateTime.Now.Year,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_TAXComputation.Add(objTC);
                    context.SaveChanges();
                    TempData["SuccessMsg"] = "Salary and Statutory details of employee saved successfully !";
                    ModelState.Clear();
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Passport_Visa()
        {
            ViewData["Code_List_Visa"] = VisaCodeList();
            return PartialView();
        }
        public List<SelectListItem> VisaCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Visa_Passport.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Employee_Passport_Visa(Emp_Passport model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_Emp_Visa_Passport objSkill = new tbl_Emp_Visa_Passport()
                    {
                        PassportNumber = model.PassportNumber,
                        IssuingAuthority = model.IssuingAuthority,
                        IssueFrom = model.IssueFrom,
                        IssueTo = model.IssueTo,
                        Citizenship = model.Citizenship,
                        VisaNumber = model.VisaNumber,
                        TypeofVisa = model.TypeofVisa,
                        VisaValidFrom = model.VisaValidFrom,
                        VisaValidTo = model.VisaValidTo,
                        VisaCitizenship = model.VisaCitizenship,
                        Remarks = model.Remarks,
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_Emp_Visa_Passport.Add(objSkill);
                    context.SaveChanges();
                    TempData["SuccessMsg"] = "Passport and Visa details of employee saved successfully !";
                    ModelState.Clear();
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Employee_Document()
        {
            ViewData["Code_List_BankAcnt"] = BankAccountAllCodeList();
            ViewData["DocType_List"] = DocTypeList();
            return PartialView();
        }
        public List<SelectListItem> DocTypeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobDocument.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.Name });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Employee_Document(Emp_Documents model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid && file != null)
                {
                    var getEmp_Documents = context.tbl_Emp_Documents.FirstOrDefault(x => x.DocType.ToUpper() == model.DocType.ToUpper() && x.DocName.ToUpper() == model.DocName.ToUpper() && x.Employee_Code == model.Employee_Code);
                    if (getEmp_Documents != null)
                    {
                        TempData["ErrorMsg"] = "This Document already uploaded for this Employee !";
                    }
                    else
                    {
                        //Upload Document
                        bool flag = false;
                        string myfile = string.Empty;
                        var allowedExtensions = new[] { ".JPG", ".JPEG", ".PNG", ".PDF", ".DOC", ".DOCX" };
                        var ext = Path.GetExtension(file.FileName).ToUpper();
                        if (allowedExtensions.Contains(ext))
                        {
                            myfile = model.Employee_Code + "-DOC-" + model.DocName.Replace(" ", "") + ext;
                            string path = Path.Combine(Server.MapPath("~/Content/Emp_Document"), myfile);
                            file.SaveAs(path);
                            flag = true;
                        }
                        else
                        {
                            TempData["ErrorMsg"] = "Please choose only .JPG, .JPEG, .PNG, PDF, .DOC, .DOCX file";
                        }
                        // End Document Upload
                        if (flag)
                        {
                            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                            tbl_Emp_Documents objDoc = new tbl_Emp_Documents()
                            {
                                DocType = model.DocType,
                                DocName = model.DocName,
                                DocPath = myfile,
                                Employee_Code = model.Employee_Code,
                                FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                CreatedBy = objSubs.Username
                            };
                            context.tbl_Emp_Documents.Add(objDoc);
                            context.SaveChanges();
                            TempData["SuccessMsg"] = "Document of employee uploaded successfully !";
                            ModelState.Clear();
                        }
                    }
                    return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Employee_Add");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Employee_Add");
            }
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]

        public ActionResult Employee_Account_List()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_BankDetails> EmpList = new List<Emp_BankDetails>();
            try
            {
                EmpList = context.tbl_Emp_BankAccount.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Emp_BankDetails
                {
                    AccountID = s.AccountID,
                    Employee_Code = s.Employee_Code,
                    AccountNumber = s.AccountNumber,
                    PAN = s.PAN,
                    AADHAR_Number = s.AADHAR_Number,
                    NomineeName = s.NomineeName,
                    RelationwithNominee = s.RelationwithNominee,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpList);
        }

        #region Delete Bank Details
        public JsonResult DeleteBankDetails(string AccountID)
        {
            AccountID = HttpUtility.UrlDecode(AccountID);
            if (AccountID != null)
            {
                int id = Convert.ToInt32(AccountID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_BankAccount objQulDetails = context.tbl_Emp_BankAccount.First(d => d.AccountID == id);
                try
                {
                    // Update Table
                    objQulDetails.IsActive = false;
                    objQulDetails.UpdatedBy = objSubs.Username;
                    objQulDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objQulDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Employee Bank Details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Account_Add(string id)
        {

            Emp_BankDetails model = new Emp_BankDetails();
            if (id != null)
            {
                ViewData["Code_List_BankAcnt"] = BankAccountAllCodeList();
                id = HttpUtility.UrlDecode(id);
                return View(GetBankDetails(id));
            }
            else
            {
                ViewData["Code_List_BankAcnt"] = BankAccountCodeList();
                return View(model);
            }
        }

        public List<SelectListItem> BankAccountCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_BankAccount.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> BankAccountAllCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        #region Get Bank Details
        private Emp_BankDetails GetBankDetails(string id)
        {
            int AccountID = Convert.ToInt32(id);
            tbl_Emp_BankAccount objdetails = context.tbl_Emp_BankAccount.Where(x => x.IsActive == true && x.AccountID == AccountID).FirstOrDefault();

            Emp_BankDetails objlist = new Emp_BankDetails();
            if (objdetails != null)
            {
                objlist = new Emp_BankDetails()
                {
                    Employee_Code = objdetails.Employee_Code,
                    AccountID = objdetails.AccountID,
                    AccountNumber = objdetails.AccountNumber,
                    PAN = objdetails.PAN,
                    AADHAR_Number = objdetails.AADHAR_Number,
                    NomineeName = objdetails.NomineeName,
                    RelationwithNominee = objdetails.RelationwithNominee,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Employee_Account_Add(Emp_BankDetails model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int AccountID = Convert.ToInt32(id);
                        tbl_Emp_BankAccount objQulDetails = context.tbl_Emp_BankAccount.First(d => d.AccountID == AccountID);
                        // Update Table
                        objQulDetails.Employee_Code = model.Employee_Code;
                        objQulDetails.AccountNumber = model.AccountNumber;
                        objQulDetails.PAN = model.PAN;
                        objQulDetails.AADHAR_Number = model.AADHAR_Number;
                        objQulDetails.NomineeName = model.NomineeName;
                        objQulDetails.RelationwithNominee = model.RelationwithNominee;
                        objQulDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objQulDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objQulDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Bank Details updated successfully !";
                        ViewData["Code_List_BankAcnt"] = BankAccountAllCodeList();
                    }
                    else
                    {
                        tbl_Emp_BankAccount objQul = new tbl_Emp_BankAccount()
                        {
                            Employee_Code = model.Employee_Code,
                            AccountNumber = model.AccountNumber,
                            PAN = model.PAN,
                            AADHAR_Number = model.AADHAR_Number,
                            NomineeName = model.NomineeName,
                            RelationwithNominee = model.RelationwithNominee,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_BankAccount.Add(objQul);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Bank Details added successfully !";
                        ModelState.Clear();
                        ViewData["Code_List_BankAcnt"] = BankAccountCodeList();
                    }
                    return View();
                }
                else
                {
                    ViewData["Code_List_BankAcnt"] = BankAccountCodeList();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult ResignRequest_List()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_Resign> EmpRList = new List<Emp_Resign>();
            try
            {
                EmpRList = context.tbl_Emp_Resign.Where(x => (x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID) && x.ResignDate >= System.DateTime.Now || x.ResignDate == null).Select(s => new Emp_Resign
                {
                    ResignID = s.ResignID,
                    Employee_Code = s.Employee_Code,
                    Name = s.Name,
                    Email = s.Email,
                    Number = s.Number,
                    Department = s.Department,
                    Designation = s.Designation,
                    Reason = s.Reason,
                    CreatedOn = s.CreatedOn,
                    NoticePeriod = s.NoticePeriod,
                    IsAccepted = s.IsAccepted,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpRList);
        }

        #region Approve Resign request
        public JsonResult ApproveRequest(string ResignID)
        {
            ResignID = HttpUtility.UrlDecode(ResignID);
            if (ResignID != null)
            {
                int id = Convert.ToInt32(ResignID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Resign objDetails = context.tbl_Emp_Resign.First(d => d.ResignID == id);
                try
                {
                    // Update Table
                    objDetails.IsAccepted = true;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    objDetails.ResignDate = System.DateTime.Now.AddDays(Convert.ToDouble(objDetails.NoticePeriod));
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();
                    ViewBag.SuccessMsg = "Resign request approved !";
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        #region Reject Resign request
        public JsonResult RejectRequest(string ResignID)
        {
            ResignID = HttpUtility.UrlDecode(ResignID);
            if (ResignID != null)
            {
                int id = Convert.ToInt32(ResignID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Resign objDetails = context.tbl_Emp_Resign.First(d => d.ResignID == id);
                try
                {
                    // Update Table
                    objDetails.IsAccepted = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Resign request rejected !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult ResignedEmployee_List()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_Resign> EmpRList = new List<Emp_Resign>();
            try
            {
                EmpRList = context.tbl_Emp_Resign.Where(x => x.IsAccepted == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ResignDate < System.DateTime.Now).Select(s => new Emp_Resign
                {
                    ResignID = s.ResignID,
                    Employee_Code = s.Employee_Code,
                    Name = s.Name,
                    Email = s.Email,
                    Number = s.Number,
                    Department = s.Department,
                    Designation = s.Designation,
                    Reason = s.Reason,
                    ResignDate = s.ResignDate,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpRList);
        }

        public ActionResult Emp_Details_Edit()
        {
            return View();
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_All_Detail()
        {
            return View();
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_ContntactDetail()
        {
            Emp_Contact_Basic model = new Emp_Contact_Basic();
            model = getContactDetail(model);
            return PartialView(model);
        }

        private Emp_Contact_Basic getContactDetail(Emp_Contact_Basic model)
        {
            model.Emp_Contact_Basic_list = new List<Emp_Contact_Basic>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Contact_Basic> EmpCList = new List<Emp_Contact_Basic>();
            var collection = context.tbl_Emp_Contact_Basic.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Contact_Basic();
                model.ContactID = item.ContactID;
                model.Name = item.Name;
                model.DOB = item.DOB;
                model.Employee_Code = item.Employee_Code;
                model.Email = item.Email;
                model.Sex = item.Sex;
                model.Number = item.Number;
                EmpCList.Add(model);

            }
            model.Emp_Contact_Basic_list = EmpCList;
            return model;
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_QulaificationDetail()
        {
            Emp_Qual model = new Emp_Qual();
            model = getQualificationDetail(model);
            return PartialView(model);
        }

        private Emp_Qual getQualificationDetail(Emp_Qual model)
        {
            model.Emp_Qual_list = new List<Emp_Qual>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Qual> EmpCList = new List<Emp_Qual>();
            var collection = context.tbl_Emp_Qual_Employment.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Qual();
                model.QualID = item.QualID;
                model.Employee_Code = item.Employee_Code;
                model.College_University = item.College_University;
                model.Duration_from = item.Duration_from;
                model.Duration_to = item.Duration_to;

                model.Marks_Grade = item.Marks_Grade;

                int JobQualificationID = Convert.ToInt32(item.HighestDegree);
                model.HighestDegree = JobQualificationID;
                var objJobQual = context.tbl_JobQualification.FirstOrDefault(k => k.JobQualificationID == JobQualificationID && k.IsActive == true);
                if (objJobQual != null)
                {
                    model.HighestDegreeName = objJobQual.Name;
                }
                EmpCList.Add(model);

            }
            model.Emp_Qual_list = EmpCList;
            return model;
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_SkillsNEmployement()
        {
            Emp_Skills model = new Emp_Skills();
            model = getSkillNEmployement(model);
            return PartialView(model);
        }

        private Emp_Skills getSkillNEmployement(Emp_Skills model)
        {
            model.Emp_Skills_List = new List<Emp_Skills>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Skills> EmpCList = new List<Emp_Skills>();
            var collection = context.tbl_Emp_Skill_Employment.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Skills();
                model.SkillID = item.SkillID;
                model.Employee_Code = item.Employee_Code;
                model.Skills = item.Skills;
                model.TotalExperience = item.TotalExperience;
                model.WorkedFrom = item.WorkedFrom;
                model.WorkedTo = item.WorkedTo;
                model.PreviousCompany = item.PreviousCompany;
                model.PreviousCompanyAddress = item.PreviousCompanyAddress;
                model.LastDrawnSalary = item.LastDrawnSalary;
                EmpCList.Add(model);

            }
            model.Emp_Skills_List = EmpCList;
            return model;
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_ProbationNJoining()
        {
            Emp_Joining model = new Emp_Joining();
            model = getProbationJoiningDetail(model);
            return PartialView(model);
        }

        private Emp_Joining getProbationJoiningDetail(Emp_Joining model)
        {
            model.Emp_Joining_List = new List<Emp_Joining>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Joining> EmpCList = new List<Emp_Joining>();
            var collection = context.tbl_Emp_Joining_Probation.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Joining();
                model.JoiningID = item.JoiningID;
                model.Employee_Code = item.Employee_Code;
                model.OfficeLocation = item.OfficeLocation;
                model.ProbationPolicy = item.ProbationPolicy;
                model.Department = item.Department;
                model.Category = item.Category;
                model.Grade = item.Grade;
                model.Shift = item.Shift;
                model.IsAppraiser = item.IsAppraiser;
                model.JoiningDate = item.JoiningDate;
                model.FK_CategoryID = item.FK_CategoryID;
                model.FK_DepartmentID = item.FK_DepartmentID;
                model.FK_GradeID = item.FK_GradeID;
                model.FK_OfficeLocationID = item.FK_OfficeLocationID;
                model.FK_ProbationPolicyID = item.FK_ProbationPolicyID;
                model.FK_ShiftID = item.FK_ShiftID;


                EmpCList.Add(model);

            }
            model.Emp_Joining_List = EmpCList;
            return model;
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_SalaryNSatutary()
        {
            Emp_Salary model = new Emp_Salary();
            model = getSalaryNSatutaory(model);
            return PartialView(model);
        }

        private Emp_Salary getSalaryNSatutaory(Emp_Salary model)
        {
            model.Emp_Salary_list = new List<Emp_Salary>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Salary> EmpCList = new List<Emp_Salary>();
            var collection = context.tbl_Emp_Salary.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Salary();
                model.FK_PackageID = item.FK_PackageID;
                model.SalaryID = item.SalaryID;
                model.Employee_Code = item.Employee_Code;
                model.CTC = item.CTC;
                model.Monthly = item.Monthly;
                model.PFNumber = item.PFNumber;
                EmpCList.Add(model);

            }
            model.Emp_Salary_list = EmpCList;
            return model;
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_PassportNVisa()
        {
            Emp_Passport model = new Emp_Passport();
            model.Emp_Passport_List = new List<Emp_Passport>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Passport> EmpCList = new List<Emp_Passport>();
            var collection = context.tbl_Emp_Visa_Passport.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Passport();
                model.PassportID = item.PassportID;
                model.PassportNumber = item.PassportNumber;
                model.IssuingAuthority = item.IssuingAuthority;
                model.IssueFrom = item.IssueFrom;
                model.IssueTo = item.IssueTo;
                model.Citizenship = item.Citizenship;
                model.VisaNumber = item.VisaNumber;
                model.TypeofVisa = item.TypeofVisa;
                model.VisaValidFrom = item.VisaValidFrom;
                model.VisaValidTo = item.VisaValidTo;
                model.VisaCitizenship = item.VisaCitizenship;
                model.Remarks = item.Remarks;
                model.Employee_Code = item.Employee_Code;


                EmpCList.Add(model);

            }
            model.Emp_Passport_List = EmpCList;
            return PartialView(model);
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult View_UploadDocuments()
        {
            Emp_Documents model = new Emp_Documents();
            model.Emp_Documents_List = new List<Emp_Documents>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Documents> EmpCList = new List<Emp_Documents>();
            var collection = context.tbl_Emp_Documents.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            foreach (var item in collection)
            {
                model = new Emp_Documents();
                model.EmpDocID = item.EmpDocID;
                model.DocName = item.DocName;
                model.DocPath = item.DocPath;
                model.DocType = item.DocType;
                model.Employee_Code = item.Employee_Code;
                EmpCList.Add(model);

            }
            model.Emp_Documents_List = EmpCList;
            return PartialView(model);
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult EditEmployeee(string employeCode)
        {
            if (!string.IsNullOrEmpty(employeCode))
            {
                Session["employeCode"] = employeCode;
            }
            return View();
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Contact_Basic()
        {
            Emp_Contact_Basic model = new Emp_Contact_Basic();
            string employeCode = Session["employeCode"].ToString();
            if (employeCode != null)
            {
                employeCode = objglobal.Decrypt(employeCode);
                model = getContactDetail(model);
                if (model.Emp_Contact_Basic_list != null)
                {
                    model = model.Emp_Contact_Basic_list.FirstOrDefault<Emp_Contact_Basic>(m => m.Employee_Code == employeCode);
                }
                if (model == null)
                {
                    //TempData["ErrorMsg"] = "Employee not found";
                }
            }
            else
            {
                TempData["ErrorMsg"] = "Employee not found";
            }

            return PartialView(model);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        [HttpPost]
        public ActionResult Edit_Contact_Basic(Emp_Contact_Basic model)
        {

            try
            {

                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    var chkUser = context.tbl_Emp_Contact_Basic.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                    if (chkUser != null)
                    {
                        chkUser.Name = model.Name;
                        chkUser.Email = model.Email;
                        chkUser.Number = model.Number;
                        chkUser.DOB = model.DOB;
                        chkUser.Sex = model.Sex;
                        chkUser.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        chkUser.UpdatedBy = objSubs.Username;

                        context.Entry(chkUser).State = EntityState.Modified;
                        context.SaveChanges();
                        TempData["SuccessMsg"] = "Contact and Basic details of employee updated successfully !";

                        //return RedirectToAction("Employee_Add");
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "This Email or Contact Number already registered with us !";
                        //return RedirectToAction("Employee_Add");
                    }
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    //return RedirectToAction("Employee_Add");
                }

            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                //return RedirectToAction("Employee_Add");
            }



            return RedirectToAction("EditEmployeee");
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Qual_Emp()
        {
            Emp_Qual model = new Emp_Qual();

            string employeCode = Session["employeCode"].ToString();
            if (employeCode != null)
            {
                employeCode = objglobal.Decrypt(employeCode);
                model = getQualificationDetail(model);
                if (model.Emp_Qual_list != null)
                {
                    model = model.Emp_Qual_list.FirstOrDefault<Emp_Qual>(m => m.Employee_Code == employeCode);
                }
                if (model == null)
                {
                    //TempData["ErrorMsg"] = "Employee not found";
                }

            }
            else
            {
                TempData["ErrorMsg"] = "Employee not found";
            }


            ViewData["Qualification_List"] = QualificationList();
            return PartialView(model);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        [HttpPost]
        public ActionResult Edit_Qual_Emp(Emp_Qual model)
        {

            try
            {

                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    var chkUser = context.tbl_Emp_Qual_Employment.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                    if (chkUser != null)
                    {
                        chkUser.HighestDegree = model.HighestDegree;
                        chkUser.College_University = model.College_University;
                        chkUser.Duration_from = model.Duration_from;
                        chkUser.Duration_to = model.Duration_to;
                        chkUser.Marks_Grade = model.Marks_Grade;

                        chkUser.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        chkUser.UpdatedBy = objSubs.Username;

                        context.Entry(chkUser).State = EntityState.Modified;
                        context.SaveChanges();

                        TempData["SuccessMsg"] = "Qualification details of employee updated successfully !";

                        //return RedirectToAction("Employee_Add");
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "This Email or Contact Number already registered with us !";
                        //return RedirectToAction("Employee_Add");
                    }
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    //return RedirectToAction("Employee_Add");
                }

            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                //return RedirectToAction("Employee_Add");
            }
            ViewData["Qualification_List"] = QualificationList();


            return RedirectToAction("EditEmployeee");
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Skills_Emp()
        {

            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

            Emp_Skills model = new Emp_Skills();
            string employeCode = Session["employeCode"].ToString();
            if (employeCode != null)
            {
                employeCode = objglobal.Decrypt(employeCode);
                model = getSkillNEmployement(model);
                if (model.Emp_Skills_List != null)
                {
                    model = model.Emp_Skills_List.FirstOrDefault<Emp_Skills>(m => m.Employee_Code == employeCode);
                }
                if (model == null)
                {
                  //  TempData["ErrorMsg"] = "Employee not found";
                }

            }
            else
            {
                TempData["ErrorMsg"] = "Employee not found";
            }
            var jobSkill = GetEmployeSkillList();
            
            if (jobSkill.Count != 0)
            {
                model.JobSkill_list = jobSkill;
            }

            ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;


            return PartialView(model);

        }
        private List<JobSkill> GetEmployeSkillList()
        {
            List<JobSkill> empskills = new List<JobSkill>();
            List<JobSkill> objFinalList = new List<JobSkill>();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {

                empskills = context.tbl_JobSkill.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID)
                    .Select(s => new JobSkill
                    {
                        SkillID = s.SkillID,
                        Type = s.Type,
                    }).ToList();

                string Employee_Code = Session["employeCode"].ToString();
                Employee_Code = objglobal.Decrypt(Employee_Code);
                var empJobList = context.tbl_Emp_Skill_Employment.FirstOrDefault(x =>
                    x.IsActive == true &&
                    x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID &&
                    x.Employee_Code == Employee_Code);

                string[] splitString = { };
                if (empJobList != null)
                {
                    splitString = empJobList.Skills.Split(',');

                    foreach (var itemDistinct in empskills)
                    {
                        JobSkill obj = new JobSkill();
                        bool flag = false;
                        foreach (var item in splitString)
                        {
                            if (item.Trim() == itemDistinct.Type.Trim())
                            {
                                flag = true;
                                break;
                            }
                        }
                        obj.Type = itemDistinct.Type;
                        obj.SkillID = itemDistinct.SkillID;

                        if (flag)
                        {
                            obj.status = "checked";
                        }
                        objFinalList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);

            }
            return objFinalList;
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        [HttpPost]
        public ActionResult Edit_Skills_Emp(Emp_Skills model, FormCollection fc)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {

                //if (string.IsNullOrEmpty(Request["chkSkill"]))
                //{
                //    TempData["ErrorMsg"] = "Something went wrong !";
                //    return RedirectToAction("EditEmployeee");
                //}
                var chkboxValues = Request["chkSkill"].Split(',').ToList();
                model.Skills = string.Empty;
                foreach (var item in chkboxValues)
                {
                    model.Skills = model.Skills + item + ",";
                }
                var chkUser = context.tbl_Emp_Skill_Employment.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                if (chkUser != null)
                {

                    chkUser.Skills = model.Skills;
                    chkUser.TotalExperience = model.TotalExperience;
                    chkUser.PreviousCompany = model.PreviousCompany;
                    chkUser.WorkedFrom = model.WorkedFrom;
                    chkUser.WorkedTo = model.WorkedTo;
                    chkUser.LastDrawnSalary = model.LastDrawnSalary;
                    chkUser.PreviousCompanyAddress = model.PreviousCompanyAddress;
                    chkUser.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    chkUser.UpdatedBy = objSubs.Username;

                    context.Entry(chkUser).State = EntityState.Modified;
                    context.SaveChanges();

                    TempData["SuccessMsg"] = "Skill & employment details updated successfully !";

                    //return RedirectToAction("Employee_Add");
                }
                else
                {
                    TempData["ErrorMsg"] = "This Email or Contact Number already registered with us !";
                    //return RedirectToAction("Employee_Add");
                }


                ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;

            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;

            }


            return RedirectToAction("EditEmployeee");
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Probation_Joining()
        {

            Emp_Joining model = new Emp_Joining();

            string employeCode = Session["employeCode"].ToString();
            if (employeCode != null)
            {
                employeCode = objglobal.Decrypt(employeCode);
                model = getProbationJoiningDetail(model);
                if (model.Emp_Joining_List != null)
                {
                    model = model.Emp_Joining_List.FirstOrDefault<Emp_Joining>(m => m.Employee_Code == employeCode);
                }
                if (model == null)
                {
                    //TempData["ErrorMsg"] = "Employee not found";
                }
            }
            else
            {
                TempData["ErrorMsg"] = "Employee not found";
            }
            ViewData["Code_List_Join"] = JoinCodeList();
            ViewData["Location_List"] = LocationList();
            ViewData["Probation_Type_List"] = ProbationTypeList();
            ViewData["Department_List"] = DepartmentList();
            ViewData["Category_List"] = CategoryList();
            ViewData["Grade_List"] = GradeList();
            ViewData["Shift_List"] = ShiftList();
            return PartialView(model);

        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        [HttpPost]
        public ActionResult Edit_Probation_Joining(Emp_Joining model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    var chkUser = context.tbl_Emp_Joining_Probation.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                    if (chkUser != null)
                    {
                        chkUser.JoiningDate = model.JoiningDate;
                        chkUser.ProbationPolicy = model.ProbationPolicy;
                        chkUser.FK_ProbationPolicyID = model.FK_ProbationPolicyID;
                        chkUser.OfficeLocation = model.OfficeLocation;
                        chkUser.FK_OfficeLocationID = model.FK_OfficeLocationID;
                        chkUser.Department = model.Department;
                        chkUser.FK_DepartmentID = model.FK_DepartmentID;
                        chkUser.Category = model.Category;
                        chkUser.FK_CategoryID = model.FK_CategoryID;
                        chkUser.Grade = model.Grade;
                        chkUser.FK_GradeID = model.FK_GradeID;
                        chkUser.Shift = model.Shift;
                        chkUser.FK_ShiftID = Convert.ToInt32(model.FK_ShiftID);
                        chkUser.IsAppraiser = model.IsAppraiser;
                        chkUser.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        chkUser.UpdatedBy = objSubs.Username;

                        context.Entry(chkUser).State = EntityState.Modified;
                        context.SaveChanges();
                        TempData["SuccessMsg"] = "Probation and Joining details of employee saved successfully !";
                    }
                    else
                    { }

                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return RedirectToAction("EditEmployeee");
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Salary()
        {
            Emp_Salary model = new Emp_Salary();

            string employeCode = Session["employeCode"].ToString();
            if (employeCode != null)
            {
                employeCode = objglobal.Decrypt(employeCode);
                model = getSalaryNSatutaory(model);
                if (model.Emp_Salary_list != null)
                {
                    model = model.Emp_Salary_list.FirstOrDefault<Emp_Salary>(m => m.Employee_Code == employeCode);
                    
                }
                if (model == null)
                {
                    //TempData["ErrorMsg"] = "Employee not found";
                }
            }
            else
            {
                TempData["ErrorMsg"] = "Employee not found";
            }

            ViewData["Code_List_Salary"] = SalaryCodeList();
            ViewData["CTC_List_Salary"] = SalaryCTCList();

            return PartialView(model);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        [HttpPost]
        public ActionResult Edit_Salary(Emp_Salary model)
        {
            using (DbContextTransaction dbTran = context.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                        var Emp_Salary = context.tbl_Emp_Salary.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                        if (Emp_Salary != null)
                        {
                            Emp_Salary.FK_PackageID = model.FK_PackageID;
                            Emp_Salary.CTC = model.CTC;
                            Emp_Salary.Monthly = model.Monthly;
                            Emp_Salary.PFNumber = model.PFNumber;
                            Emp_Salary.IsActive = true;
                            Emp_Salary.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            Emp_Salary.UpdatedBy = objSubs.Username;
                            context.Entry(Emp_Salary).State = EntityState.Modified;
                            context.SaveChanges();
                        }


                        ItemForTAXComputation objIT = objglobal.GetITDetails(model.Employee_Code, model.FK_PackageID, model.CTC);

                        var TAXComputation = context.tbl_TAXComputation.FirstOrDefault(x => x.Employee_Code.Trim() == model.Employee_Code.Trim() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                        if (TAXComputation != null)
                        {
                            TAXComputation.TaxableAmount = objIT.TaxableAmount;
                            TAXComputation.Investment = objIT.Investment;
                            TAXComputation.YearlyTAXAmount = objIT.YearlyTAX;
                            TAXComputation.MonthlyTAXAmount = objIT.MonthlyTAX;
                            TAXComputation.FK_SlabMasterID = objIT.FK_SlabMasterID;
                            TAXComputation.ForYear = objglobal.TimeZone(objglobal.getTimeZone()).Year;
                            TAXComputation.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            TAXComputation.UpdatedBy = objSubs.Username;
                            context.Entry(TAXComputation).State = EntityState.Modified;
                            context.SaveChanges();
                        }

                        dbTran.Commit();
                        TempData["SuccessMsg"] = "Salary and Statutory details of employee saved successfully !";
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    dbTran.Rollback();

                }
                return RedirectToAction("EditEmployeee");
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Passport_Visa()
        {
            return PartialView();
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Document()
        {
            return PartialView();
        }






    }
}