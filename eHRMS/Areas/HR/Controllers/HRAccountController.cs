﻿using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Models;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRAccountController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/Account
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult ChangePassword()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            ChangePassword objlist = new ChangePassword();
            objlist.UserID = objSubs.Username;
            return View(objlist);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePassword model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            model.UserID = objSubs.Username;
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Password == model.Confirm_Password)
                    {
                        string crntpass = objglobal.Encrypt(model.CurrentPassword);
                        tbl_Login objlogin = context.tbl_Login.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Username == model.UserID).FirstOrDefault();
                        if (objlogin.Password == crntpass)
                        {
                            objlogin.Password = objglobal.Encrypt(model.Password);
                            objlogin.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            objlogin.UpdatedBy = objSubs.Username;
                            context.Entry(objlogin).State = EntityState.Modified;
                            context.SaveChanges();
                            ViewBag.SuccessMsg = "Password updated successfully !";
                        }
                        else
                        {
                            ViewBag.ErrorMsg = "Current Password you have entered is wrong !";
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "New Passwords did not match !";
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        #region LogOut
        [HttpGet]
        public ActionResult LogOut()
        {
            Session["User_ID"] = null;
            Session["ContactPerson"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Session.Abandon();

            return RedirectToAction("SignIn", "Account", new { area = "" });
        }
        #endregion
    }
}