﻿using eHRMS.Areas.HR.Models;
using eHRMS.Models;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Repository;
using System.Data.Entity;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRProjectController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/HRProject
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Project_Dashboard()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Projects_Management()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<CompanyProject> ProjectList = new List<CompanyProject>();
            try
            {
                ProjectList = context.tbl_CompanyProject.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyProject
                {
                    ProjectID = s.ProjectID,
                    ProjectName = s.ProjectName,
                    ProjectCode = s.ProjectCode,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(ProjectList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Projects_Management_Add(string id)
        {
            CompanyProject model = new CompanyProject();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetProjectDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        #region Get Project Details
        private CompanyProject GetProjectDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_CompanyProject objdetails = context.tbl_CompanyProject.Where(x => x.IsActive == true && x.ProjectID == ID).FirstOrDefault();

            CompanyProject objlist = new CompanyProject();
            if (objdetails != null)
            {
                objlist = new CompanyProject()
                {
                    ProjectCode = objdetails.ProjectCode,
                    ProjectName = objdetails.ProjectName,
                    Description = objdetails.Description,
                    StartDate = objdetails.StartDate,
                    EndDate = objdetails.EndDate
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Projects_Management_Add(CompanyProject model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int ProjectID = Convert.ToInt32(id);
                        tbl_CompanyProject objProjectDetails = context.tbl_CompanyProject.First(d => d.ProjectID == ProjectID);
                        // Update Table
                        objProjectDetails.ProjectCode = model.ProjectCode;
                        objProjectDetails.ProjectName = model.ProjectName;
                        objProjectDetails.Description = model.Description;
                        objProjectDetails.StartDate = model.StartDate;
                        objProjectDetails.EndDate = model.EndDate;
                        objProjectDetails.UpadatedOn = System.DateTime.Now;
                        objProjectDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objProjectDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Project details updated successfully !";
                    }
                    else if (context.tbl_CompanyProject.Any(x => x.ProjectCode.ToUpper() == model.ProjectCode.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Project already added !";
                    }
                    else
                    {
                        tbl_CompanyProject objProject = new tbl_CompanyProject()
                        {
                            ProjectCode = model.ProjectCode,
                            ProjectName = model.ProjectName,
                            Description = model.Description,
                            StartDate = model.StartDate,
                            EndDate = model.EndDate,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_CompanyProject.Add(objProject);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Project details added successfully !";
                        ModelState.Clear();
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        #region Delete Project
        public JsonResult DeleteProject(string ProjectID)
        {
            ProjectID = HttpUtility.UrlDecode(ProjectID);
            if (ProjectID != null)
            {
                int id = Convert.ToInt32(ProjectID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyProject objProjectDetails = context.tbl_CompanyProject.First(d => d.ProjectID == id);
                try
                {
                    // Update Table
                    objProjectDetails.IsActive = false;
                    objProjectDetails.UpdatedBy = objSubs.Username;
                    objProjectDetails.UpadatedOn = System.DateTime.Now;
                    context.Entry(objProjectDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Project details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion
    }
}