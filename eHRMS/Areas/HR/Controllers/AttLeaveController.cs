﻿using eHRMS.Areas.Employee.Models;
using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Models;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;


namespace eHRMS.Areas.HR.Controllers
{
    public class AttLeaveController : Controller
    {

        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult AttendanceLeaveDashboard()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Attendance_Request()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<AttendanceRequest> AttRList = new List<AttendanceRequest>();
            try
            {
                AttRList = context.tbl_Emp_AttRequest.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new AttendanceRequest
                {
                    Employee_Code = s.Employee_Code,
                    RequestID = s.RequestID,
                    Date = s.Date,
                    InTime = s.InTime,
                    OutTime = s.OutTime,
                    Remarks = s.Remarks,
                    IsApproved = s.IsApproved,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(AttRList);
        }

        #region Approve Attendance request
        public JsonResult ApproveAttn(string RequestID)
        {
            RequestID = HttpUtility.UrlDecode(RequestID);
            if (RequestID != null)
            {
                int id = Convert.ToInt32(RequestID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_AttRequest objDetails = context.tbl_Emp_AttRequest.First(d => d.RequestID == id);
                try
                {
                    if (context.tbl_Emp_Attendance.Any(x => x.Employee_Code == objDetails.Employee_Code && x.Date == objDetails.Date))
                    {
                        tbl_Emp_Attendance objAtDetails = context.tbl_Emp_Attendance.First(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objDetails.Employee_Code && x.Date == objDetails.Date);
                        if (objAtDetails.InTime != null && objAtDetails.OutTime != null)
                        {
                            // Update Table
                            objDetails.IsApproved = true;
                            objDetails.UpdatedBy = objSubs.Username;
                            objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            context.Entry(objDetails).State = EntityState.Modified;
                            context.SaveChanges();
                            //
                            objAtDetails.InTime = objDetails.InTime;
                            objAtDetails.OutTime = objDetails.OutTime;
                            objAtDetails.WorkingHours = (Convert.ToDateTime(objDetails.OutTime) - Convert.ToDateTime(objDetails.InTime)).ToString();
                            objAtDetails.UpdatedBy = objSubs.Username;
                            objAtDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            context.Entry(objAtDetails).State = EntityState.Modified;
                            context.SaveChanges();

                            ViewBag.SuccessMsg = "Attendance request approved !";
                        }
                        else if (objAtDetails.InTime == null && objAtDetails.OutTime == null)
                        {
                            // insert into table
                            tbl_Emp_Attendance objrsgn = new tbl_Emp_Attendance()
                            {
                                Date = objDetails.Date,
                                InTime = objDetails.InTime,
                                OutTime = objDetails.OutTime,
                                WorkingHours = (Convert.ToDateTime(objDetails.OutTime) - Convert.ToDateTime(objDetails.InTime)).ToString(),
                                FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                Employee_Code = objDetails.Employee_Code,
                                CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                CreatedBy = objSubs.Username
                            };
                            context.tbl_Emp_Attendance.Add(objrsgn);
                            context.SaveChanges();
                            ModelState.Clear();

                            // Update Attendance_request Table
                            objDetails.IsApproved = true;
                            objDetails.UpdatedBy = objSubs.Username;
                            objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            context.Entry(objDetails).State = EntityState.Modified;
                            context.SaveChanges();

                            // upadte leave table
                            tbl_Emp_Leave objtblEmp_Leave = context.tbl_Emp_Leave.Where(d => d.Employee_Code == objDetails.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.LeaveFrom <= objDetails.Date
                                && d.LeaveTo >= objDetails.Date).FirstOrDefault();
                            if (objtblEmp_Leave.LeaveDays > 0)
                            {
                                objtblEmp_Leave.Reason = objtblEmp_Leave.Reason + "<br/>Emp is present on " + objDetails.Date.Value.ToString("dd-MM-yyyy");
                                objtblEmp_Leave.LeaveDays = (objtblEmp_Leave.LeaveDays - 1);
                                objtblEmp_Leave.UpdatedBy = objSubs.Username;
                                objtblEmp_Leave.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                                context.Entry(objDetails).State = EntityState.Modified;
                                context.SaveChanges();

                                ViewBag.ErrorMsg = "Employee was in Leave but Attendance inserted successfully !";
                            }
                        }

                    }
                    else
                    {
                        // insert into table
                        tbl_Emp_Attendance objrsgn = new tbl_Emp_Attendance()
                        {
                            Date = objDetails.Date,
                            InTime = objDetails.InTime,
                            OutTime = objDetails.OutTime,
                            WorkingHours = (Convert.ToDateTime(objDetails.OutTime) - Convert.ToDateTime(objDetails.InTime)).ToString(),
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            Employee_Code = objDetails.Employee_Code,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Attendance.Add(objrsgn);
                        context.SaveChanges();
                        ModelState.Clear();

                        // Update Attendance_request Table
                        objDetails.IsApproved = true;
                        objDetails.UpdatedBy = objSubs.Username;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();

                        // upadte leave table
                        tbl_Emp_Leave objtblEmp_Leave = context.tbl_Emp_Leave.Where(d => d.Employee_Code == objDetails.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.LeaveFrom <= objDetails.Date
                            && d.LeaveTo >= objDetails.Date).FirstOrDefault();
                        if (objtblEmp_Leave.LeaveDays > 0)
                        {
                            objtblEmp_Leave.Reason = objtblEmp_Leave.Reason + "<br/>Emp is present on " + objDetails.Date.Value.ToString("dd-MM-yyyy");
                            objtblEmp_Leave.LeaveDays = (objtblEmp_Leave.LeaveDays - 1);
                            objtblEmp_Leave.UpdatedBy = objSubs.Username;
                            objtblEmp_Leave.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                            context.Entry(objDetails).State = EntityState.Modified;
                            context.SaveChanges();

                            ViewBag.ErrorMsg = "Employee was in Leave but Attendance inserted successfully !";
                        }

                        ViewBag.SuccessMsg = "Attendance request approved !";
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }


            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        #region Reject Attendance request
        public JsonResult RejectAttn(string RequestID)
        {
            RequestID = HttpUtility.UrlDecode(RequestID);
            if (RequestID != null)
            {
                int id = Convert.ToInt32(RequestID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_AttRequest objDetails = context.tbl_Emp_AttRequest.First(d => d.RequestID == id);
                try
                {
                    // Update Table
                    objDetails.IsApproved = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Leave request rejected !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        #region Update late  Approved
        public JsonResult UpdateLateApproved(string id, bool LateApproved)
        {
            string RequestID = HttpUtility.UrlDecode(id);
            if (RequestID != null)
            {
                int ID = Convert.ToInt32(RequestID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                var objDetails = context.tbl_Emp_Attendance.FirstOrDefault(d => d.ID == ID);
                try
                {
                    // Update Table
                    if (LateApproved)
                    {
                        objDetails.WorkingHoursInMin = 8 * 60;
                    }
                    else
                    {
                        objDetails.WorkingHoursInMin = Convert.ToDateTime(objDetails.WorkingHours).Hour * 60;
                    }


                    objDetails.LateApproved = LateApproved;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();
                    return Json("Late approved successfully !");

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    return Json("Something is wrong!");
                }
            }
            else
            {

                return Json("Something is wrong!");
            }
            return Json("Success");
        }
        #endregion

        #region Update Approved hour
        public JsonResult UpdateApprovedHour(string id, string ApprovedHours)
        {
            string RequestID = HttpUtility.UrlDecode(id);
            //if (RequestID != null && !string.IsNullOrEmpty(ApprovedHours))
            if (RequestID != null)
            {
                int ID = Convert.ToInt32(RequestID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                var objDetails = context.tbl_Emp_Attendance.FirstOrDefault(d => d.ID == ID);
                try
                {
                    // Update Table
                    if (string.IsNullOrEmpty(ApprovedHours))
                    {
                        objDetails.ApprovedHours = null;
                    }
                    else
                    {
                        objDetails.ApprovedHours = ApprovedHours;
                    }
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();
                    return Json("Approved hour save successfully !");

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    return Json("Something is wrong!");
                }
            }
            else
            {
                return Json(null);
            }
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Leave_Request()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_Leave> EmpLList = new List<Emp_Leave>();
            try
            {
                EmpLList = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Emp_Leave
                {
                    ID = s.ID,
                    Employee_Code = s.Employee_Code,
                    LeaveFrom = s.LeaveFrom,
                    LeaveTo = s.LeaveTo,
                    LeaveDays = s.LeaveDays,
                    AppliedFor = s.AppliedFor,
                    Reason = s.Reason,
                    IsApproved = s.IsApproved,
                    LeaveType = s.LeaveType
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpLList);
        }

        #region Approve Leave request
        public JsonResult ApproveRequest(string LeaveID)
        {
            LeaveID = HttpUtility.UrlDecode(LeaveID);
            if (LeaveID != null)
            {
                int id = Convert.ToInt32(LeaveID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Leave objDetails = context.tbl_Emp_Leave.First(d => d.ID == id);
                try
                {
                    // Update Table
                    objDetails.IsApproved = true;
                    objDetails.ApprovedBy = objSubs.Username;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();
                    // insert into Attendance table
                    if (objDetails.LeaveFrom == objDetails.LeaveTo)
                    {
                        tbl_Emp_Attendance objloc = new tbl_Emp_Attendance()
                        {
                            Date = objDetails.LeaveTo,
                            WorkingHours = "Leave approved for " + objDetails.AppliedFor,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            Employee_Code = objDetails.Employee_Code,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Attendance.Add(objloc);
                        context.SaveChanges();
                    }
                    else
                    {
                        int dates = Convert.ToInt32(objDetails.LeaveDays);

                        for (int i = 0; i < dates; i++)
                        {
                            //var v = i.ToString();
                            var LASTDATE = objDetails.LeaveFrom.Value.AddDays(i); ;
                            //var LASTDATE = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objDetails.Employee_Code && x.Date==objDetails.LeaveFrom).Select(s => s.Date).FirstOrDefault();
                            //var nxtdate = LASTDATE.Value.AddDays(1);
                            tbl_Emp_Attendance RQM = new tbl_Emp_Attendance()
                            {
                                Date = LASTDATE,
                                WorkingHours = "Leave approved for " + objDetails.AppliedFor,
                                FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                Employee_Code = objDetails.Employee_Code,
                                CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                CreatedBy = objSubs.Username
                            };
                            context.tbl_Emp_Attendance.Add(RQM);
                            context.SaveChanges();
                        }
                    }
                    ViewBag.SuccessMsg = "Leave request approved !";
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        #region Reject Leave request
        public JsonResult RejectRequest(string LeaveID)
        {
            LeaveID = HttpUtility.UrlDecode(LeaveID);
            if (LeaveID != null)
            {
                int id = Convert.ToInt32(LeaveID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Leave objDetails = context.tbl_Emp_Leave.First(d => d.ID == id);
                try
                {
                    // Update Table
                    objDetails.IsApproved = false;
                    objDetails.ApprovedBy = objSubs.Username;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Leave request rejected !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion



        #region Delete attendance
        public JsonResult DeleteAttendance(string attendID)
        {
            attendID = HttpUtility.UrlDecode(attendID);
            if (attendID != null)
            {
                int id = Convert.ToInt32(attendID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Attendance objDetails = context.tbl_Emp_Attendance.FirstOrDefault(d => d.ID == id);
                try
                {
                    // Delete record from attendance Table
                    if (objDetails != null)
                    {
                        context.tbl_Emp_Attendance.Remove(objDetails);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Leave request rejected !";
                        return Json("Leave request rejected !");
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                    return Json("Something is wrong!");
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";

            }
            return Json("Id not Found");

        }
        #endregion



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Track_Attendance()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            return View();
        }
        public PartialViewResult Track_Attendance_View()
        {
            List<TrackAtt> track_List = new List<TrackAtt>();
            return PartialView(track_List);
        }

        [HttpPost]
        public PartialViewResult Track_Attendance_View(TrackAtt model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<TrackAtt> track_List = new List<TrackAtt>();


            if (ModelState.IsValid)
            {
                var month = Convert.ToInt32(model.Month);
                var year = Convert.ToInt32(model.Year);
                try
                {
                    track_List = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.Date.Value.Month == month && x.Date.Value.Year == year).Select(s => new TrackAtt
                    {
                        ID = s.ID,
                        Employee_Code = s.Employee_Code,
                        Date = s.Date,
                        InTime = s.InTime,
                        OutTime = s.OutTime,
                        Status = s.Status,
                        WorkingHours = s.WorkingHours,
                        OTHours = s.OTHours,
                        //LateApproved =  s.LateApproved,
                        ApprovedHours = s.ApprovedHours,

                    }).ToList();
                    if (track_List.Count <= 0)
                    {
                        ViewBag.ErrorMsg = "No Record Found !";
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
                return PartialView(track_List);
            }
            else
            {
                ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                return PartialView(track_List);
            }
        }

        public List<SelectListItem> AllEmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Salary.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Track_Leave()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            return View();
        }

        public PartialViewResult Track_Leave_Holidays()
        {
            List<LeaveBalance> Bln_List = new List<LeaveBalance>();
            return PartialView(Bln_List);
        }

        [HttpPost]
        public PartialViewResult Track_Leave_Holidays(Track_Leave model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<LeaveBalance> Bln_List = new List<LeaveBalance>();
            if (ModelState.IsValid)
            {
                var year = Convert.ToInt32(model.Year);
                if (context.tbl_Emp_Joining_Probation.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code))
                {
                    var DepartmentID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code).FirstOrDefault().FK_DepartmentID;
                    try
                    {
                        var objSettings_AssignLeave = context.tbl_Settings_AssignLeave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.IsActive == true && x.FK_DepartmentID == DepartmentID).ToList();

                        foreach (var item in objSettings_AssignLeave)
                        {
                            var objLeaveBalance = new LeaveBalance();
                            var objEmp_Leave = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == item.Type).ToList();
                            var totalHalf = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Half").ToList().Sum(x => x.LeaveDays ?? 0));
                            var totalFull = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Full").ToList().Sum(x => x.LeaveDays ?? 0));
                            var totalLeave = Convert.ToDecimal((totalHalf * 0.5) + (totalFull * 1));

                            objLeaveBalance.AppliedFor = item.Type;
                            objLeaveBalance.LeaveAllowed = item.NoOfLeave ?? 0;
                            objLeaveBalance.LeaveTaken = totalLeave;
                            objLeaveBalance.LeaveDue = objLeaveBalance.LeaveAllowed - totalLeave;
                            Bln_List.Add(objLeaveBalance);
                            //objLeaveBalance.LeaveTaken = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == s.Type).Sum(x => x.LeaveDays ?? 0),
                            // objLeaveBalance.LeaveDue = (context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.FK_DepartmentID == DepartmentID && x.Type == s.Type).FirstOrDefault().NoOfLeave ?? 0) - (context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == s.Type).Sum(x => x.LeaveDays ?? 0)),
                            //.ToList();
                        }



                    }
                    catch (Exception ex)
                    {
                        objError.LogError(ex);
                        ViewBag.ErrorMsg = "Something went wrong !";
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Please assign a Department to the selected Employee !";
                }

                return PartialView(Bln_List);
            }
            else
            {
                ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                return PartialView(Bln_List);
            }
        }

        public PartialViewResult Track_Leave_View()
        {
            List<Emp_Leave> track_List = new List<Emp_Leave>();
            return PartialView(track_List);
        }

        [HttpPost]
        public PartialViewResult Track_Leave_View(Track_Leave model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Leave> EmpLList = new List<Emp_Leave>();
            if (ModelState.IsValid)
            {
                try
                {
                    var year = Convert.ToInt32(model.Year);
                    EmpLList = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true).Select(s => new Emp_Leave
                    {
                        LeaveFrom = s.LeaveFrom,
                        LeaveTo = s.LeaveTo,
                        LeaveDays = s.LeaveDays,
                        AppliedFor = s.AppliedFor,
                        Reason = s.Reason,
                        LeaveType = s.LeaveType,
                        ApprovedBy = s.ApprovedBy,
                    }).ToList();
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
                return PartialView(EmpLList);
            }
            else
            {
                ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                return PartialView(EmpLList);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Holiday()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Holiday> Holiday_List = new List<Holiday>();
            List<Holiday> MainHoliday_List = new List<Holiday>();

            try
            {
                Holiday_List = context.tbl_SettingsHolidays.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Year == DateTime.Now.Year.ToString() && x.SameDayEveryYear == false).Select(s => new Holiday
                {
                    Date = s.Date,
                    Title = s.Title,
                    Details = s.Details,
                    SameDayEveryYear = s.SameDayEveryYear,
                }).ToList();

                MainHoliday_List.AddRange(Holiday_List);

                Holiday_List = context.tbl_SettingsHolidays.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.SameDayEveryYear == true).Select(s => new Holiday
                {
                    Date = s.Date,
                    Title = s.Title,
                    Details = s.Details,
                    SameDayEveryYear = s.SameDayEveryYear,
                }).ToList();

                MainHoliday_List.AddRange(Holiday_List);


            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(MainHoliday_List);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Holiday_Add()
        {
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Holiday_Add(Holiday model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_SettingsHolidays.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Date == model.Date))
                    {
                        ViewBag.ErrorMsg = "Holiday already added !";
                    }
                    else if (context.tbl_SettingsHolidays.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Date.Value.Day == model.Date.Value.Day && x.Date.Value.Month == model.Date.Value.Month && x.SameDayEveryYear == true))
                    {
                        ViewBag.ErrorMsg = "Holiday already added for every year for same day !";
                    }
                    else
                    {
                        tbl_SettingsHolidays objprc = new tbl_SettingsHolidays()
                        {
                            Title = model.Title,
                            Date = model.Date,
                            Year = model.Date.Value.ToString("yyyy"),
                            Details = model.Details,
                            SameDayEveryYear = model.SameDayEveryYear,
                            IsActive = true,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_SettingsHolidays.Add(objprc);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Holiday added successfully !";

                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Weekly_Offs()
        {
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Weekly_Offs(WeeklyOffs model)
        {
            try
            {
                //string a = "apu,Dipu,sourav";
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_SettingsOffDays.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Type == model.Type && x.Day == model.Day))
                    {
                        ViewBag.ErrorMsg = "Already added for this combination !";
                    }
                    else
                    {
                        tbl_SettingsOffDays objprc = new tbl_SettingsOffDays()
                        {
                            Day = model.Day,
                            Type = model.Type,
                            First = model.First,
                            Second = model.Second,
                            Third = model.Third,
                            Fourth = model.Fourth,
                            Fifth = model.Fifth,
                            IsActive = true,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_SettingsOffDays.Add(objprc);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Offdays added successfully !";

                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult Weekly_Offs_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<WeeklyOffs> Offs_List = new List<WeeklyOffs>();
            try
            {
                Offs_List = context.tbl_SettingsOffDays.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new WeeklyOffs
                {
                    ID = s.ID,
                    Day = s.Day,
                    Type = s.Type,
                    First = s.First,
                    Second = s.Second,
                    Third = s.Third,
                    Fourth = s.Fourth,
                    Fifth = s.Fifth,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Offs_List);
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assign_Leave()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<AssignLeave> Assign_List = new List<AssignLeave>();
            try
            {
                Assign_List = context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new AssignLeave
                {
                    ID = s.ID,
                    Type = s.Type,
                    NoOfLeave = s.NoOfLeave,
                    FK_DepartmentID = s.FK_DepartmentID,
                    Department = s.Department,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(Assign_List);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assign_Leave_Add()
        {
            ViewData["List_LeaveCat"] = AllLeaveCat();
            ViewData["Department_List"] = DepartmentList();
            return View();
        }

        public List<SelectListItem> AllLeaveCat()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Leave_Category.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.CategoryName, Value = item.CategoryName });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> AllLeaveType()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                objSelLst.Add(new SelectListItem { Text = "Half", Value = "Half" });
                objSelLst.Add(new SelectListItem { Text = "Full", Value = "Full" });
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> DepartmentList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobDepartment.Where(c => c.Fk_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.DepartmentID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assign_Leave_Add(AssignLeave model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_Settings_AssignLeave.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Type == model.Type && x.Department == model.Department))
                    {
                        ViewData["List_LeaveCat"] = AllLeaveCat();
                        ViewData["Department_List"] = DepartmentList();
                        ViewBag.ErrorMsg = "Leave already assigned for this combination !";
                    }
                    else
                    {
                        tbl_Settings_AssignLeave objprc = new tbl_Settings_AssignLeave()
                        {
                            Type = model.Type,
                            NoOfLeave = model.NoOfLeave,
                            FK_DepartmentID = model.FK_DepartmentID,
                            Department = model.Department,
                            IsActive = true,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Settings_AssignLeave.Add(objprc);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Leave Assigned successfully !";

                    }
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    ViewData["Department_List"] = DepartmentList();
                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    ViewData["Department_List"] = DepartmentList();
                    ModelState.Clear();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["List_LeaveCat"] = AllLeaveCat();
                ViewData["Department_List"] = DepartmentList();
                ModelState.Clear();
                return View(model);
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Settings()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            return View(GetSettingsDetails(objSubs.FK_CompanyRegistrationID));
        }

        #region Get SettingsDetails
        private AttLeave_Settings GetSettingsDetails(string id)
        {
            tbl_Settings_AttLeave objdetails = context.tbl_Settings_AttLeave.Where(x => x.FK_CompanyRegistrationID == id).FirstOrDefault();

            AttLeave_Settings objlist = new AttLeave_Settings();
            if (objdetails != null)
            {
                objlist = new AttLeave_Settings()
                {
                    SettingsID = objdetails.SettingsID,
                    AttendanceCanNotUpdate = objdetails.AttendanceCanNotUpdate,
                    NoDeductionHalfdayApprove = objdetails.NoDeductionHalfdayApprove,
                    LateEntryInMonth = objdetails.LateEntryInMonth,
                    WeeklyHours = objdetails.WeeklyHours,
                    CountLateEntryAfter = objdetails.CountLateEntryAfter,
                    NoDeductionIfCompleteWorkingHours = objdetails.NoDeductionIfCompleteWorkingHours,
                    PayOvertime = objdetails.PayOvertime,
                    AmountForOvertime = objdetails.AmountForOvertime,
                    ApplyLeaveBefore = objdetails.ApplyLeaveBefore,
                    UnapprovedLeaveDeduct = objdetails.UnapprovedLeaveDeduct,
                    PendingLeaves = objdetails.PendingLeaves,
                    AmountForPendingLeaves = objdetails.AmountForPendingLeaves,
                    HO_WorkingEmployeeGet = objdetails.HO_WorkingEmployeeGet,
                    AmountFor_HO = objdetails.AmountFor_HO,
                };
                return objlist;
            }
            else
            {
                //ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Settings(AttLeave_Settings model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_Settings_AttLeave.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        tbl_Settings_AttLeave objstng = context.tbl_Settings_AttLeave.First(d => d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                        objstng.AttendanceCanNotUpdate = model.AttendanceCanNotUpdate;
                        objstng.NoDeductionHalfdayApprove = model.NoDeductionHalfdayApprove;
                        objstng.LateEntryInMonth = model.LateEntryInMonth;
                        objstng.WeeklyHours = model.WeeklyHours;
                        objstng.CountLateEntryAfter = model.CountLateEntryAfter;
                        objstng.NoDeductionIfCompleteWorkingHours = model.NoDeductionIfCompleteWorkingHours;
                        objstng.PayOvertime = model.PayOvertime;
                        objstng.AmountForOvertime = model.AmountForOvertime;
                        objstng.ApplyLeaveBefore = model.ApplyLeaveBefore;
                        objstng.UnapprovedLeaveDeduct = model.UnapprovedLeaveDeduct;
                        objstng.PendingLeaves = model.PendingLeaves;
                        objstng.AmountForPendingLeaves = model.AmountForPendingLeaves;
                        objstng.HO_WorkingEmployeeGet = model.HO_WorkingEmployeeGet;
                        objstng.AmountFor_HO = model.AmountFor_HO;
                        objstng.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objstng.UpdatedBy = objSubs.Username;
                        context.Entry(objstng).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Settings updated successfully !";
                    }
                    else
                    {
                        tbl_Settings_AttLeave objstng = new tbl_Settings_AttLeave()
                        {
                            AttendanceCanNotUpdate = model.AttendanceCanNotUpdate,
                            NoDeductionHalfdayApprove = model.NoDeductionHalfdayApprove,
                            LateEntryInMonth = model.LateEntryInMonth,
                            WeeklyHours = model.WeeklyHours,
                            CountLateEntryAfter = model.CountLateEntryAfter,
                            NoDeductionIfCompleteWorkingHours = model.NoDeductionIfCompleteWorkingHours,
                            PayOvertime = model.PayOvertime,
                            AmountForOvertime = model.AmountForOvertime,
                            ApplyLeaveBefore = model.ApplyLeaveBefore,
                            UnapprovedLeaveDeduct = model.UnapprovedLeaveDeduct,
                            PendingLeaves = model.PendingLeaves,
                            AmountForPendingLeaves = model.AmountForPendingLeaves,
                            HO_WorkingEmployeeGet = model.HO_WorkingEmployeeGet,
                            AmountFor_HO = model.AmountFor_HO,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Settings_AttLeave.Add(objstng);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Settings saved successfully !";
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Leave_Category()
        {
            return View();
        }
        public PartialViewResult Leave_Category_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<LeaveCategory> Cat_List = new List<LeaveCategory>();
            try
            {
                Cat_List = context.tbl_Leave_Category.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new LeaveCategory
                {
                    LCID = s.LCID,
                    CategoryName = s.CategoryName,
                    CanApply = s.CanApply,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Cat_List);
        }

        #region Delete Leave Category
        public JsonResult DeleteLeaveCategory(string LCID)
        {
            LCID = HttpUtility.UrlDecode(LCID);
            if (LCID != null)
            {
                int id = Convert.ToInt32(LCID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Leave_Category objDetails = context.tbl_Leave_Category.First(d => d.LCID == id);
                try
                {
                    // Update Table
                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Leave category deleted !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [HttpPost]
        public ActionResult Leave_Category(LeaveCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_Leave_Category.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.CategoryName.ToUpper() == model.CategoryName.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Leave Category already added !";
                    }
                    else
                    {
                        tbl_Leave_Category objcat = new tbl_Leave_Category()
                        {
                            CategoryName = model.CategoryName,
                            CanApply = model.CanApply,
                            IsActive = true,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Leave_Category.Add(objcat);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Leave Category added successfully !";

                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Working_Shift()
        {

            return View();
        }


        public PartialViewResult Working_Shift_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Work_Shift> Shift_List = new List<Work_Shift>();
            try
            {
                Shift_List = context.tbl_WorkShift.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Work_Shift
                {
                    ShiftID = s.ShiftID,
                    ShiftName = s.ShiftName,
                    StartTime = s.StartTime,
                    EndTime = s.EndTime,
                    WorkingTime = s.WorkingTime,
                    IsOutNextDay = (s.IsOutNextDay.HasValue) ? true : false,
                    HDWorkingTime = s.HDWorkingTime,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Shift_List);
        }

        #region Delete Shift
        public JsonResult DeleteShift(string ShiftID)
        {
            ShiftID = HttpUtility.UrlDecode(ShiftID);
            if (ShiftID != null)
            {
                int id = Convert.ToInt32(ShiftID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_WorkShift objDetails = context.tbl_WorkShift.First(d => d.ShiftID == id);
                try
                {
                    // Update Table
                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Working Shift deleted !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [HttpPost]
        public ActionResult Working_Shift(Work_Shift model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();


                    if (context.tbl_WorkShift.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ShiftName.ToUpper() == model.ShiftName.ToUpper()))
                    {

                        ViewBag.ErrorMsg = "Leave Category already added !";
                    }
                    else
                    {
                        tbl_WorkShift objshift = new tbl_WorkShift()
                        {
                            ShiftName = model.ShiftName,
                            StartTime = model.StartTime,
                            EndTime = model.EndTime,
                            WorkingTime = model.WorkingTime,
                            HDWorkingTime = model.HDWorkingTime,
                            IsActive = true,
                            IsOutNextDay = model.IsOutNextDay,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_WorkShift.Add(objshift);
                        context.SaveChanges();
                        ModelState.Clear();

                        ViewBag.SuccessMsg = "Working shift added successfully !";

                    }
                    return View(model);
                }
                else
                {

                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {

                objError.LogError(ex);
                return View(model);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Manual_Attendance()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            ViewData["InOutDropdown"] = getInOutDropdown();
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Manual_Attendance(AttendanceRequest model)
        {
            try
            {
                //model.Remarks = "Manual Insert by HR !";
                //if (ModelState.IsValid)
                //{
                //DateTime date = objglobal.TimeZone(objEmp.TimeZone).Date;
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Attendance objDetails = context.tbl_Emp_Attendance.Where(d => d.Employee_Code == model.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Status == "In").FirstOrDefault();
                //tbl_Emp_Attendance chkDuplicateLog = context.tbl_Emp_Attendance.Where(d => d.Employee_Code == model.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Date == model.Date && d.Status == "Out").FirstOrDefault();
                //if (chkDuplicateLog != null)
                //{
                //    ViewBag.ErrorMsg = "Employee attendance allready exist on "+model.Date.Value.ToString("dd-MM-yyyy");
                //    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                //    ViewData["InOutDropdown"] = getInOutDropdown();
                //    return View(model);

                //}


                DateTime attedanceTime = Convert.ToDateTime(model.Date.Value.Date.ToShortDateString() + " " + model.InTime);
                //DateTime outTime = Convert.ToDateTime(model.Date.Value.Date.ToShortDateString() + " " + model.OutTime);
                //TimeSpan workingHours = outTime.Subtract(inTime);

                if (ModelState.IsValid)
                {
                    if (objDetails != null)
                    {
                        if (model.Status == "In")
                        {
                            ViewBag.ErrorMsg = "This Employee Out time is not present.!";
                            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                            ViewData["InOutDropdown"] = getInOutDropdown();
                            return View(model);
                        }
                        DateTime inTime = Convert.ToDateTime(objDetails.InTime);
                        TimeSpan workingHours = attedanceTime.Subtract(inTime);

                        if (workingHours.ToString().Contains("-"))
                        {
                            ViewBag.ErrorMsg = "Something went wrong";
                            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                            ViewData["InOutDropdown"] = getInOutDropdown();
                            return View(model);
                        }

                        TimeSpan shiftHour = new TimeSpan(8, 0, 0);
                        string ExactOtHour = string.Empty;
                        if (workingHours.Hours > 8)
                        {
                            ExactOtHour = workingHours.Subtract(shiftHour).ToString();
                            if (ExactOtHour.Contains("-"))
                            {
                                ExactOtHour = "00:00:00";
                            }
                        }
                        objDetails.OutTime = attedanceTime.ToString();
                        objDetails.WorkingHours = workingHours.ToString();
                        objDetails.WorkingHoursInMin = Convert.ToDecimal(workingHours.TotalMinutes);
                        objDetails.Status = model.Status;
                        objDetails.OTHours = ExactOtHour;
                        objDetails.UpdatedBy = objSubs.Username;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Attendance logged Out successfully";

                    }
                    else
                    {
                        tbl_Emp_Attendance objEmp_Attendance = new tbl_Emp_Attendance()
                          {
                              Date = model.Date,
                              InTime = attedanceTime.ToString(),
                              Status = model.Status,
                              FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                              Employee_Code = model.Employee_Code,
                              CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                              CreatedBy = objSubs.Username
                          };
                        context.tbl_Emp_Attendance.Add(objEmp_Attendance);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Attendance logged In successfully";
                    }
                }
                //if (objDetails != null)
                //{
                //    if (objDetails.InTime != null && objDetails.OutTime != null)
                //    {
                //        objDetails.InTime = model.InTime;
                //        objDetails.OutTime = model.OutTime;
                //        objDetails.WorkingHours = getWorkingHours(model, objSubs);
                //        objDetails.UpdatedBy = objSubs.Username;
                //        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                //        context.Entry(objDetails).State = EntityState.Modified;
                //        context.SaveChanges();
                //        //ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                //        ViewBag.ErrorMsg = "Attendance already inserted on this date and updated successfully !";

                //    }
                //    else if (objDetails.InTime == null && objDetails.OutTime == null)
                //    {
                //        // update table
                //        objDetails.InTime = model.InTime;
                //        objDetails.OutTime = model.OutTime;
                //        objDetails.WorkingHours = getWorkingHours(model, objSubs);
                //        objDetails.UpdatedBy = objSubs.Username;
                //        objDetails.UpdatedOn = System.DateTime.Now;
                //        context.Entry(objDetails).State = EntityState.Modified;
                //        context.SaveChanges();
                //        //ViewData["Code_List_Employee"] = AllEmployeeCodeList();


                //        tbl_Emp_Leave objtblEmp_Leave = context.tbl_Emp_Leave.Where(d => d.Employee_Code == model.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.LeaveFrom <= model.Date
                //            && d.LeaveTo >= model.Date).FirstOrDefault();
                //        if (objtblEmp_Leave.LeaveDays > 0)
                //        {
                //            objtblEmp_Leave.Reason = objtblEmp_Leave.Reason + "<br/>Emp is present on " + model.Date.Value.ToString("dd-MM-yyyy");
                //            objtblEmp_Leave.LeaveDays = (objtblEmp_Leave.LeaveDays - 1);
                //            objtblEmp_Leave.UpdatedBy = objSubs.Username;
                //            objtblEmp_Leave.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                //            context.Entry(objDetails).State = EntityState.Modified;
                //            context.SaveChanges();

                //            ViewBag.ErrorMsg = "Employee was in Leave but Attendance inserted successfully !";
                //        }

                //    }
                //}
                //else
                //{
                //    tbl_Emp_Attendance objrsgn = new tbl_Emp_Attendance()
                //    {
                //        Date = model.Date,
                //        InTime = model.InTime,
                //        OutTime = model.OutTime,
                //        WorkingHours = getWorkingHours(model, objSubs),
                //        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                //        Employee_Code = model.Employee_Code,
                //        CreatedOn = System.DateTime.Now,
                //        CreatedBy = objSubs.Username
                //    };
                //    context.tbl_Emp_Attendance.Add(objrsgn);
                //    context.SaveChanges();
                //    ModelState.Clear();
                //    ViewBag.SuccessMsg = "Attendance inserted successfully !";
                //}
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                ViewData["InOutDropdown"] = getInOutDropdown();
                return View(model);
                //}
                //else
                //{
                //ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                //ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                //return View(model);
                //}
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View(model);
            }
        }
        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult uploadByExcel(HttpPostedFileBase excelFile)
        {
            string Message, fileName, actualFileName;
            Message = fileName = actualFileName = string.Empty;
            bool flag = false;
            string path = string.Empty;
            string direcoryPath = "~/Content/Emp_Document/";
            if (excelFile != null)
            {
                //var file = Request.Files[0];
                actualFileName = excelFile.FileName;
                fileName = Guid.NewGuid() + Path.GetExtension(excelFile.FileName);
                int size = excelFile.ContentLength;

                try
                {
                    string Dirurl = Server.MapPath(direcoryPath);
                    if (!Directory.Exists(Dirurl))
                        Directory.CreateDirectory(Dirurl);
                    path = Path.Combine(Dirurl, fileName);

                    excelFile.SaveAs(path);
                    path = direcoryPath + fileName;
                    SaveToExecl(path);

                }
                catch (Exception ex)
                {
                    path = string.Empty;
                    Message = "File upload failed! Please try again";
                }
            }

            return RedirectToAction("Manual_Attendance", new AttendanceRequest());
        }

        public bool SaveToExecl(string filePath)
        {
            DataTable dt = new DataTable();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();


            string extension = Path.GetExtension(filePath);

            string conn = string.Empty;

            if (extension.CompareTo(".xls") == 0)//compare the extension of the file
                conn = @"provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath(filePath) + ";Extended Properties='Excel 8.0;HRD=Yes;IMEX=1';";//for below excel 2007
            else
                conn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(filePath) + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=1';";//for above excel 2007
            using (OleDbConnection con = new OleDbConnection(conn))
            {
                try
                {
                    OleDbDataAdapter oleAdpt = new OleDbDataAdapter("select * from [Sheet1$]", con);//here we read data from sheet1
                    oleAdpt.Fill(dt);//fill excel data into dataTable
                }
                catch (Exception ex)
                {
                    TempData["ErrorMsg"] = "Internal error" + ex.Message;
                    objError.LogError(ex);
                }
            }
            TempData["SuccessMsg"] = "Row found:" + dt.Rows.Count;
            using (DbContextTransaction dbTran = context.Database.BeginTransaction())
            {
                int countRow = 1;
                try
                {

                    foreach (DataRow item in dt.Rows)
                    {
                        countRow++;
                        DateTime inTime = Convert.ToDateTime(Convert.ToDateTime(item["InDate"]).ToString("MM/dd/yyyy") + " " + Convert.ToDateTime(item["InTime"]).ToString("HH:mm tt"));
                        DateTime outTime = Convert.ToDateTime(Convert.ToDateTime(item["OutDate"]).ToString("MM/dd/yyyy") + " " + Convert.ToDateTime(item["OutTime"]).ToString("HH:mm tt")); ;
                        TimeSpan workingHours = outTime.Subtract(inTime);
                        if (workingHours.ToString().Contains("-") || workingHours.Days > 0)
                        {
                            TempData["ErrorMsg"] = "Something went wrong on row: " + countRow;
                            return false;
                        }
                        TimeSpan shiftHour = new TimeSpan(8, 0, 0);
                        string ExactOtHour = string.Empty;


                        tbl_Emp_Attendance objDetails = new tbl_Emp_Attendance();
                        objDetails.Date = Convert.ToDateTime(item["InDate"]);
                        objDetails.Employee_Code = item["EmpCode"].ToString();
                        objDetails.InTime = inTime.ToString();
                        objDetails.OutTime = outTime.ToString();
                        objDetails.WorkingHours = workingHours.ToString();
                        if (workingHours.TotalMinutes > 8 * 60)
                        {
                            objDetails.WorkingHoursInMin = 8 * 60;//8 HOURS CONVERTED INTO MINUTE
                            objDetails.OTHours = workingHours.Subtract(shiftHour).ToString();
                        }
                        else
                        {
                            objDetails.WorkingHoursInMin = Convert.ToDecimal(workingHours.TotalMinutes);
                        }

                        objDetails.Status = "Out";
                        objDetails.FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID;
                        objDetails.CreatedBy = objSubs.Username;
                        objDetails.CreatedOn = objglobal.TimeZone(objglobal.getTimeZone());

                        context.tbl_Emp_Attendance.Add(objDetails);
                        context.SaveChanges();
                    }
                    dbTran.Commit();
                    TempData["SuccessMsg"] = "Attendance uploaded successfully";
                    return true;
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    TempData["ErrorMsg"] = "Something went wrong on row: " + countRow + Environment.NewLine + "Error:" + ex.Message;
                    objError.LogError(ex);
                }
            }
            return false;


        }
        public static void getExcelFile(string path)
        {

            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(path);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;


            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;

            //iterate over the rows and columns and print to the console as it appears in the file
            //excel is not zero based!!
            DataTable dt = new DataTable();
            //dt.Columns.Add("EmpCode", typeof(string));
            //dt.Columns.Add("InDate", typeof(DateTime));
            //dt.Columns.Add("InTime", typeof(DateTime));
            //dt.Columns.Add("OutDate", typeof(DateTime));
            //dt.Columns.Add("OutTime", typeof(DateTime));
            for (int i = 1; i <= rowCount; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 1; j <= colCount; j++)
                {
                    //new line
                    if (j == 1)
                        Console.Write("\r\n");

                    if (i == 1)
                    {
                        dt.Columns.Add(xlRange.Cells[i, j].Value2.ToString());
                    }
                    else
                    {
                        dr[j - 1] = xlRange.Cells[i, j].Value2;
                    }
                    //write the value to the console
                    if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j].Value2 != null)
                        Console.Write(xlRange.Cells[i, j].Value2.ToString() + "\t");
                }
                dt.Rows.Add(dr);
            }

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //rule of thumb for releasing com objects:
            //  never use two dots, all COM objects must be referenced and released individually
            //  ex: [somthing].[something].[something] is bad

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }
        public List<SelectListItem> getInOutDropdown()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                objSelLst.Add(new SelectListItem { Text = "In Time", Value = "In" });
                objSelLst.Add(new SelectListItem { Text = "Out Time", Value = "Out" });
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        private string getWorkingHours(AttendanceRequest model, SubscriptionModel objSubs)
        {
            string WorkingHours = string.Empty;
            var Emp_Joining_Probation = context.tbl_Emp_Joining_Probation.FirstOrDefault(m => m.Employee_Code == model.Employee_Code && m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);

            if (Emp_Joining_Probation != null)
            {
                var Emp_WorkShift = context.tbl_WorkShift.FirstOrDefault(m => m.ShiftID == Emp_Joining_Probation.FK_ShiftID && m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                if (Emp_WorkShift != null)
                {

                    if (Emp_WorkShift.IsOutNextDay == true)
                    {
                        WorkingHours = (Convert.ToDateTime(model.OutTime).AddDays(1) - Convert.ToDateTime(model.InTime)).ToString();
                    }
                    else
                    {
                        WorkingHours = (Convert.ToDateTime(model.OutTime) - Convert.ToDateTime(model.InTime)).ToString();
                    }
                }
                else
                {
                    WorkingHours = (Convert.ToDateTime(model.OutTime) - Convert.ToDateTime(model.InTime)).ToString();
                }
            }
            return WorkingHours;
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Manual_Leave()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            ViewData["List_LeaveCat"] = AllLeaveCat();
            ViewData["List_LeaveType"] = AllLeaveType();
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Manual_Leave(Emp_Leave model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    

                    var r1 = context.tbl_Emp_Leave.FirstOrDefault(z => z.LeaveFrom >= model.LeaveFrom.Value && z.LeaveTo <= model.LeaveTo.Value && z.IsApproved == true && z.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                    var r2 = context.tbl_Emp_Leave.FirstOrDefault(z => z.LeaveFrom <= model.LeaveFrom.Value && z.LeaveTo >= model.LeaveTo.Value && z.IsApproved == true && z.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);

                    if (r1 != null || r2 != null)
                    {
                        ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                        ViewData["List_LeaveCat"] = AllLeaveCat();
                        ViewData["List_LeaveType"] = AllLeaveType();
                        ViewBag.ErrorMsg = "Leave already applied for selected combination !";
                        return View(model);
                    } 
                    
                    if (context.tbl_Emp_Leave.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.LeaveFrom == model.LeaveFrom && x.LeaveTo == model.LeaveTo && x.LeaveDays == model.LeaveDays && x.IsApproved == true))
                    {
                        ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                        ViewData["List_LeaveCat"] = AllLeaveCat();
                        ViewData["List_LeaveType"] = AllLeaveType();
                        ViewBag.ErrorMsg = "Leave already applied for selected combination !";
                    }
                    else
                    {
                        tbl_Emp_Leave objlv = new tbl_Emp_Leave()
                        {
                            LeaveFrom = model.LeaveFrom,
                            LeaveTo = model.LeaveTo,
                            LeaveDays = model.LeaveDays,
                            AppliedFor = model.AppliedFor,
                            Reason = model.Reason,
                            LeaveType = model.LeaveType,
                            Employee_Code = model.Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsApproved = true,
                            ApprovedBy = objSubs.Username,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Leave.Add(objlv);
                        context.SaveChanges();
                        // insert into Attendance table
                        if (model.LeaveFrom == model.LeaveTo)
                        {
                            tbl_Emp_Attendance objloc = new tbl_Emp_Attendance()
                            {
                                Date = model.LeaveTo,
                                WorkingHours = "Leave approved for " + model.AppliedFor,
                                FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                Employee_Code = model.Employee_Code,
                                CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                CreatedBy = objSubs.Username
                            };
                            context.tbl_Emp_Attendance.Add(objloc);
                            context.SaveChanges();
                        }
                        else
                        {
                            int dates = Convert.ToInt32(model.LeaveDays);

                            for (int i = 0; i < dates; i++)
                            {
                                //var v = i.ToString();
                                var LASTDATE = model.LeaveFrom.Value.AddDays(i); ;
                                //var LASTDATE = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objDetails.Employee_Code && x.Date==objDetails.LeaveFrom).Select(s => s.Date).FirstOrDefault();
                                //var nxtdate = LASTDATE.Value.AddDays(1);
                                tbl_Emp_Attendance RQM = new tbl_Emp_Attendance()
                                {
                                    Date = LASTDATE,
                                    WorkingHours = "Leave approved for " + model.AppliedFor,
                                    FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                    Employee_Code = model.Employee_Code,
                                    CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                    CreatedBy = objSubs.Username
                                };
                                context.tbl_Emp_Attendance.Add(RQM);
                                context.SaveChanges();
                            }
                        }

                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Leave assigned successfully !";
                    }
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    ViewData["List_LeaveType"] = AllLeaveType();
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    ViewData["List_LeaveType"] = AllLeaveType();
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                ViewData["List_LeaveCat"] = AllLeaveCat();
                ViewData["List_LeaveType"] = AllLeaveType();
                return View(model);
            }
        }
    }
}