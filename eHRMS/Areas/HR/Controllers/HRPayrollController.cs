﻿using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Models;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRPayrollController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Payroll_Dashboard()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Earning_Types(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetDetails(id));
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Earning_Types(SalaryComponents model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int TypeID = Convert.ToInt32(id);
                        tbl_SlaryComponents objDetails = context.tbl_SlaryComponents.First(d => d.TypeID == TypeID);
                        // Update Table
                        objDetails.TypeName = model.TypeName;
                        objDetails.Percentage = model.Percentage;
                        objDetails.IsTaxable = model.IsTaxable;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Component updated successfully !";
                    }
                    else
                    {
                        tbl_SlaryComponents objQul = new tbl_SlaryComponents()
                        {
                            TypeName = model.TypeName,
                            ComponentType = "Earning",
                            Percentage = model.Percentage,
                            IsTaxable = model.IsTaxable,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_SlaryComponents.Add(objQul);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Component added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        public PartialViewResult Earning_Types_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SalaryComponents> Salary_Cmp_List = new List<SalaryComponents>();
            try
            {
                Salary_Cmp_List = context.tbl_SlaryComponents.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ComponentType == "Earning").Select(s => new SalaryComponents
                {
                    TypeID = s.TypeID,
                    TypeName = s.TypeName,
                    ComponentType = s.ComponentType,
                    Percentage = s.Percentage,
                    IsTaxable = s.IsTaxable,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Salary_Cmp_List);
        }


        #region Get Component Details
        private SalaryComponents GetDetails(string id)
        {
            int TypeIDID = Convert.ToInt32(id);
            tbl_SlaryComponents objdetails = context.tbl_SlaryComponents.Where(x => x.IsActive == true && x.TypeID == TypeIDID).FirstOrDefault();

            SalaryComponents objlist = new SalaryComponents();
            if (objdetails != null)
            {
                objlist = new SalaryComponents()
                {
                    TypeName = objdetails.TypeName,
                    Percentage = objdetails.Percentage,
                    IsTaxable = objdetails.IsTaxable,
                    DeductFrom = objdetails.DeductFrom,
                    DeductType = objdetails.DeductType
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        #region Delete Component
        public JsonResult DeleteComponent(string TypeID)
        {
            TypeID = HttpUtility.UrlDecode(TypeID);
            if (TypeID != null)
            {
                int id = Convert.ToInt32(TypeID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_SlaryComponents objDetails = context.tbl_SlaryComponents.First(d => d.TypeID == id);
                try
                {
                    // Update Table

                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Component deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Deduction_Types(string id)
        {
            ViewBag.DeductFromData = getDeductionFrom();
            ViewBag.DeductTypeData = getDeductionType();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetDetails(id));
            }
            else
            {
                return View();
            }
        }
        public List<SelectListItem> getDeductionFrom()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                objSelLst.Add(new SelectListItem { Text = "Basic", Value = "Basic" });
                objSelLst.Add(new SelectListItem { Text = "Gross", Value = "Gross" });
                objSelLst.Add(new SelectListItem { Text = "Value", Value = "Value" });
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        public List<SelectListItem> getDeductionType()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                objSelLst.Add(new SelectListItem { Text = "Amount", Value = "Amount" });
                objSelLst.Add(new SelectListItem { Text = "Percentage", Value = "Percentage" });
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
        [HttpPost]
        public ActionResult Deduction_Types(SalaryComponents model, string id)
        {
            ViewBag.DeductFromData = getDeductionFrom();
            ViewBag.DeductTypeData = getDeductionType();
            try
            {
                if (ModelState.IsValid && model.DeductFrom != null && model.DeductType != null)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int TypeID = Convert.ToInt32(id);
                        tbl_SlaryComponents objDetails = context.tbl_SlaryComponents.First(d => d.TypeID == TypeID);
                        // Update Table
                        objDetails.TypeName = model.TypeName;
                        objDetails.Percentage = model.Percentage;
                        objDetails.IsTaxable = model.IsTaxable;
                        objDetails.DeductFrom = model.DeductFrom;
                        objDetails.DeductType = model.DeductType;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;

                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Component updated successfully !";
                    }
                    else
                    {
                        tbl_SlaryComponents objQul = new tbl_SlaryComponents()
                        {
                            TypeName = model.TypeName,
                            ComponentType = "Deduction",
                            Percentage = model.Percentage,
                            IsTaxable = model.IsTaxable,
                            DeductFrom = model.DeductFrom,
                            DeductType = model.DeductType,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_SlaryComponents.Add(objQul);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Component added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        public PartialViewResult Deduction_Types_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SalaryComponents> Salary_Cmp_List = new List<SalaryComponents>();
            try
            {
                Salary_Cmp_List = context.tbl_SlaryComponents.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ComponentType == "Deduction").Select(s => new SalaryComponents
                {
                    TypeID = s.TypeID,
                    TypeName = s.TypeName,
                    ComponentType = s.ComponentType,
                    DeductFrom = s.DeductFrom,
                    DeductType = s.DeductType,
                    Percentage = s.Percentage,
                    IsTaxable = s.IsTaxable,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Salary_Cmp_List);
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Template_View()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<PaySlipTemplate> TemplateList = new List<PaySlipTemplate>();
            try
            {
                TemplateList = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).GroupBy(g => new { g.PackageID, g.PackageName, g.CTCFrom, g.CTCTo, g.MonthlyFrom, g.MonthlyTo }).Select(s => new PaySlipTemplate
                {
                    PackageID = s.Key.PackageID,
                    PackageName = s.Key.PackageName,
                    CTCFrom = s.Key.CTCFrom,
                    CTCTo = s.Key.CTCTo,
                    MonthlyFrom = s.Key.MonthlyFrom,
                    MonthlyTo = s.Key.MonthlyTo,
                }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(TemplateList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Create_Template()
        {
            PaySlipTemplate Comp_Payslip_Template = new PaySlipTemplate();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
            string PackageId = Request.QueryString["PackageId"];
            if (string.IsNullOrEmpty(PackageId))
            {


                var id = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).OrderByDescending(o => o.CreatedOn).Select(s => s.PackageID).FirstOrDefault();
                if (id != null)
                {
                    Comp_Payslip_Template.PackageID = (Convert.ToInt32(id) + 1).ToString();
                }
                else
                {
                    Comp_Payslip_Template.PackageID = "1";

                }
            }
            else
            {
                PackageId = objglobal.Decrypt(PackageId);
                var PayslipTemplate = context.tbl_PayslipTemplate.FirstOrDefault(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.PackageID == PackageId);
                if (PayslipTemplate != null)
                {

                    Comp_Payslip_Template.PackageID = PayslipTemplate.PackageID;
                    Comp_Payslip_Template.PackageName = PayslipTemplate.PackageName;
                    Comp_Payslip_Template.CTCFrom = PayslipTemplate.CTCFrom;
                    Comp_Payslip_Template.CTCTo = PayslipTemplate.CTCTo;
                    Comp_Payslip_Template.MonthlyFrom = PayslipTemplate.MonthlyFrom;
                    Comp_Payslip_Template.MonthlyTo = PayslipTemplate.MonthlyTo;
                    Comp_Payslip_Template.FK_TypeID = PayslipTemplate.FK_TypeID;
                    //tbl_SlaryComponents objdetails = context.tbl_SlaryComponents.Where(x => x.TypeID == EarnCatId && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                    //Comp_Payslip_Template.TypeName = objdetails.TypeName;
                    //Comp_Payslip_Template.ComponentType = objdetails.ComponentType;
                }
            }
            return View(Comp_Payslip_Template);
        }

        [HttpPost]
        public ActionResult Create_Template(PaySlipTemplate model, FormCollection fc)
        {
            PaySlipTemplate Comp_Payslip_Template = new PaySlipTemplate();
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            try
            {
                if (ModelState.IsValid)
                {
                    using (var dbTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var chkPayslipTemplate = context.tbl_PayslipTemplate.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.PackageID == model.PackageID && m.IsActive == true).ToList();
                            if (chkPayslipTemplate != null)
                            {
                                context.tbl_PayslipTemplate.RemoveRange(chkPayslipTemplate);
                                context.SaveChanges();
                            }
                            int Earncount = default(int);
                            int Deduccount = default(int);
                            int.TryParse(fc["hidEarnCompCount"], out Earncount);
                            for (int i = 1; i <= Earncount; i++)
                            {
                                int EarnCatId = default(int);
                                int.TryParse(fc["chkEarnCat" + i], out EarnCatId);
                                tbl_SlaryComponents objdetails = context.tbl_SlaryComponents.Where(x => x.TypeID == EarnCatId && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                                if (EarnCatId > 0)
                                {
                                    tbl_PayslipTemplate objSalaryTemp = new tbl_PayslipTemplate()
                                        {
                                            PackageID = model.PackageID,
                                            PackageName = model.PackageName,
                                            CTCFrom = model.CTCFrom,
                                            CTCTo = model.CTCTo,
                                            MonthlyFrom = model.MonthlyFrom,
                                            MonthlyTo = model.MonthlyTo,
                                            FK_TypeID = EarnCatId,
                                            TypeName = objdetails.TypeName,
                                            ComponentType = objdetails.ComponentType,
                                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                            IsActive = true,
                                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                            CreatedBy = objSubs.Username
                                        };
                                    context.tbl_PayslipTemplate.Add(objSalaryTemp);
                                    context.SaveChanges();
                                }
                            }
                            int.TryParse(fc["hidEarnCompCount"], out Deduccount);
                            for (int j = 1; j <= Deduccount; j++)
                            {
                                int DeduCatId = default(int);
                                int.TryParse(fc["chkDedCat" + j], out DeduCatId);
                                tbl_SlaryComponents objdetails = context.tbl_SlaryComponents.Where(x => x.TypeID == DeduCatId && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                                if (DeduCatId > 0)
                                {
                                    tbl_PayslipTemplate objSalaryTemp = new tbl_PayslipTemplate()
                                    {
                                        PackageID = model.PackageID,
                                        PackageName = model.PackageName,
                                        CTCFrom = model.CTCFrom,
                                        CTCTo = model.CTCTo,
                                        MonthlyFrom = model.MonthlyFrom,
                                        MonthlyTo = model.MonthlyTo,
                                        FK_TypeID = DeduCatId,
                                        TypeName = objdetails.TypeName,
                                        ComponentType = objdetails.ComponentType,
                                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                        IsActive = true,
                                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                        CreatedBy = objSubs.Username
                                    };
                                    context.tbl_PayslipTemplate.Add(objSalaryTemp);
                                    context.SaveChanges();

                                }
                            }
                            dbTransaction.Commit();
                            ModelState.Clear();
                            ViewBag.SuccessMsg = "Payslip Template added successfully !";
                            ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
                            var id = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).OrderByDescending(o => o.CreatedOn).Select(s => s.PackageID).FirstOrDefault();
                            if (id != null)
                            {
                                Comp_Payslip_Template.PackageID = (Convert.ToInt32(id) + 1).ToString();
                            }
                            else
                            {
                                Comp_Payslip_Template.PackageID = "1";
                            }
                            return View(Comp_Payslip_Template);
                        }
                        catch (Exception ex)
                        {
                            dbTransaction.Rollback();
                        }
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";

                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            ViewData["CompanyRegistrationID"] = objSubs.FK_CompanyRegistrationID;
            var uid = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).OrderByDescending(o => o.CreatedOn).Select(s => s.PackageID).FirstOrDefault();
            if (uid != null)
            {
                Comp_Payslip_Template.PackageID = (Convert.ToInt32(uid) + 1).ToString();
            }
            else
            {
                Comp_Payslip_Template.PackageID = "1";
            }
            return View(Comp_Payslip_Template);
        }


        #region Delete Template
        public JsonResult DeleteTemplate(string PackageID)
        {
            PackageID = HttpUtility.UrlDecode(PackageID);
            if (PackageID != null)
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                try
                {
                    List<PaySlipTemplate> TemplateList = new List<PaySlipTemplate>();
                    TemplateList = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.PackageID == PackageID).Select(s => new PaySlipTemplate
                    {
                        PayslipID = s.PayslipID,
                        IsActive = s.IsActive,
                    }).ToList();
                    foreach (var item in TemplateList)
                    {
                        tbl_PayslipTemplate objDetails = context.tbl_PayslipTemplate.First(d => d.PayslipID == item.PayslipID);
                        // Update Table
                        objDetails.IsActive = false;
                        objDetails.UpdatedBy = objSubs.Username;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    ViewBag.SuccessMsg = "Payslip template deleted successfully !";
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult IncomeTax()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Rent()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<HouseRent> Emp_Rent_List = new List<HouseRent>();
            try
            {
                Emp_Rent_List = context.tbl_Emp_HouseRent.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new HouseRent
                {
                    RentID = s.RentID,
                    EmployeeCode = s.EmployeeCode,
                    StayInRentHouse = s.StayInRentHouse,
                    January = s.January,
                    February = s.February,
                    March = s.March,
                    April = s.April,
                    May = s.May,
                    June = s.June,
                    July = s.July,
                    August = s.August,
                    September = s.September,
                    October = s.October,
                    November = s.November,
                    December = s.December,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(Emp_Rent_List);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Rent_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View(GetRentDetails(id));
            }
            else
            {
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View();
            }
        }

        #region Get GetRentDetails
        private HouseRent GetRentDetails(string id)
        {
            int RentID = Convert.ToInt32(id);
            tbl_Emp_HouseRent objdetails = context.tbl_Emp_HouseRent.Where(x => x.RentID == RentID).FirstOrDefault();

            HouseRent objlist = new HouseRent();
            if (objdetails != null)
            {
                objlist = new HouseRent()
                {
                    RentID = objdetails.RentID,
                    EmployeeCode = objdetails.EmployeeCode,
                    StayInRentHouse = objdetails.StayInRentHouse,
                    January = objdetails.January,
                    February = objdetails.February,
                    March = objdetails.March,
                    April = objdetails.April,
                    May = objdetails.May,
                    June = objdetails.June,
                    July = objdetails.July,
                    August = objdetails.August,
                    September = objdetails.September,
                    October = objdetails.October,
                    November = objdetails.November,
                    December = objdetails.December
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Employee_Rent_Add(HouseRent model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int RentID = Convert.ToInt32(id);
                        tbl_Emp_HouseRent objDetails = context.tbl_Emp_HouseRent.First(d => d.RentID == RentID);
                        // Update Table
                        objDetails.EmployeeCode = model.EmployeeCode;
                        objDetails.StayInRentHouse = model.StayInRentHouse;
                        objDetails.January = model.January;
                        objDetails.February = model.February;
                        objDetails.March = model.March;
                        objDetails.April = model.April;
                        objDetails.May = model.May;
                        objDetails.June = model.June;
                        objDetails.July = model.July;
                        objDetails.August = model.August;
                        objDetails.September = model.September;
                        objDetails.October = model.October;
                        objDetails.November = model.November;
                        objDetails.December = model.December;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Rent Details updated successfully !";
                    }
                    else if (context.tbl_Emp_HouseRent.Any(x => x.IsActive == true && x.EmployeeCode == model.EmployeeCode))
                    {
                        ViewBag.ErrorMsg = "House Rentwith same Employee Code already added !";
                    }
                    else
                    {
                        tbl_Emp_HouseRent objRent = new tbl_Emp_HouseRent()
                        {
                            EmployeeCode = model.EmployeeCode,
                            StayInRentHouse = model.StayInRentHouse,
                            January = model.January,
                            February = model.February,
                            March = model.March,
                            April = model.April,
                            May = model.May,
                            June = model.June,
                            July = model.July,
                            August = model.August,
                            September = model.September,
                            October = model.October,
                            November = model.November,
                            December = model.December,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_HouseRent.Add(objRent);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Rent Details added successfully !";
                        ModelState.Clear();
                    }
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View();
            }
        }

        #region Delete HouseRent
        public JsonResult DeleteHouseRent(string RentID)
        {
            RentID = HttpUtility.UrlDecode(RentID);
            if (RentID != null)
            {
                int id = Convert.ToInt32(RentID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_HouseRent objDetails = context.tbl_Emp_HouseRent.First(d => d.RentID == id);
                try
                {
                    // Update Table

                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "House Rent deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Investment()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<EmployeeInvestment> Emp_Invest_List = new List<EmployeeInvestment>();
            try
            {
                Emp_Invest_List = context.tbl_Employee_Investment.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new EmployeeInvestment
                {
                    InvestID = s.InvestID,
                    Employee_Code = s.Employee_Code,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(Emp_Invest_List);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employee_Investment_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View(GetInvestmentDetails(id));
            }
            else
            {
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View();
            }
        }

        #region Get GetInvestmentDetails
        private EmployeeInvestment GetInvestmentDetails(string id)
        {
            int InvestID = Convert.ToInt32(id);
            tbl_Employee_Investment objdetails = context.tbl_Employee_Investment.Where(x => x.InvestID == InvestID).FirstOrDefault();

            EmployeeInvestment objlist = new EmployeeInvestment();
            if (objdetails != null)
            {
                objlist = new EmployeeInvestment()
                {
                    Employee_Code = objdetails.Employee_Code,
                    Public_Provident_Fund_PPF_80C = objdetails.Public_Provident_Fund_PPF_80C,
                    Life_Insurance_LIC_80C = objdetails.Life_Insurance_LIC_80C,
                    National_Savings_Certificate_Purchased_NSC_80C = objdetails.National_Savings_Certificate_Purchased_NSC_80C,
                    Unit_Linked_Insurance_Plan_ULIP_80C = objdetails.Unit_Linked_Insurance_Plan_ULIP_80C,
                    Mutual_Funds_Notified_US_10_23D = objdetails.Mutual_Funds_Notified_US_10_23D,
                    Contribution_To_Pension_Fund_80C = objdetails.Contribution_To_Pension_Fund_80C,
                    Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C = objdetails.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C,
                    Senior_Citizens_Savings_Scheme_80C = objdetails.Senior_Citizens_Savings_Scheme_80C,
                    Equity_Linked_Savings_Scheme_ELSS_80C = objdetails.Equity_Linked_Savings_Scheme_ELSS_80C,
                    Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C = objdetails.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C,
                    Five_Years_Of_Post_Office_Deposits_80C = objdetails.Five_Years_Of_Post_Office_Deposits_80C,
                    Education_Tution_Fee_Upto_Two_Children_80C = objdetails.Education_Tution_Fee_Upto_Two_Children_80C,
                    Rajiv_Gandhi_Equity_Saving_Schemes = objdetails.Rajiv_Gandhi_Equity_Saving_Schemes,
                    Medical_Insurance_Premium_Individual_Spouse_And_Children = objdetails.Medical_Insurance_Premium_Individual_Spouse_And_Children,
                    Medical_Insurance_Premium_Parents = objdetails.Medical_Insurance_Premium_Parents,
                    Preventive_Health_Check_Up = objdetails.Preventive_Health_Check_Up,
                    Medical_Treatment_For_Dependent_Handicap = objdetails.Medical_Treatment_For_Dependent_Handicap,
                    Medical_Treatment_For_Specified_Disease = objdetails.Medical_Treatment_For_Specified_Disease,
                    Medical_Treatment_For_Specified_Disease_For_Senior_Citizen = objdetails.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen,
                    Permanent_Physical_Disability = objdetails.Permanent_Physical_Disability,
                    Interest_On_Education_Loan = objdetails.Interest_On_Education_Loan,
                    Interest_On_Housing_Loan = objdetails.Interest_On_Housing_Loan,
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Employee_Investment_Add(EmployeeInvestment model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int InvestID = Convert.ToInt32(id);
                        tbl_Employee_Investment objDetails = context.tbl_Employee_Investment.First(d => d.InvestID == InvestID);
                        // Update Table
                        objDetails.Public_Provident_Fund_PPF_80C = model.Public_Provident_Fund_PPF_80C;
                        objDetails.Life_Insurance_LIC_80C = model.Life_Insurance_LIC_80C;
                        objDetails.National_Savings_Certificate_Purchased_NSC_80C = model.National_Savings_Certificate_Purchased_NSC_80C;
                        objDetails.Unit_Linked_Insurance_Plan_ULIP_80C = model.Unit_Linked_Insurance_Plan_ULIP_80C;

                        objDetails.Mutual_Funds_Notified_US_10_23D = model.Mutual_Funds_Notified_US_10_23D;

                        objDetails.Contribution_To_Pension_Fund_80C = model.Contribution_To_Pension_Fund_80C;

                        objDetails.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C = model.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C;

                        objDetails.Senior_Citizens_Savings_Scheme_80C = model.Senior_Citizens_Savings_Scheme_80C;

                        objDetails.Equity_Linked_Savings_Scheme_ELSS_80C = model.Equity_Linked_Savings_Scheme_ELSS_80C;

                        objDetails.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C = model.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C;
                        objDetails.Five_Years_Of_Post_Office_Deposits_80C = model.Five_Years_Of_Post_Office_Deposits_80C;
                        objDetails.Education_Tution_Fee_Upto_Two_Children_80C = model.Education_Tution_Fee_Upto_Two_Children_80C;
                        objDetails.Rajiv_Gandhi_Equity_Saving_Schemes = model.Rajiv_Gandhi_Equity_Saving_Schemes;
                        objDetails.Medical_Insurance_Premium_Individual_Spouse_And_Children = model.Medical_Insurance_Premium_Individual_Spouse_And_Children;
                        objDetails.Medical_Insurance_Premium_Parents = model.Medical_Insurance_Premium_Parents;
                        objDetails.Preventive_Health_Check_Up = model.Preventive_Health_Check_Up;
                        objDetails.Medical_Treatment_For_Dependent_Handicap = model.Medical_Treatment_For_Dependent_Handicap;
                        objDetails.Medical_Treatment_For_Specified_Disease = model.Medical_Treatment_For_Specified_Disease;
                        objDetails.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen = model.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen;
                        objDetails.Permanent_Physical_Disability = model.Permanent_Physical_Disability;
                        objDetails.Interest_On_Education_Loan = model.Interest_On_Education_Loan;
                        objDetails.Interest_On_Housing_Loan = model.Interest_On_Housing_Loan;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        // Total of All Investment
                        int Total = Convert.ToInt32(model.Public_Provident_Fund_PPF_80C) + Convert.ToInt32(model.Life_Insurance_LIC_80C) + Convert.ToInt32(model.National_Savings_Certificate_Purchased_NSC_80C);
                        Total += Convert.ToInt32(model.Unit_Linked_Insurance_Plan_ULIP_80C) + Convert.ToInt32(model.Mutual_Funds_Notified_US_10_23D) + Convert.ToInt32(model.Contribution_To_Pension_Fund_80C);
                        Total += Convert.ToInt32(model.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C) + Convert.ToInt32(model.Senior_Citizens_Savings_Scheme_80C) + Convert.ToInt32(model.Equity_Linked_Savings_Scheme_ELSS_80C);
                        Total += Convert.ToInt32(model.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C) + Convert.ToInt32(model.Five_Years_Of_Post_Office_Deposits_80C) + Convert.ToInt32(model.Education_Tution_Fee_Upto_Two_Children_80C);
                        Total += Convert.ToInt32(model.Rajiv_Gandhi_Equity_Saving_Schemes) + Convert.ToInt32(model.Medical_Insurance_Premium_Individual_Spouse_And_Children) + Convert.ToInt32(model.Medical_Insurance_Premium_Parents);
                        Total += Convert.ToInt32(model.Preventive_Health_Check_Up) + Convert.ToInt32(model.Medical_Treatment_For_Dependent_Handicap) + Convert.ToInt32(model.Medical_Treatment_For_Specified_Disease);
                        Total += Convert.ToInt32(model.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen) + Convert.ToInt32(model.Permanent_Physical_Disability) + Convert.ToInt32(model.Interest_On_Education_Loan);
                        Total += Convert.ToInt32(model.Interest_On_Housing_Loan);
                        //
                        if (Total > 150000)
                        {
                            Total = 150000;
                        }
                        // Current TAX value of the Employee
                        tbl_TAXComputation objTAX = context.tbl_TAXComputation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.ForYear == System.DateTime.Now.Year).FirstOrDefault();
                        int prct = Convert.ToInt32(context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.SlabMasterID == objTAX.FK_SlabMasterID).Select(s => s.TAX).FirstOrDefault());
                        //update TaxComputation table
                        objTAX.Investment = Total;
                        objTAX.YearlyTAXAmount = ((objTAX.TaxableAmount - Total) * prct) / 100;
                        objTAX.MonthlyTAXAmount = (((objTAX.TaxableAmount - Total) * prct) / 100) / 12;
                        objTAX.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objTAX.UpdatedBy = objSubs.Username;
                        context.Entry(objTAX).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Investment Details updated successfully !";
                    }
                    else if (context.tbl_Employee_Investment.Any(x => x.IsActive == true && x.Employee_Code == model.Employee_Code))
                    {
                        ViewBag.ErrorMsg = "Investment with same Employee Code already added !";
                    }
                    else
                    {
                        tbl_Employee_Investment objInvest = new tbl_Employee_Investment()
                        {
                            Employee_Code = model.Employee_Code,
                            Public_Provident_Fund_PPF_80C = model.Public_Provident_Fund_PPF_80C,
                            Life_Insurance_LIC_80C = model.Life_Insurance_LIC_80C,
                            National_Savings_Certificate_Purchased_NSC_80C = model.National_Savings_Certificate_Purchased_NSC_80C,
                            Unit_Linked_Insurance_Plan_ULIP_80C = model.Unit_Linked_Insurance_Plan_ULIP_80C,
                            Mutual_Funds_Notified_US_10_23D = model.Mutual_Funds_Notified_US_10_23D,
                            Contribution_To_Pension_Fund_80C = model.Contribution_To_Pension_Fund_80C,
                            Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C = model.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C,
                            Senior_Citizens_Savings_Scheme_80C = model.Senior_Citizens_Savings_Scheme_80C,
                            Equity_Linked_Savings_Scheme_ELSS_80C = model.Equity_Linked_Savings_Scheme_ELSS_80C,
                            Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C = model.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C,
                            Five_Years_Of_Post_Office_Deposits_80C = model.Five_Years_Of_Post_Office_Deposits_80C,
                            Education_Tution_Fee_Upto_Two_Children_80C = model.Education_Tution_Fee_Upto_Two_Children_80C,
                            Rajiv_Gandhi_Equity_Saving_Schemes = model.Rajiv_Gandhi_Equity_Saving_Schemes,
                            Medical_Insurance_Premium_Individual_Spouse_And_Children = model.Medical_Insurance_Premium_Individual_Spouse_And_Children,
                            Medical_Insurance_Premium_Parents = model.Medical_Insurance_Premium_Parents,
                            Preventive_Health_Check_Up = model.Preventive_Health_Check_Up,
                            Medical_Treatment_For_Dependent_Handicap = model.Medical_Treatment_For_Dependent_Handicap,
                            Medical_Treatment_For_Specified_Disease = model.Medical_Treatment_For_Specified_Disease,
                            Medical_Treatment_For_Specified_Disease_For_Senior_Citizen = model.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen,
                            Permanent_Physical_Disability = model.Permanent_Physical_Disability,
                            Interest_On_Education_Loan = model.Interest_On_Education_Loan,
                            Interest_On_Housing_Loan = model.Interest_On_Housing_Loan,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Employee_Investment.Add(objInvest);
                        context.SaveChanges();
                        // Total of All Investment
                        int Total = Convert.ToInt32(model.Public_Provident_Fund_PPF_80C) + Convert.ToInt32(model.Life_Insurance_LIC_80C) + Convert.ToInt32(model.National_Savings_Certificate_Purchased_NSC_80C);
                        Total += Convert.ToInt32(model.Unit_Linked_Insurance_Plan_ULIP_80C) + Convert.ToInt32(model.Mutual_Funds_Notified_US_10_23D) + Convert.ToInt32(model.Contribution_To_Pension_Fund_80C);
                        Total += Convert.ToInt32(model.Five_Years_Fixed_Deposits_In_Scheduled_Bank_80C) + Convert.ToInt32(model.Senior_Citizens_Savings_Scheme_80C) + Convert.ToInt32(model.Equity_Linked_Savings_Scheme_ELSS_80C);
                        Total += Convert.ToInt32(model.Bonds_Issued_By_National_Bank_Of_Agriculture_And_Rural_Development_80C) + Convert.ToInt32(model.Five_Years_Of_Post_Office_Deposits_80C) + Convert.ToInt32(model.Education_Tution_Fee_Upto_Two_Children_80C);
                        Total += Convert.ToInt32(model.Rajiv_Gandhi_Equity_Saving_Schemes) + Convert.ToInt32(model.Medical_Insurance_Premium_Individual_Spouse_And_Children) + Convert.ToInt32(model.Medical_Insurance_Premium_Parents);
                        Total += Convert.ToInt32(model.Preventive_Health_Check_Up) + Convert.ToInt32(model.Medical_Treatment_For_Dependent_Handicap) + Convert.ToInt32(model.Medical_Treatment_For_Specified_Disease);
                        Total += Convert.ToInt32(model.Medical_Treatment_For_Specified_Disease_For_Senior_Citizen) + Convert.ToInt32(model.Permanent_Physical_Disability) + Convert.ToInt32(model.Interest_On_Education_Loan);
                        Total += Convert.ToInt32(model.Interest_On_Housing_Loan);
                        //
                        if (Total > 150000)
                        {
                            Total = 150000;
                        }
                        // Current TAX value of the Employee
                        tbl_TAXComputation objTAX = context.tbl_TAXComputation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.ForYear == System.DateTime.Now.Year).FirstOrDefault();
                        int prct = Convert.ToInt32(context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.SlabMasterID == objTAX.FK_SlabMasterID).Select(s => s.TAX).FirstOrDefault());
                        //update TaxComputation table
                        objTAX.Investment = Total;
                        objTAX.YearlyTAXAmount = ((objTAX.TaxableAmount - Total) * prct) / 100;
                        objTAX.MonthlyTAXAmount = (((objTAX.TaxableAmount - Total) * prct) / 100) / 12;
                        objTAX.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objTAX.UpdatedBy = objSubs.Username;
                        context.Entry(objTAX).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.SuccessMsg = "Employee Investment Details added successfully !";
                        ModelState.Clear();
                    }
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return View();
            }
        }


        #region Delete Investment
        public JsonResult DeleteInvestment(string InvestID)
        {
            InvestID = HttpUtility.UrlDecode(InvestID);
            if (InvestID != null)
            {
                int id = Convert.ToInt32(InvestID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Employee_Investment objDetails = context.tbl_Employee_Investment.First(d => d.InvestID == id);
                try
                {
                    // Update Table

                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Employee Investment deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Previous_Employee_Tax()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Previous_Employee_TAX> PE_TAX = new List<Previous_Employee_TAX>();
            try
            {
                PE_TAX = context.tbl_PreviousEmployee_TAX.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Previous_Employee_TAX
                {
                    PEInvestID = s.PEInvestID,
                    Employee_Code = s.Employee_Code,
                    GrossSalary = s.GrossSalary,
                    Exemptions_Under_Section_Ten = s.Exemptions_Under_Section_Ten,
                    PF = s.PF,
                    ProfessionalTax = s.ProfessionalTax,
                    TotalTaxPaid = s.TotalTaxPaid,
                    TotalIncome = s.TotalIncome,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(PE_TAX);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Previous_Employee_Tax_Add(string id)
        {
            Previous_Employee_TAX model = new Previous_Employee_TAX();
            if (id != null)
            {
                ViewData["Code_List_PEmployee"] = PreviousEmployeeCodeList();
                id = HttpUtility.UrlDecode(id);
                return View(PreviousEmployeesTAXDetails(id));
            }
            else
            {
                ViewData["Code_List_PEmployee"] = PreviousEmployeeCodeList();
                return View(model);
            }
        }

        #region Get Previous Employee TAX Details
        private Previous_Employee_TAX PreviousEmployeesTAXDetails(string id)
        {
            int PEInvestID = Convert.ToInt32(id);
            tbl_PreviousEmployee_TAX objdetails = context.tbl_PreviousEmployee_TAX.Where(x => x.PEInvestID == PEInvestID).FirstOrDefault();

            Previous_Employee_TAX objlist = new Previous_Employee_TAX();
            if (objdetails != null)
            {
                objlist = new Previous_Employee_TAX()
                {
                    PEInvestID = objdetails.PEInvestID,
                    Employee_Code = objdetails.Employee_Code,
                    GrossSalary = objdetails.GrossSalary,
                    Exemptions_Under_Section_Ten = objdetails.Exemptions_Under_Section_Ten,
                    PF = objdetails.PF,
                    ProfessionalTax = objdetails.ProfessionalTax,
                    TotalTaxPaid = objdetails.TotalTaxPaid,
                    TotalIncome = objdetails.TotalIncome,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        public List<SelectListItem> PreviousEmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Resign.Where(x => x.IsAccepted == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ResignDate < System.DateTime.Now).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        public ActionResult Previous_Employee_Tax_Add(Previous_Employee_TAX model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int PEInvestID = Convert.ToInt32(id);
                        tbl_PreviousEmployee_TAX objDetails = context.tbl_PreviousEmployee_TAX.First(d => d.PEInvestID == PEInvestID);
                        // Update Table
                        objDetails.GrossSalary = model.GrossSalary;
                        objDetails.Exemptions_Under_Section_Ten = model.Exemptions_Under_Section_Ten;
                        objDetails.PF = model.PF;
                        objDetails.ProfessionalTax = model.ProfessionalTax;
                        objDetails.TotalTaxPaid = model.TotalTaxPaid;
                        objDetails.TotalIncome = model.TotalIncome;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Previous Employee TAX Details updated successfully !";
                    }
                    else if (context.tbl_PreviousEmployee_TAX.Any(x => x.Employee_Code == model.Employee_Code))
                    {
                        ViewBag.ErrorMsg = "TAX with same Employee Code already added !";
                    }
                    else
                    {
                        tbl_PreviousEmployee_TAX objInvest = new tbl_PreviousEmployee_TAX()
                        {
                            Employee_Code = model.Employee_Code,
                            GrossSalary = model.GrossSalary,
                            Exemptions_Under_Section_Ten = model.Exemptions_Under_Section_Ten,
                            PF = model.PF,
                            ProfessionalTax = model.ProfessionalTax,
                            TotalTaxPaid = model.TotalTaxPaid,
                            TotalIncome = model.TotalIncome,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_PreviousEmployee_TAX.Add(objInvest);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Previous Employee TAX Details added successfully !";
                        ModelState.Clear();
                    }
                    ViewData["Code_List_PEmployee"] = PreviousEmployeeCodeList();
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["Code_List_PEmployee"] = PreviousEmployeeCodeList();
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_PEmployee"] = PreviousEmployeeCodeList();
                return View();
            }
        }


        public ActionResult IncomeTaxComputation()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<TaxComputation> TAXCompute = new List<TaxComputation>();
            try
            {
                TAXCompute = context.tbl_TAXComputation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.ForYear == System.DateTime.Now.Year).Select(s => new TaxComputation
                {
                    TAXID = s.TAXID,
                    Employee_Code = s.Employee_Code,
                    TaxableAmount = s.TaxableAmount,
                    Investment = s.Investment,
                    YearlyTAXAmount = s.YearlyTAXAmount,
                    MonthlyTAXAmount = s.MonthlyTAXAmount,
                    ForYear = s.ForYear,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(TAXCompute);
        }


        public ActionResult Settings()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            return View(GetSettingsDetails(objSubs.FK_CompanyRegistrationID));
        }

        #region Get SettingsDetails
        private PayrollSettings GetSettingsDetails(string id)
        {
            tbl_PayrollSettings objdetails = context.tbl_PayrollSettings.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == id).FirstOrDefault();

            PayrollSettings objlist = new PayrollSettings();
            if (objdetails != null)
            {
                objlist = new PayrollSettings()
                {
                    PaySettingsID = objdetails.PaySettingsID,
                    GratuityApplicable = objdetails.GratuityApplicable,
                    PercentageOfGratuity = objdetails.PercentageOfGratuity,
                    LWFApplicable = objdetails.LWFApplicable,
                    PercentageOfLWF = objdetails.PercentageOfLWF,
                    MonthlyWorkingDays = objdetails.MonthlyWorkingDays,
                };
                return objlist;
            }
            else
            {
                //ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Settings(PayrollSettings model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_PayrollSettings.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        tbl_PayrollSettings objstng = context.tbl_PayrollSettings.First(d => d.IsActive == true && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                        objstng.GratuityApplicable = model.GratuityApplicable;
                        objstng.PercentageOfGratuity = model.PercentageOfGratuity;
                        objstng.LWFApplicable = model.LWFApplicable;
                        objstng.PercentageOfLWF = model.PercentageOfLWF;
                        objstng.MonthlyWorkingDays = model.MonthlyWorkingDays;
                        objstng.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objstng.UpdatedBy = objSubs.Username;
                        context.Entry(objstng).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Settings updated successfully !";
                    }
                    else
                    {
                        tbl_PayrollSettings objstng = new tbl_PayrollSettings()
                        {
                            GratuityApplicable = model.GratuityApplicable,
                            PercentageOfGratuity = model.PercentageOfGratuity,
                            LWFApplicable = model.LWFApplicable,
                            PercentageOfLWF = model.PercentageOfLWF,
                            MonthlyWorkingDays = model.MonthlyWorkingDays,
                            IsActive = true,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_PayrollSettings.Add(objstng);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Settings saved successfully !";
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Employees_Salary()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_Salary> Salary_Emp_List = new List<Emp_Salary>();
            try
            {
                Salary_Emp_List = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Emp_Salary
                {
                    SalaryID = s.SalaryID,
                    Employee_Code = s.Employee_Code,
                    CTC = s.CTC,
                    PFNumber = s.PFNumber,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(Salary_Emp_List);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Edit_Employees_Salary(string id)
        {
            Emp_Salary model = new Emp_Salary();
            if (id != null)
            {
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                ViewData["Salary_List"] = AllSalaryList();
                id = HttpUtility.UrlDecode(id);
                return View(GetEmployeesSalaryDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        public List<SelectListItem> AllEmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Salary.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).OrderBy(s => (s.Employee_Code)).ToList();


                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public List<SelectListItem> AllSalaryList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_PayslipTemplate.Where(c => c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).GroupBy(g => new { g.PackageID, g.PackageName }).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Key.PackageName.ToString(), Value = item.Key.PackageID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        #region Get Employee Salary Details
        private Emp_Salary GetEmployeesSalaryDetails(string id)
        {
            int SalaryID = Convert.ToInt32(id);
            tbl_Emp_Salary objdetails = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.SalaryID == SalaryID).FirstOrDefault();

            Emp_Salary objlist = new Emp_Salary();
            if (objdetails != null)
            {
                objlist = new Emp_Salary()
                {
                    SalaryID = objdetails.SalaryID,
                    Employee_Code = objdetails.Employee_Code,
                    FK_PackageID = objdetails.FK_PackageID,
                    CTC = objdetails.CTC,
                    Monthly = objdetails.Monthly,
                    PFNumber = objdetails.PFNumber,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Edit_Employees_Salary(Emp_Salary model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int SalaryID = Convert.ToInt32(id);
                        tbl_Emp_Salary objSalaryDetails = context.tbl_Emp_Salary.First(d => d.SalaryID == SalaryID);
                        // Update Table
                        objSalaryDetails.Employee_Code = model.Employee_Code;
                        objSalaryDetails.FK_PackageID = model.FK_PackageID;
                        objSalaryDetails.CTC = model.CTC;
                        objSalaryDetails.Monthly = model.Monthly;
                        objSalaryDetails.PFNumber = model.PFNumber;
                        objSalaryDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objSalaryDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objSalaryDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        // Compute Income tax
                        ItemForTAXComputation objIT = objglobal.GetITDetails(model.Employee_Code, model.FK_PackageID, model.CTC);
                        // Update Income TAX
                        tbl_TAXComputation objTAXCom = context.tbl_TAXComputation.First(d => d.IsActive == true && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Employee_Code == model.Employee_Code);

                        objTAXCom.TaxableAmount = objIT.TaxableAmount;
                        objTAXCom.Investment = objIT.Investment;
                        objTAXCom.YearlyTAXAmount = objIT.YearlyTAX;
                        objTAXCom.MonthlyTAXAmount = objIT.MonthlyTAX;
                        objTAXCom.FK_SlabMasterID = objIT.FK_SlabMasterID;
                        objTAXCom.FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID;
                        objTAXCom.ForYear = objglobal.TimeZone(objglobal.getTimeZone()).Year;
                        objSalaryDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objSalaryDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objTAXCom).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.SuccessMsg = "Employee Salary Details updated successfully !";
                        ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                        ViewData["Salary_List"] = AllSalaryList();
                    }
                    return View();
                }
                else
                {
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    ViewData["Salary_List"] = AllSalaryList();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }

        }

        #region Delete Employee Salary
        public JsonResult DeleteEmpSalary(string SalaryID)
        {
            SalaryID = HttpUtility.UrlDecode(SalaryID);
            if (SalaryID != null)
            {
                int id = Convert.ToInt32(SalaryID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Salary objDetails = context.tbl_Emp_Salary.First(d => d.SalaryID == id);
                try
                {
                    // Update Table

                    objDetails.IsActive = false;
                    objDetails.UpdatedBy = objSubs.Username;
                    objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Employee Salary deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Process_Salary()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            return View(new Process_Salary());
        }
        //[HttpPost]
        //public ActionResult Process_Salary(Process_Salary model)
        //{
        //    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
        //            if (model.Processfor == "All")
        //            {
        //                //var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Qual_Employment.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
        //                var Result = context.tbl_Emp_Salary.Where(w =>
        //                    !context.tbl_ProcessSalary.Where(x =>
        //                        x.Month == model.Month && x.Year == model.Year)
        //                        .Select(b => b.FK_Employee_Code).Contains(w.Employee_Code) && w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
        //                foreach (var item in Result)
        //                {
        //                    tbl_ProcessSalary objprc = new tbl_ProcessSalary()
        //                    {
        //                        Month = model.Month,
        //                        Year = model.Year,
        //                        ProcessFor = model.Processfor,
        //                        FK_Employee_Code = item.Employee_Code,
        //                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
        //                        CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
        //                        CreatedBy = objSubs.Username
        //                    };
        //                    context.tbl_ProcessSalary.Add(objprc);
        //                    context.SaveChanges();
        //                }

        //                ModelState.Clear();
        //                model = new Process_Salary();
        //                ViewBag.SuccessMsg = "Salary Processed for All Employee !";

        //            }
        //            else if (context.tbl_ProcessSalary.Any(x => x.Month == model.Month && x.Year == model.Year && x.FK_Employee_Code == model.FK_Employee_Code && x.IsActive == true))
        //            {
        //                ViewBag.ErrorMsg = "Salary for this Employee already processed for those combination !";
        //            }
        //            else
        //            {
        //                tbl_ProcessSalary objprc = new tbl_ProcessSalary()
        //                {
        //                    Month = model.Month,
        //                    Year = model.Year,
        //                    ProcessFor = model.Processfor,
        //                    FK_Employee_Code = model.FK_Employee_Code,
        //                    FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
        //                    CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
        //                    IsActive = true,
        //                    CreatedBy = objSubs.Username
        //                };
        //                context.tbl_ProcessSalary.Add(objprc);
        //                context.SaveChanges();
        //                ModelState.Clear();
        //                ViewBag.SuccessMsg = "Salary Processed for this Employee !";
        //            }
        //            return View(model);
        //        }
        //        else
        //        {
        //            ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
        //            return View(model);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objError.LogError(ex);
        //        return View(model);
        //    }
        //}

        [HttpPost]
        public JsonResult Process_Salary(Process_Salary model)
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (model.Processfor == "All")
                    {
                        //var Result = context.tbl_Emp_Contact_Basic.Where(c => !context.tbl_Emp_Qual_Employment.Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                        var Result = context.tbl_Emp_Salary.Where(w =>
                            !context.tbl_ProcessSalary.Where(x =>
                                x.Month == model.Month && x.Year == model.Year)
                                .Select(b => b.FK_Employee_Code).Contains(w.Employee_Code) && w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                        foreach (var item in Result)
                        {
                            tbl_ProcessSalary objprc = new tbl_ProcessSalary()
                            {
                                Month = model.Month,
                                Year = model.Year,
                                ProcessFor = model.Processfor,
                                FK_Employee_Code = item.Employee_Code,
                                FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                                CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                                CreatedBy = objSubs.Username,
                                TotalWorkingDays = model.TotalWorkingDays
                            };
                            context.tbl_ProcessSalary.Add(objprc);
                            context.SaveChanges();
                        }

                        ModelState.Clear();
                        model = new Process_Salary();
                        ViewBag.SuccessMsg = "Salary Processed for All Employee !";
                        return Json(ViewBag.SuccessMsg);

                    }
                    else if (context.tbl_ProcessSalary.Any(x => x.Month == model.Month && x.Year == model.Year && x.FK_Employee_Code == model.FK_Employee_Code && x.IsActive == true))
                    {
                        ViewBag.ErrorMsg = "Salary for this Employee already processed for those combination !";
                        return Json(ViewBag.ErrorMsg);
                    }
                    else
                    {
                        tbl_ProcessSalary objprc = new tbl_ProcessSalary()
                        {
                            Month = model.Month,
                            Year = model.Year,
                            ProcessFor = model.Processfor,
                            FK_Employee_Code = model.FK_Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            IsActive = true,
                            CreatedBy = objSubs.Username,
                            TotalWorkingDays = model.TotalWorkingDays
                        };
                        context.tbl_ProcessSalary.Add(objprc);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Salary Processed for this Employee !";
                        return Json(new { result = ViewBag.SuccessMsg, salId = objprc.ProcessSalaryID });
                    }

                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return Json(ViewBag.ErrorMsg);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return Json(ex.Message);
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Payslip()
        {
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            return View();

        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Payslip_Print(Print_Payslip model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

                    if (context.tbl_ProcessSalary.Any(x => x.Month == model.Month && x.Year == model.Year && x.FK_Employee_Code == model.FK_Employee_Code))
                    {
                        var m = model.Month; var y = model.Year;
                        var EmpDetails = context.tbl_Emp_Contact_Basic.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault();
                        DateTime CreatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).Select(ls => ls.OfficeLocation).FirstOrDefault();
                        var CompanyAdress = (from ej in context.tbl_Emp_Joining_Probation
                                             join cl in context.tbl_CompanyLocation on ej.FK_OfficeLocationID equals cl.OfficeLocationID
                                             where ej.Employee_Code == model.FK_Employee_Code && cl.IsActive == true
                                             select new
                                             {
                                                 companyAddress = cl.OfficeAddress

                                             }
                                   ).Select(sl => sl.companyAddress).FirstOrDefault();
                        model = context.tbl_CompanyProfile.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Print_Payslip
                        {
                            Month = m,
                            Year = y,
                            TotalWorkingDay = model.TotalWorkingDay,//total working day 
                            FK_Employee_Code = EmpDetails.Employee_Code,
                            SalarySlipID = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper(),
                            BannerImagePath = s.BannerImagePath,
                            CompanyName = s.CompanyName,
                            //CompanyAddress = context.tbl_CompanyLocation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(ls => ls.OfficeAddress).FirstOrDefault(),
                            CompanyAddress = CompanyAdress,
                            CompanyWebsite = s.CompanyWebsite,
                            EmployeeName = EmpDetails.Name,
                            EmployeeEmail = EmpDetails.Email,
                            EmployeeNumber = EmpDetails.Number,
                            CreatedOn = CreatedOn,
                        }).FirstOrDefault();
                        ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                        Session["ErrorMsg"] = null;
                        if (model == null)
                        {
                            Session["ErrorMsg"] = "Company profile details not added!";
                            return RedirectToAction("Payslip");
                        }
                        return View(model);
                        //return RedirectToAction("Salary_Slip_Print", "HRPayroll", new { area = "HR" });
                    }
                    else
                    {
                        ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                        Session["ErrorMsg"] = "Salary not processed for those combination !";
                        return RedirectToAction("Payslip");
                    }
                }
                else
                {
                    ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                    Session["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Payslip");

                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = AllEmployeeCodeList();
                return RedirectToAction("Payslip");
            }

        }

        //public PartialViewResult Salary_Slip_Components()
        //{
        //    List<SalaryComponents_EarningAndDeduction> Salary_Component_List = new List<SalaryComponents_EarningAndDeduction>();
        //    return PartialView(Salary_Component_List);
        //}


        public PartialViewResult Salary_Slip_Components(Print_Payslip model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SalaryComponents_EarningAndDeduction> Deduction_List = new List<SalaryComponents_EarningAndDeduction>();
            List<SalaryComponents_EarningAndDeduction> Earning_List = new List<SalaryComponents_EarningAndDeduction>();
            List<SalaryComponents_EarningAndDeduction> Filnal_List = new List<SalaryComponents_EarningAndDeduction>();
            List<TrackAtt> track_List = new List<TrackAtt>();
            try
            {
                // EMPLOYEE SALRY DETAILS
                var EmpSallary = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault();
                // PAYROLL SETTING DETAILS
                var PayrollSettings = context.tbl_PayrollSettings.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                // GET EMPLOYEE ATTENDANCE DETAILS
                var month = Convert.ToInt32(model.Month);
                var year = Convert.ToInt32(model.Year);
                var totalWorkingDay = model.TotalWorkingDay;
                var sundayList = GlobalFunction.getSundays(year, month);

                //track_List = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code && x.Date.Value.Month == month && x.Date.Value.Year == year).Select(s => new TrackAtt
                //{
                //    Employee_Code = s.Employee_Code,
                //    Date = s.Date,
                //    InTime = s.InTime,
                //    OutTime = s.OutTime,
                //    WorkingHours = s.WorkingHours,
                //    WorkingHoursInMin = s.WorkingHoursInMin,

                //}).ToList();

                var totalWorkingHoursInMin = context.tbl_Emp_Attendance
                    .Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code && x.Date.Value.Month == month && x.Date.Value.Year == year)
                    .AsEnumerable()
                    .Sum(p => p.WorkingHoursInMin);


                var totalApprovedOThours = GetTotalApprovedOThours(EmpSallary.Employee_Code, objSubs.FK_CompanyRegistrationID, month, year);
                // PER DAY SALARY OF THE EMPLOYEE☻
                //decimal DalySalary = (Convert.ToDecimal(EmpSallary.Monthly) / Convert.ToDecimal(totalWorkingDay));
                decimal DalySalary = (Convert.ToDecimal(EmpSallary.Monthly) *12/ Convert.ToDecimal(365.0));
                decimal HourlySalary = (DalySalary / 8);
                decimal MinuteSalary = (HourlySalary / 60);

                // EMPLOYEE ALL SALARY COMPONENTS LIST FOR COMPONENTID☻
                var Salary_Cmp_List = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.PackageID == EmpSallary.FK_PackageID).ToList();
                //double HourlySalary = (DaylySalary /track_List.WorkingHours);


                //GET TOTAL HOLIDAYS  FOR THE SALARY MONTH AND CALCULATE SALARY☻
                var currentHolidays = getCurrentHolidays(month, year).Count();
                //decimal MonthlySalary = DalySalary * (track_List.Count() + currentHolidays);
                //decimal MonthlySalary = MinuteSalary * (Convert.ToDecimal(totalWorkingHoursInMin) + Convert.ToDecimal(totalApprovedOThours));
                decimal MonthlySalary = MinuteSalary * (Convert.ToDecimal(totalWorkingHoursInMin));
                //MonthlySalary += currentHolidays * DalySalary;    

                //GET TOTAL LEAVE FOR THE SALARY MONTH AND CALCULATE SALARY☻
                TempData["LeaveDetails"] = GetEmployeeLeaveByMonth(model);
                var totalLeaveInMonth = GetEmployeeLeaveByMonth(model).Sum(x => x.LeaveTaken ?? 0);
                MonthlySalary += totalLeaveInMonth * DalySalary;

                // SALARY COMPONENTS OF THE EMPLOYEE FOR THE MONTH☻
                List<tbl_SlaryComponents> TempComponentList = new List<tbl_SlaryComponents>();
                foreach (var item in Salary_Cmp_List)
                {
                    var t = context.tbl_SlaryComponents.FirstOrDefault(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.TypeID == item.FK_TypeID);
                    TempComponentList.Add(t);
                }
                decimal bascicSalary = 0;
                foreach (var item in TempComponentList)
                {
                    if (item.ComponentType == "Earning")
                    {
                        // EARNING OF THE EMPLOYEE OF THIS MONTH☻
                        SalaryComponents_EarningAndDeduction objSalaryComponents = new SalaryComponents_EarningAndDeduction();
                        decimal prcntg = Convert.ToDecimal(item.Percentage);
                        objSalaryComponents.ComponentType = item.ComponentType;
                        objSalaryComponents.Component = item.TypeName;
                        objSalaryComponents.Amount = (MonthlySalary * prcntg) / 100;
                        Earning_List.Add(objSalaryComponents);
                        if (item.TypeName.ToLower().Contains("basic"))
                        {
                            bascicSalary = (MonthlySalary * prcntg) / 100;
                        }
                        model.TotalEarnings += Convert.ToDecimal(objSalaryComponents.Amount);
                    }
                }
                //CALCULATE OT HOURS AND ADD EARNING LIST ☻
                SalaryComponents_EarningAndDeduction objtemp = CalculateOtHours(totalApprovedOThours, model, MinuteSalary);
                Earning_List.Add(objtemp);
                model.TotalEarnings += Convert.ToDecimal(objtemp.Amount);



                foreach (var item in TempComponentList)
                {
                    if (item.ComponentType == "Deduction")
                    {
                        // DEDUCTION OF THE EMPLOYEE OF THIS MONTH ☻ 
                        SalaryComponents_EarningAndDeduction objSalaryComponents = new SalaryComponents_EarningAndDeduction();
                        decimal prcntg = Convert.ToDecimal(item.Percentage);
                        objSalaryComponents.ComponentType = item.ComponentType;
                        objSalaryComponents.Component = item.TypeName;

                        if (item.DeductFrom == "Gross" && item.DeductType == "Percentage")
                        {
                            objSalaryComponents.Component = item.TypeName + " (" + prcntg + "% of  " + item.DeductFrom + ")";
                            objSalaryComponents.Amount = (model.TotalEarnings * prcntg) / 100;
                        }
                        else if (item.DeductFrom == "Basic" && item.DeductType == "Percentage")
                        {
                            objSalaryComponents.Component = item.TypeName + " (" + prcntg + "% of  " + item.DeductFrom + ")";
                            objSalaryComponents.Amount = (bascicSalary * prcntg) / 100;
                        }
                        else if (item.DeductFrom == "Gross" && item.DeductType == "Amount")
                        {
                            objSalaryComponents.Amount = (model.TotalEarnings - prcntg);
                        }
                        else if (item.DeductFrom == "Basic" && item.DeductType == "Amount")
                        {
                            objSalaryComponents.Amount = (bascicSalary - prcntg);
                        }
                        else if (item.DeductFrom == "Value" && item.DeductType == "Amount")
                        {
                            objSalaryComponents.Amount = prcntg;
                        }
                        Deduction_List.Add(objSalaryComponents);

                        model.TotalDeduction += Convert.ToDecimal(objSalaryComponents.Amount);
                    }
                }
                //P.TAX CALCULATION  ☻
                SalaryComponents_EarningAndDeduction objSalaryComponentPTAX = new SalaryComponents_EarningAndDeduction();
                if (model.TotalEarnings >= 10001 && model.TotalEarnings <= 15000)
                {
                    objSalaryComponentPTAX.Amount = 110;
                    objSalaryComponentPTAX.Component = "P.Tax";
                    objSalaryComponentPTAX.ComponentType = "Deduction";
                }
                else if (model.TotalEarnings >= 15001 && model.TotalEarnings <= 25000)
                {
                    objSalaryComponentPTAX.Amount = 130;
                    objSalaryComponentPTAX.Component = "P.Tax";
                    objSalaryComponentPTAX.ComponentType = "Deduction";
                }
                else if (model.TotalEarnings >= 25001 && model.TotalEarnings <= 40000)
                {
                    objSalaryComponentPTAX.Amount = 150;
                    objSalaryComponentPTAX.Component = "P.Tax";
                    objSalaryComponentPTAX.ComponentType = "Deduction";
                }
                else if (model.TotalEarnings >= 40001)
                {
                    objSalaryComponentPTAX.Amount = 200;
                    objSalaryComponentPTAX.Component = "P.Tax";
                    objSalaryComponentPTAX.ComponentType = "Deduction";
                }
                Deduction_List.Add(objSalaryComponentPTAX);
                model.TotalDeduction += Convert.ToDecimal(objSalaryComponentPTAX.Amount);

                //// GET EMPLOYEE LEAVE DETAIL 
                //TempData["LeaveDetails"] = GetEmployeeLeaveDetails(model);


                // ADD VALUE TO MODEL ☻
                model.NetAmount = model.TotalEarnings - model.TotalDeduction;
                model.AmountInWords = objglobal.NumberToWords(Convert.ToInt32(model.NetAmount)) + " ONLY.";
                model.OT = Convert.ToDecimal(totalApprovedOThours / 60);

                model.TotalWorkingDay = Convert.ToInt32(totalWorkingDay);
                model.TotalDaysPresent = Math.Round(Convert.ToDecimal(totalWorkingHoursInMin) / 60 / 8);
                //model.TotalAbsent = Math.Round(model.TotalWorkingDay - model.TotalDaysPresent) - currentHolidays;
                var a = Math.Round(model.TotalWorkingDay - (model.TotalDaysPresent + totalLeaveInMonth));
                model.TotalAbsent = a;
                //model.TotalAbsent = Math.Round(model.TotalWorkingDay - model.TotalDaysPresent) ;
                model.Holiday = currentHolidays;


                Filnal_List.AddRange(Earning_List);
                Filnal_List.AddRange(Deduction_List);
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Filnal_List);
        }

        private SalaryComponents_EarningAndDeduction CalculateOtHours(double totalApprovedOThours, Print_Payslip model, decimal MinuteSalary)
        {
            var result = (from cat in context.tbl_JobCategory
                          join jp in context.tbl_Emp_Joining_Probation on cat.CategoryID equals jp.FK_CategoryID
                          where jp.IsActive == true && jp.Employee_Code == model.FK_Employee_Code
                          select cat).FirstOrDefault();

            SalaryComponents_EarningAndDeduction objtemp = new SalaryComponents_EarningAndDeduction();
            
            if (result != null)
            {
                objtemp.ComponentType = "Earning";
                objtemp.Component = "O.T.";
                if (result.OTMultipllyValue.HasValue)
                {
                    objtemp.Amount = Convert.ToDecimal(MinuteSalary * (Convert.ToDecimal(totalApprovedOThours) * result.OTMultipllyValue));
                }
                else
                {
                    objtemp.Amount = Convert.ToDecimal(MinuteSalary * Convert.ToDecimal(totalApprovedOThours));
                }
            }
            else
            {
                objtemp.ComponentType = "Emp Joining Probation is not defined!";
            }
            return objtemp;
        }



        public List<LeaveBalance> GetEmployeeLeaveDetails(Print_Payslip model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<LeaveBalance> Bln_List = new List<LeaveBalance>();

            var year = Convert.ToInt32(model.Year);
            if (context.tbl_Emp_Joining_Probation.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code))
            {
                var DepartmentID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault().FK_DepartmentID;
                try
                {
                    var objSettings_AssignLeave = context.tbl_Settings_AssignLeave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.IsActive == true && x.FK_DepartmentID == DepartmentID).ToList();

                    foreach (var item in objSettings_AssignLeave)
                    {
                        var objLeaveBalance = new LeaveBalance();
                        var objEmp_Leave = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == item.Type).ToList();
                        var totalHalf = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Half").ToList().Sum(x => x.LeaveDays ?? 0));
                        var totalFull = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Full").ToList().Sum(x => x.LeaveDays ?? 0));
                        var totalLeave = Convert.ToDecimal((totalHalf * 0.5) + (totalFull * 1));

                        objLeaveBalance.AppliedFor = item.Type;
                        objLeaveBalance.LeaveAllowed = item.NoOfLeave ?? 0;
                        objLeaveBalance.LeaveTaken = totalLeave;
                        objLeaveBalance.LeaveDue = objLeaveBalance.LeaveAllowed - totalLeave;
                        Bln_List.Add(objLeaveBalance);
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Please assign a Department to the selected Employee !";
            }
            return Bln_List;
        }
        public List<LeaveBalance> GetEmployeeLeaveByMonth(Print_Payslip model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<LeaveBalance> Bln_List = new List<LeaveBalance>();

            var year = Convert.ToInt32(model.Year);
            var month = Convert.ToInt32(model.Month);
            if (context.tbl_Emp_Joining_Probation.Any(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code))
            {
                var DepartmentID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault().FK_DepartmentID;
                try
                {
                    var objSettings_AssignLeave = context.tbl_Settings_AssignLeave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.IsActive == true && x.FK_DepartmentID == DepartmentID).ToList();

                    foreach (var item in objSettings_AssignLeave)
                    {
                        var objLeaveBalance = new LeaveBalance();
                        var objEmp_Leave = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == item.Type).ToList();
                        var totalHalf = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Half").ToList().Sum(x => x.LeaveDays ?? 0));
                        var totalFull = Convert.ToDouble(objEmp_Leave.Where(m => m.LeaveType == "Full").ToList().Sum(x => x.LeaveDays ?? 0));
                        var totalLeave = Convert.ToDecimal((totalHalf * 0.5) + (totalFull * 1));

                        objLeaveBalance.AppliedFor = item.Type;
                        objLeaveBalance.LeaveAllowed = item.NoOfLeave ?? 0;
                        objLeaveBalance.LeaveTaken = totalLeave;
                        objLeaveBalance.LeaveDue = objLeaveBalance.LeaveAllowed - totalLeave;
                        Bln_List.Add(objLeaveBalance);
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Please assign a Department to the selected Employee !";
            }
            return Bln_List;
        }
        private IEnumerable<DateTime> getCurrentHolidays(int valMonth, int valYear)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            var objCompanyLocation = context.tbl_CompanyLocation.FirstOrDefault(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
            string TimeZone = string.Empty;
            if (objCompanyLocation != null)
            {
                TimeZone = objCompanyLocation.TimeZone;
            }
            int day = 1;
            //int month = objglobal.TimeZone(TimeZone).Month;
            //int year = objglobal.TimeZone(TimeZone).Year;
            int month = valMonth;
            int year = valYear;


            DateTime date = Convert.ToDateTime(month + "/" + day + "/" + year);

            var objSettingsHolidays = context.tbl_SettingsHolidays.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            var SameOfAllYear = objSettingsHolidays.Where(m => m.Date.Value.Month == month && m.SameDayEveryYear == true).ToList();
            var SameYearOnly = objSettingsHolidays.Where(m => m.Date.Value.Month == date.Month && m.Date.Value.Year == date.Year && m.SameDayEveryYear == false).ToList();
            List<DateTime> holidayDate = new List<DateTime>();
            foreach (var item in SameOfAllYear)
            {
                holidayDate.Add(Convert.ToDateTime(item.Date));
            }
            foreach (var item in SameYearOnly)
            {
                holidayDate.Add(Convert.ToDateTime(item.Date));
            }
            var finalDate = holidayDate.Select(m => m.Date).Distinct();
            return finalDate;
        }


        public double GetTotalApprovedOThours(string Employee_Code, string FK_CompanyRegistrationID, int month, int year)
        {
            var result = context.tbl_Emp_Attendance.Where(m => m.Employee_Code == Employee_Code && m.FK_CompanyRegistrationID == FK_CompanyRegistrationID
                && m.Date.Value.Month == month && m.Date.Value.Year == year && m.ApprovedHours != null).ToList();

            TimeSpan ts = new TimeSpan();
            double totalApprovedHoursInMinute = 0;
            foreach (var item in result)
            {
                totalApprovedHoursInMinute += Convert.ToDateTime(item.ApprovedHours).TimeOfDay.TotalMinutes;
            }
            return totalApprovedHoursInMinute;
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Process_Salary_List(string id)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            var result = context.tbl_ProcessSalary.Where(m => m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.IsActive == true).ToList();
            ViewData["Code_List_Employee"] = AllEmployeeCodeList();
            return PartialView(result);

        }




        #region Delete Process Salary
        public JsonResult DeleteProcessSalary(string salaryID)
        {
            salaryID = HttpUtility.UrlDecode(salaryID);
            if (salaryID != null)
            {
                int id = Convert.ToInt32(salaryID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_ProcessSalary objDetails = context.tbl_ProcessSalary.FirstOrDefault(d => d.ProcessSalaryID == id && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                try
                {
                    // Delete record from attendance Table
                    if (objDetails != null)
                    {
                        objDetails.IsActive = false;
                        objDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        return Json("Salary deleted successfully!");
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    return Json("Something is wrong!");
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";

            }
            return Json("Id not Found");

        }
        #endregion

    }
}