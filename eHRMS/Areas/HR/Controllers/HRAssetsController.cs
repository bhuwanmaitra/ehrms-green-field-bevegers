﻿using eHRMS.Areas.HR.Models;
using eHRMS.Models;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Repository;
using System.Data.Entity;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRAssetsController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/HRAssets
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assets_Dashboard()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assets_Management()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<CompanyAssets> AssetsList = new List<CompanyAssets>();
            try
            {
                AssetsList = context.tbl_CompanyAssets.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CompanyAssets
                {
                    AssetsID = s.AssetsID,
                    Name = s.Name,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(AssetsList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assets_Management_Add(string id)
        {
            CompanyAssets model = new CompanyAssets();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetAssetsDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        #region Get Project Details
        private CompanyAssets GetAssetsDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_CompanyAssets objdetails = context.tbl_CompanyAssets.Where(x => x.IsActive == true && x.AssetsID == ID).FirstOrDefault();

            CompanyAssets objlist = new CompanyAssets();
            if (objdetails != null)
            {
                objlist = new CompanyAssets()
                {
                    AssetsID = objdetails.AssetsID,
                    Name = objdetails.Name,
                    Description = objdetails.Description
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Assets_Management_Add(CompanyAssets model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int AssetsID = Convert.ToInt32(id);
                        tbl_CompanyAssets objAssetsDetails = context.tbl_CompanyAssets.First(d => d.AssetsID == AssetsID);
                        // Update Table
                        objAssetsDetails.Name = model.Name;
                        objAssetsDetails.Description = model.Description;
                        objAssetsDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objAssetsDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objAssetsDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Asset details updated successfully !";
                    }
                    else if (context.tbl_CompanyAssets.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Asset already added !";
                    }
                    else
                    {
                        tbl_CompanyAssets objAsset = new tbl_CompanyAssets()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_CompanyAssets.Add(objAsset);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Asset details added successfully !";
                        ModelState.Clear();
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        #region Delete Asset
        public JsonResult DeleteAsset(string AssetsID)
        {
            AssetsID = HttpUtility.UrlDecode(AssetsID);
            if (AssetsID != null)
            {
                int id = Convert.ToInt32(AssetsID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_CompanyAssets objAssetsDetails = context.tbl_CompanyAssets.First(d => d.AssetsID == id);
                try
                {
                    // Update Table
                    objAssetsDetails.IsActive = false;
                    objAssetsDetails.UpdatedBy = objSubs.Username;
                    objAssetsDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objAssetsDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Asset details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion
    }
}