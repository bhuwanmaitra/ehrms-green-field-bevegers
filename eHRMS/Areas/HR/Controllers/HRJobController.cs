﻿using eHRMS.Areas.HR.Models;
using eHRMS.Models;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Repository;
using System.Data.Entity;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRJobController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        // GET: HR/HRJob
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management()
        {
            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Qualification()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobQualification> qulList = new List<JobQualification>();
            try
            {
                qulList = context.tbl_JobQualification.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobQualification
                {
                    JobQualificationID=s.JobQualificationID,
                    Name=s.Name,
                    Description=s.Description,                    
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(qulList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Qualification_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetQulDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Qualification Details
        public  JobQualification GetQulDetails(string id)
        {
            int JobQualificationID = Convert.ToInt32(id);
            tbl_JobQualification objdetails = context.tbl_JobQualification.Where(x => x.IsActive == true && x.JobQualificationID == JobQualificationID).FirstOrDefault();

            JobQualification objlist = new JobQualification();
            if (objdetails != null)
            {
                objlist = new JobQualification()
                {
                    JobQualificationID=objdetails.JobQualificationID,
                    Name=objdetails.Name,
                    Description=objdetails.Description,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Qualification_Add(JobQualification model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int JobQualificationID = Convert.ToInt32(id);
                        tbl_JobQualification objQulDetails = context.tbl_JobQualification.First(d => d.JobQualificationID == JobQualificationID);
                        // Update Table
                        objQulDetails.Name = model.Name;
                        objQulDetails.Description = model.Description;
                        objQulDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objQulDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objQulDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Qualification updated successfully !";
                    }
                    else
                    {
                        tbl_JobQualification objQul = new tbl_JobQualification()
                        {
                            Name=model.Name,
                            Description=model.Description,                            
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobQualification.Add(objQul);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Qualification added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Qualification
        public JsonResult DeleteQualification(string JobQualificationID)
        {
            JobQualificationID = HttpUtility.UrlDecode(JobQualificationID);
            if (JobQualificationID != null)
            {
                int id = Convert.ToInt32(JobQualificationID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobQualification objQulDetails = context.tbl_JobQualification.First(d => d.JobQualificationID == id);
                try
                {
                    // Update Table
                    objQulDetails.IsActive = false;
                    objQulDetails.UpdatedBy = objSubs.Username;
                    objQulDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objQulDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Qulification deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Language()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobLanguage> lnglList = new List<JobLanguage>();
            try
            {
                lnglList = context.tbl_JobLanguage.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobLanguage
                {
                    JobLanguageID=s.JobLanguageID,
                    Name = s.Name,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(lnglList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Language_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetLngDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Language Details
        private JobLanguage GetLngDetails(string id)
        {
            int JobLanguageID = Convert.ToInt32(id);
            tbl_JobLanguage objdetails = context.tbl_JobLanguage.Where(x => x.IsActive == true && x.JobLanguageID == JobLanguageID).FirstOrDefault();

            JobLanguage objlist = new JobLanguage();
            if (objdetails != null)
            {
                objlist = new JobLanguage()
                {
                    JobLanguageID = objdetails.JobLanguageID,
                    Name = objdetails.Name,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Language_Add(JobLanguage model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int JobLanguageID = Convert.ToInt32(id);
                        tbl_JobLanguage objLngDetails = context.tbl_JobLanguage.First(d => d.JobLanguageID == JobLanguageID);
                        // Update Table
                        objLngDetails.Name = model.Name;
                        objLngDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objLngDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objLngDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Language updated successfully !";
                    }
                    else
                    {
                        tbl_JobLanguage objLng = new tbl_JobLanguage()
                        {
                            Name = model.Name,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobLanguage.Add(objLng);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Language added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Language
        public JsonResult DeleteLanguage(string JobLanguageID)
        {
            JobLanguageID = HttpUtility.UrlDecode(JobLanguageID);
            if (JobLanguageID != null)
            {
                int id = Convert.ToInt32(JobLanguageID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobLanguage objLngDetails = context.tbl_JobLanguage.First(d => d.JobLanguageID == id);
                try
                {
                    // Update Table
                    objLngDetails.IsActive = false;
                    objLngDetails.UpdatedBy = objSubs.Username;
                    objLngDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objLngDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Language deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Code()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<EmployeeCode> EmpCodelList = new List<EmployeeCode>();
            try
            {
                EmpCodelList = context.tbl_JobEmployeeCode.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new EmployeeCode
                {
                    JobEmpCodeID=s.JobEmpCodeID,
                    EmpCodeType=s.EmpCodeType,
                    Prefix=s.Prefix,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpCodelList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Code_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetEmpCodeDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Emp_Code Details
        private EmployeeCode GetEmpCodeDetails(string id)
        {
            int JobEmpCodeID = Convert.ToInt32(id);
            tbl_JobEmployeeCode objdetails = context.tbl_JobEmployeeCode.Where(x => x.IsActive == true && x.JobEmpCodeID == JobEmpCodeID).FirstOrDefault();

            EmployeeCode objlist = new EmployeeCode();
            if (objdetails != null)
            {
                objlist = new EmployeeCode()
                {
                    JobEmpCodeID=objdetails.JobEmpCodeID,
                    EmpCodeType = objdetails.EmpCodeType,
                    Prefix=objdetails.Prefix,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Code_Add(EmployeeCode model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int JobEmpCodeID = Convert.ToInt32(id);
                        tbl_JobEmployeeCode objCodeDetails = context.tbl_JobEmployeeCode.First(d => d.JobEmpCodeID == JobEmpCodeID);
                        // Update Table
                        objCodeDetails.EmpCodeType = model.EmpCodeType;
                        objCodeDetails.Prefix = model.Prefix;
                        objCodeDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objCodeDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objCodeDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Code Criteria updated successfully !";
                    }
                    else if (context.tbl_JobEmployeeCode.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        ViewBag.ErrorMsg = "Employee Code Criteria already added !";
                    }
                    else
                    {
                        tbl_JobEmployeeCode objCode = new tbl_JobEmployeeCode()
                        {
                            EmpCodeType = model.EmpCodeType,
                            Prefix = model.Prefix,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobEmployeeCode.Add(objCode);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Employee Code Criteria added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Employee Code
        public JsonResult DeleteEmpCode(string JobEmpCodeID)
        {
            JobEmpCodeID = HttpUtility.UrlDecode(JobEmpCodeID);
            if (JobEmpCodeID != null)
            {
                int id = Convert.ToInt32(JobEmpCodeID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobEmployeeCode objCodDetails = context.tbl_JobEmployeeCode.First(d => d.JobEmpCodeID == id);
                try
                {
                    // Update Table
                    objCodDetails.IsActive = false;
                    objCodDetails.UpdatedBy = objSubs.Username;
                    objCodDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objCodDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Employee Code Criteria deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contract_Policy()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<ContractPolicy> ContractlList = new List<ContractPolicy>();
            try
            {
                ContractlList = context.tbl_JobContractPolicy.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new ContractPolicy
                {
                    ContractPolicyID=s.ContractPolicyID,
                    Name=s.Name,
                    Description=s.Description,
                    Duration=s.Duration,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(ContractlList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contract_Policy_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetContractPolicyDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Contract Policy Details
        private ContractPolicy GetContractPolicyDetails(string id)
        {
            int ContractPolicyID = Convert.ToInt32(id);
            tbl_JobContractPolicy objdetails = context.tbl_JobContractPolicy.Where(x => x.IsActive == true && x.ContractPolicyID == ContractPolicyID).FirstOrDefault();

            ContractPolicy objlist = new ContractPolicy();
            if (objdetails != null)
            {
                objlist = new ContractPolicy()
                {
                    ContractPolicyID = objdetails.ContractPolicyID,
                    Name = objdetails.Name,
                    Description = objdetails.Description,
                    Duration = objdetails.Duration
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contract_Policy_Add(ContractPolicy model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int ContractPolicyID = Convert.ToInt32(id);
                        tbl_JobContractPolicy objContractDetails = context.tbl_JobContractPolicy.First(d => d.ContractPolicyID == ContractPolicyID);
                        // Update Table
                        objContractDetails.Name = model.Name;
                        objContractDetails.Description = model.Description;
                        objContractDetails.Duration = model.Duration;
                        objContractDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objContractDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objContractDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Contract Policy updated successfully !";
                    }
                    else if (context.tbl_JobContractPolicy.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Contract Policy with same name already added !";
                    }
                    else
                    {
                        tbl_JobContractPolicy objContract = new tbl_JobContractPolicy()
                        {
                            Name=model.Name,
                            Description=model.Description,
                            Duration=model.Duration,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobContractPolicy.Add(objContract);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Contract Policy added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Contract Policy
        public JsonResult DeleteContractPolicy(string ContractPolicyID)
        {
            ContractPolicyID = HttpUtility.UrlDecode(ContractPolicyID);
            if (ContractPolicyID != null)
            {
                int id = Convert.ToInt32(ContractPolicyID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobContractPolicy objContractDetails = context.tbl_JobContractPolicy.First(d => d.ContractPolicyID == id);
                try
                {
                    // Update Table
                    objContractDetails.IsActive = false;
                    objContractDetails.UpdatedBy = objSubs.Username;
                    objContractDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objContractDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Contract Policy deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Probation_Policy()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<ProbationPolicy> ProbationlList = new List<ProbationPolicy>();
            try
            {
                ProbationlList = context.tbl_JobProbationPolicy.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new ProbationPolicy
                {
                    ProbationPolicyID=s.ProbationPolicyID,
                    Category=s.Category,
                    Duration=s.Duration,
                    PeriodExtendable=s.PeriodExtendable,
                    ConfirmationReminder=s.ConfirmationReminder,
                    FK_ContractPolicyID=s.FK_ContractPolicyID.ToString(),
                    SendConfirmLetter = s.SendConfirmLetter,
                    Remarks = s.Remarks,
                    ResignApplicable=s.ResignApplicable,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(ProbationlList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Probation_Policy_Add(string id)
        {
            ViewData["ContractPolicy"] = ContractPolicyList();
            //ViewBag.ContractPolicy = ContractPolicyList();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetProbationPolicyDetails(id));
            }
            else
            {
                return View();
            }
        }
        public List<SelectListItem> ContractPolicyList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var ContractResult = context.tbl_JobContractPolicy.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in ContractResult)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.ContractPolicyID.ToString() });
                }                
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        #region Get Contract Policy Details
        private ProbationPolicy GetProbationPolicyDetails(string id)
        {
            int ProbationPolicyID = Convert.ToInt32(id);
            tbl_JobProbationPolicy objdetails = context.tbl_JobProbationPolicy.Where(x => x.IsActive == true && x.ProbationPolicyID == ProbationPolicyID).FirstOrDefault();

            ProbationPolicy objlist = new ProbationPolicy();
            if (objdetails != null)
            {
                objlist = new ProbationPolicy()
                {
                    ProbationPolicyID=objdetails.ProbationPolicyID,
                    Category=objdetails.Category,
                    Duration=objdetails.Duration,
                    PeriodExtendable=objdetails.PeriodExtendable,
                    ConfirmationReminder=objdetails.ConfirmationReminder,
                    FK_ContractPolicyID=objdetails.FK_ContractPolicyID.ToString(),
                    SendConfirmLetter=objdetails.SendConfirmLetter,
                    Remarks=objdetails.Remarks,
                    ResignApplicable = objdetails.ResignApplicable
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Probation_Policy_Add(ProbationPolicy model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int ProbationPolicyID = Convert.ToInt32(id);
                        tbl_JobProbationPolicy objPolicyDetails = context.tbl_JobProbationPolicy.First(d => d.ProbationPolicyID == ProbationPolicyID);
                        // Update Table
                        objPolicyDetails.Category = model.Category;
                        objPolicyDetails.Duration = model.Duration;
                        objPolicyDetails.PeriodExtendable = model.PeriodExtendable;
                        objPolicyDetails.ConfirmationReminder = model.ConfirmationReminder;
                        objPolicyDetails.FK_ContractPolicyID = Convert.ToInt32(model.FK_ContractPolicyID);
                        objPolicyDetails.SendConfirmLetter = true;
                        objPolicyDetails.Remarks = model.Remarks;
                        objPolicyDetails.ResignApplicable = model.ResignApplicable;
                        objPolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objPolicyDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objPolicyDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Probation Policy updated successfully !";
                        ViewData["ContractPolicy"] = ContractPolicyList();
                    }
                    else if (context.tbl_JobProbationPolicy.Any(x => x.Category.ToUpper() == model.Category.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Probation Policy with same name already added !";
                        ViewData["ContractPolicy"] = ContractPolicyList();
                    }
                    else
                    {
                        tbl_JobProbationPolicy objPolicy = new tbl_JobProbationPolicy()
                        {
                            Category=model.Category,
                            Duration = model.Duration,
                            PeriodExtendable=model.PeriodExtendable,
                            ConfirmationReminder=model.ConfirmationReminder,
                            FK_ContractPolicyID = Convert.ToInt32(model.FK_ContractPolicyID),
                            SendConfirmLetter=true,
                            ResignApplicable=model.ResignApplicable,
                            Remarks = model.Remarks,                            
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobProbationPolicy.Add(objPolicy);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Probation Policy added successfully !";
                        ModelState.Clear();
                        ViewData["ContractPolicy"] = ContractPolicyList();
                    }                    
                    return View();
                }
                else
                {
                    ViewData["ContractPolicy"] = ContractPolicyList();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Probation Policy
        public JsonResult DeletePPolicy(string ProbationPolicyID)
        {
            ProbationPolicyID = HttpUtility.UrlDecode(ProbationPolicyID);
            if (ProbationPolicyID != null)
            {
                int id = Convert.ToInt32(ProbationPolicyID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobProbationPolicy objPolicyDetails = context.tbl_JobProbationPolicy.First(d => d.ProbationPolicyID == id);
                try
                {
                    // Update Table
                    objPolicyDetails.IsActive = false;
                    objPolicyDetails.UpdatedBy = objSubs.Username;
                    objPolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objPolicyDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Probation Policy deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Destination_Details()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobDestination> DestinationlList = new List<JobDestination>();
            try
            {
                DestinationlList = context.tbl_JobDestination.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobDestination
                {
                    DestinationID=s.DestinationID,
                    Name=s.Name,
                    ContactNumber=s.ContactNumber,
                    Email=s.Email,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(DestinationlList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Destination_Details_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetDestinationDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Destination Details
        private JobDestination GetDestinationDetails(string id)
        {
            int DestinationID = Convert.ToInt32(id);
            tbl_JobDestination objdetails = context.tbl_JobDestination.Where(x => x.IsActive == true && x.DestinationID == DestinationID).FirstOrDefault();

            JobDestination objlist = new JobDestination();
            if (objdetails != null)
            {
                objlist = new JobDestination()
                {                   
                    Name = objdetails.Name,
                    ContactNumber=objdetails.ContactNumber,
                    Email = objdetails.Email
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Destination_Details_Add(JobDestination model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int DestinationID = Convert.ToInt32(id);
                        tbl_JobDestination objDestinationDetails = context.tbl_JobDestination.First(d => d.DestinationID == DestinationID);
                        // Update Table
                        objDestinationDetails.Name = model.Name;
                        objDestinationDetails.ContactNumber = model.ContactNumber;
                        objDestinationDetails.Email = model.Email;
                        objDestinationDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDestinationDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDestinationDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Job Destination updated successfully !";
                    }
                    else if (context.tbl_JobDestination.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Job Destination with same name already added !";
                    }
                    else
                    {
                        tbl_JobDestination objDestination = new tbl_JobDestination()
                        {
                            Name = model.Name,
                            ContactNumber = model.ContactNumber,
                            Email = model.Email,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobDestination.Add(objDestination);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Job Destination added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Destination
        public JsonResult DeleteDestination(string DestinationID)
        {
            DestinationID = HttpUtility.UrlDecode(DestinationID);
            if (DestinationID != null)
            {
                int id = Convert.ToInt32(DestinationID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobDestination objDestinationDetails = context.tbl_JobDestination.First(d => d.DestinationID == id);
                try
                {
                    // Update Table
                    objDestinationDetails.IsActive = false;
                    objDestinationDetails.UpdatedBy = objSubs.Username;
                    objDestinationDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDestinationDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Job Destination deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Department()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobDepartment> DepartmentlList = new List<JobDepartment>();
            try
            {
                DepartmentlList = context.tbl_JobDepartment.Where(x => x.IsActive == true && x.Fk_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobDepartment
                {
                    DepartmentID=s.DepartmentID,
                    Name = s.Name,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(DepartmentlList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Department_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetDepartmentDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Department Details
        private JobDepartment GetDepartmentDetails(string id)
        {
            int DepartmentID = Convert.ToInt32(id);
            tbl_JobDepartment objdetails = context.tbl_JobDepartment.Where(x => x.IsActive == true && x.DepartmentID == DepartmentID).FirstOrDefault();

            JobDepartment objlist = new JobDepartment();
            if (objdetails != null)
            {
                objlist = new JobDepartment()
                {
                    Name = objdetails.Name,
                    Description=objdetails.Description
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Department_Add(JobDepartment model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int DepartmentID = Convert.ToInt32(id);
                        tbl_JobDepartment objDepartmentDetails = context.tbl_JobDepartment.First(d => d.DepartmentID == DepartmentID && d.Fk_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                        // Update Table
                        objDepartmentDetails.Name = model.Name;
                        objDepartmentDetails.Description = model.Description;
                        objDepartmentDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDepartmentDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDepartmentDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Department updated successfully !";
                    }
                    else if (context.tbl_JobDepartment.Any(x => x.Name.ToUpper() == model.Name.ToUpper() && x.Fk_CompanyRegistrationID== objSubs.FK_CompanyRegistrationID))
                    {
                        ViewBag.ErrorMsg = "Department with same name already added !";
                    }
                    else
                    {
                        tbl_JobDepartment objDepartment = new tbl_JobDepartment()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            Fk_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobDepartment.Add(objDepartment);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Department added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Department
        public JsonResult DeleteDepartment(string DepartmentID)
        {
            DepartmentID = HttpUtility.UrlDecode(DepartmentID);
            if (DepartmentID != null)
            {
                int id = Convert.ToInt32(DepartmentID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobDepartment objDepartmentDetails = context.tbl_JobDepartment.First(d => d.DepartmentID == id);
                try
                {
                    // Update Table
                    objDepartmentDetails.IsActive = false;
                    objDepartmentDetails.UpdatedBy = objSubs.Username;
                    objDepartmentDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDepartmentDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Department deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Category_Details()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobCategory> CategorylList = new List<JobCategory>();
            try
            {
                CategorylList = context.tbl_JobCategory.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobCategory
                {
                    CategoryID = s.CategoryID,
                    Name = s.Name,
                    Description = s.Description,
                    OTMultipllyValue = s.OTMultipllyValue,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(CategorylList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Category_Details_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetCategoryDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Category Details
        private JobCategory GetCategoryDetails(string id)
        {
            int CategoryID = Convert.ToInt32(id);
            tbl_JobCategory objdetails = context.tbl_JobCategory.Where(x => x.IsActive == true && x.CategoryID == CategoryID).FirstOrDefault();

            JobCategory objlist = new JobCategory();
            if (objdetails != null)
            {
                objlist = new JobCategory()
                {
                    Name = objdetails.Name,
                    Description = objdetails.Description,
                    OTMultipllyValue = objdetails.OTMultipllyValue

                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Category_Details_Add(JobCategory model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int CategoryID = Convert.ToInt32(id);
                        tbl_JobCategory objCategoryDetails = context.tbl_JobCategory.First(d => d.CategoryID == CategoryID);
                        // Update Table
                        objCategoryDetails.Name = model.Name;
                        objCategoryDetails.Description = model.Description;
                        objCategoryDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objCategoryDetails.UpdatedBy = objSubs.Username;
                        objCategoryDetails.OTMultipllyValue = model.OTMultipllyValue;
                        context.Entry(objCategoryDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Category updated successfully !";
                    }
                    else if (context.tbl_JobCategory.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Category with same name already added !";
                    }
                    else
                    {
                        tbl_JobCategory objCategory = new tbl_JobCategory()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            OTMultipllyValue = model.OTMultipllyValue,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobCategory.Add(objCategory);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Category added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Category
        public JsonResult DeleteCategory(string CategoryID)
        {
            CategoryID = HttpUtility.UrlDecode(CategoryID);
            if (CategoryID != null)
            {
                int id = Convert.ToInt32(CategoryID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobCategory objCategoryDetails = context.tbl_JobCategory.First(d => d.CategoryID == id);
                try
                {
                    // Update Table
                    objCategoryDetails.IsActive = false;
                    objCategoryDetails.UpdatedBy = objSubs.Username;
                    objCategoryDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objCategoryDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Category deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Grade_Details()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobGrade> GradelList = new List<JobGrade>();
            try
            {
                GradelList = context.tbl_JobGrade.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobGrade
                {
                    GradeID = s.GradeID,
                    Name = s.Name,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(GradelList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Grade_Details_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetGradeDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Grade Details
        private JobGrade GetGradeDetails(string id)
        {
            int GradeID = Convert.ToInt32(id);
            tbl_JobGrade objdetails = context.tbl_JobGrade.Where(x => x.IsActive == true && x.GradeID == GradeID).FirstOrDefault();

            JobGrade objlist = new JobGrade();
            if (objdetails != null)
            {
                objlist = new JobGrade()
                {
                    Name = objdetails.Name,
                    Description = objdetails.Description
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Grade_Details_Add(JobDepartment model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int GradeID = Convert.ToInt32(id);
                        tbl_JobGrade objGradeDetails = context.tbl_JobGrade.First(d => d.GradeID == GradeID);
                        // Update Table
                        objGradeDetails.Name = model.Name;
                        objGradeDetails.Description = model.Description;
                        objGradeDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objGradeDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objGradeDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Grade updated successfully !";
                    }
                    else if (context.tbl_JobGrade.Any(x => x.Name.ToUpper() == model.Name.ToUpper() && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        ViewBag.ErrorMsg = "Grade with same name already added !";
                    }
                    else
                    {
                        tbl_JobGrade objGrade = new tbl_JobGrade()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobGrade.Add(objGrade);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Grade added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Grade
        public JsonResult DeleteGrade(string GradeID)
        {
            GradeID = HttpUtility.UrlDecode(GradeID);
            if (GradeID != null)
            {
                int id = Convert.ToInt32(GradeID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobGrade objGradeDetails = context.tbl_JobGrade.First(d => d.GradeID == id);
                try
                {
                    // Update Table
                    objGradeDetails.IsActive = false;
                    objGradeDetails.UpdatedBy = objSubs.Username;
                    objGradeDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objGradeDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Grade deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Skill_Details()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobSkill> SkillList = new List<JobSkill>();
            try
            {
                SkillList = context.tbl_JobSkill.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobSkill
                {
                    SkillID = s.SkillID,
                    Type = s.Type,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(SkillList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Skill_Details_Add(string id)
        {
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetSkillDetails(id));
            }
            else
            {
                return View();
            }
        }

        #region Get Skill Details
        private JobSkill GetSkillDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_JobSkill objdetails = context.tbl_JobSkill.Where(x => x.IsActive == true && x.SkillID == ID).FirstOrDefault();

            JobSkill objlist = new JobSkill();
            if (objdetails != null)
            {
                objlist = new JobSkill()
                {
                    Type = objdetails.Type,
                    Description = objdetails.Description
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Skill_Details_Add(JobSkill model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int SkillID = Convert.ToInt32(id);
                        tbl_JobSkill objSkillDetails = context.tbl_JobSkill.First(d => d.SkillID == SkillID);
                        // Update Table
                        objSkillDetails.Type = model.Type;
                        objSkillDetails.Description = model.Description;
                        objSkillDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objSkillDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objSkillDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Job skill updated successfully !";
                    }
                    else if (context.tbl_JobSkill.Any(x => x.Type.ToUpper() == model.Type.ToUpper() && x.FK_CompanyRegistrationID==objSubs.FK_CompanyRegistrationID))
                    {
                        ViewBag.ErrorMsg = "Grade with same name already added !";
                    }
                    else
                    {
                        tbl_JobSkill objSkill = new tbl_JobSkill()
                        {
                            Type = model.Type,
                            Description = model.Description,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobSkill.Add(objSkill);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Job skill added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Delete Skill
        public JsonResult DeleteSkill(string SkillID)
        {
            SkillID = HttpUtility.UrlDecode(SkillID);
            if (SkillID != null)
            {
                int id = Convert.ToInt32(SkillID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobSkill objSkillDetails = context.tbl_JobSkill.First(d => d.SkillID == id);
                try
                {
                    // Update Table
                    objSkillDetails.IsActive = false;
                    objSkillDetails.UpdatedBy = objSubs.Username;
                    objSkillDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objSkillDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Job skill deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Document()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<JobDocument> DocumentList = new List<JobDocument>();
            try
            {
                DocumentList = context.tbl_JobDocument.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new JobDocument
                {
                    DocumentID=s.DocumentID,
                    Name = s.Name,
                    Description = s.Description,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(DocumentList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Document_Add(string id)
        {
            JobDocument model = new JobDocument();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetDocumentDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        #region Get Document Details
        private JobDocument GetDocumentDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_JobDocument objdetails = context.tbl_JobDocument.Where(x => x.IsActive == true && x.DocumentID == ID).FirstOrDefault();

            JobDocument objlist = new JobDocument();
            if (objdetails != null)
            {
                objlist = new JobDocument()
                {
                    Name = objdetails.Name,
                    Description = objdetails.Description
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Document_Add(JobDocument model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int DocumentID = Convert.ToInt32(id);
                        tbl_JobDocument objDocumentDetails = context.tbl_JobDocument.First(d => d.DocumentID == DocumentID);
                        // Update Table
                        objDocumentDetails.Name = model.Name;
                        objDocumentDetails.Description = model.Description;
                        objDocumentDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objDocumentDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objDocumentDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Document details updated successfully !";
                    }
                    else if (context.tbl_JobDocument.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Document type already added !";
                    }
                    else
                    {
                        tbl_JobDocument objDocument = new tbl_JobDocument()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobDocument.Add(objDocument);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Document details added successfully !";
                        ModelState.Clear();
                    }
                    return View();
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        #region Delete Document
        public JsonResult DeleteDocument(string DocumentID)
        {
            DocumentID = HttpUtility.UrlDecode(DocumentID);
            if (DocumentID != null)
            {
                int id = Convert.ToInt32(DocumentID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobDocument objDocumentDetails = context.tbl_JobDocument.First(d => d.DocumentID == id);
                try
                {
                    // Update Table
                    objDocumentDetails.IsActive = false;
                    objDocumentDetails.UpdatedBy = objSubs.Username;
                    objDocumentDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objDocumentDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Document details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Leave()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<LeavePolicy> LeavePolicyList = new List<LeavePolicy>();
            try
            {
                LeavePolicyList = context.tbl_JobLeavePolicy.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new LeavePolicy
                {
                    LeavePolicyID=s.LeavePolicyID,
                    CategoryName=s.CategoryName,
                    FK_DepartmentID=s.FK_DepartmentID,
                    NoticePeriod=s.NoticePeriod,
                    Remarks=s.Remarks,
                    EffectiveDate=s.EffectiveDate,
                    ApplicableTo=s.ApplicableTo,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(LeavePolicyList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Leave_Add(string id)
        {
            ViewData["DepartmentDrop"] = DepartmentList();
            LeavePolicy model = new LeavePolicy();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetLeavePolicyDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        public List<SelectListItem> DepartmentList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_JobDepartment.Where(w => w.IsActive == true && w.Fk_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.DepartmentID.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        #region Get Leave_Policy Details
        private LeavePolicy GetLeavePolicyDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_JobLeavePolicy objdetails = context.tbl_JobLeavePolicy.Where(x => x.IsActive == true && x.LeavePolicyID == ID).FirstOrDefault();

            LeavePolicy objlist = new LeavePolicy();
            if (objdetails != null)
            {
                objlist = new LeavePolicy()
                {
                    LeavePolicyID=objdetails.LeavePolicyID,
                    CategoryName=objdetails.CategoryName,
                    FK_DepartmentID=objdetails.FK_DepartmentID,
                    NoticePeriod=objdetails.NoticePeriod,
                    Remarks=objdetails.Remarks,
                    EffectiveDate=objdetails.EffectiveDate,
                    ApplicableTo=objdetails.ApplicableTo
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Employee_Leave_Add(LeavePolicy model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int LeavePolicyID = Convert.ToInt32(id);
                        tbl_JobLeavePolicy objLeavePolicyDetails = context.tbl_JobLeavePolicy.First(d => d.LeavePolicyID == LeavePolicyID);
                        // Update Table
                        objLeavePolicyDetails.CategoryName = model.CategoryName;
                        objLeavePolicyDetails.FK_DepartmentID = model.FK_DepartmentID;
                        objLeavePolicyDetails.NoticePeriod = model.NoticePeriod;
                        objLeavePolicyDetails.Remarks = model.Remarks;
                        objLeavePolicyDetails.EffectiveDate = model.EffectiveDate;
                        objLeavePolicyDetails.ApplicableTo = model.ApplicableTo;
                        objLeavePolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objLeavePolicyDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objLeavePolicyDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Leave Policy updated successfully !";
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    else if (context.tbl_JobLeavePolicy.Any(x => x.FK_DepartmentID==model.FK_DepartmentID))
                    {
                        ViewBag.ErrorMsg = "Leave Policy details already added for this department !";
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    else
                    {
                        tbl_JobLeavePolicy objLeavePolicy = new tbl_JobLeavePolicy()
                        {
                            CategoryName=model.CategoryName,
                            FK_DepartmentID=model.FK_DepartmentID,
                            NoticePeriod=model.NoticePeriod,
                            Remarks=model.Remarks,
                            EffectiveDate=model.EffectiveDate,
                            ApplicableTo=model.ApplicableTo,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_JobLeavePolicy.Add(objLeavePolicy);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Leave Policy details added successfully !";
                        ModelState.Clear();
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    return View(model);
                }
                else
                {
                    ViewData["DepartmentDrop"] = DepartmentList();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }

        #region Delete LeavePolicy
        public JsonResult DeleteLeavePolicy(string LeavePolicyID)
        {
            LeavePolicyID = HttpUtility.UrlDecode(LeavePolicyID);
            if (LeavePolicyID != null)
            {
                int id = Convert.ToInt32(LeavePolicyID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_JobLeavePolicy objLeavePolicyDetails = context.tbl_JobLeavePolicy.First(d => d.LeavePolicyID == id);
                try
                {
                    // Update Table
                    objLeavePolicyDetails.IsActive = false;
                    objLeavePolicyDetails.UpdatedBy = objSubs.Username;
                    objLeavePolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objLeavePolicyDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Leave Policy details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contractual_Policy()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<ContractualPolicy> CnPolicyList = new List<ContractualPolicy>();
            try
            {
                CnPolicyList = context.tbl_ContractualPolicy.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new ContractualPolicy
                {
                    ContractualID=s.ContractualID,
                    Name=s.Name,
                    Description=s.Description,
                    Duration=s.Duration,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(CnPolicyList);
            
        }

        #region Delete CnPolicy
        public JsonResult DeleteCnPolicy(string ContractualID)
        {
            ContractualID = HttpUtility.UrlDecode(ContractualID);
            if (ContractualID != null)
            {
                int id = Convert.ToInt32(ContractualID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_ContractualPolicy objCnPolicyDetails = context.tbl_ContractualPolicy.First(d => d.ContractualID == id);
                try
                {
                    // Update Table
                    objCnPolicyDetails.IsActive = false;
                    objCnPolicyDetails.UpdatedBy = objSubs.Username;
                    objCnPolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                    context.Entry(objCnPolicyDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Contractual Policy details deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contractual_Policy_Add(string id)
        {
            ViewData["DepartmentDrop"] = DepartmentList();
            ContractualPolicy model = new ContractualPolicy();
            if (id != null)
            {
                id = HttpUtility.UrlDecode(id);
                return View(GetCnPolicyDetails(id));
            }
            else
            {
                return View(model);
            }
        }

        #region Get Cn_Policy Details
        private ContractualPolicy GetCnPolicyDetails(string id)
        {
            int ID = Convert.ToInt32(id);
            tbl_ContractualPolicy objdetails = context.tbl_ContractualPolicy.Where(x => x.IsActive == true && x.ContractualID == ID).FirstOrDefault();

            ContractualPolicy objlist = new ContractualPolicy();
            if (objdetails != null)
            {
                objlist = new ContractualPolicy()
                {
                    ContractualID=objdetails.ContractualID,
                    Name=objdetails.Name,
                    Description=objdetails.Description,
                    Duration=objdetails.Duration,
                    FK_CompanyRegistrationID = objdetails.FK_CompanyRegistrationID
                };
                return objlist;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                return objlist;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Job_Management_Contractual_Policy_Add(ContractualPolicy model, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    id = HttpUtility.UrlDecode(id);
                    if (id != "" && id != null)
                    {
                        int ContractualID = Convert.ToInt32(id);
                        tbl_ContractualPolicy objCnPolicyDetails = context.tbl_ContractualPolicy.First(d => d.ContractualID == ContractualID);
                        // Update Table
                        objCnPolicyDetails.Name=model.Name;
                        objCnPolicyDetails.Description = model.Description;
                        objCnPolicyDetails.Duration = model.Duration;
                        objCnPolicyDetails.UpdatedOn = objglobal.TimeZone(objglobal.getTimeZone());
                        objCnPolicyDetails.UpdatedBy = objSubs.Username;
                        context.Entry(objCnPolicyDetails).State = EntityState.Modified;
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Contractual Policy updated successfully !";
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    else if (context.tbl_ContractualPolicy.Any(x => x.Name.ToUpper() == model.Name.ToUpper()))
                    {
                        ViewBag.ErrorMsg = "Policy details already added !";
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    else
                    {
                        tbl_ContractualPolicy objCnPolicy = new tbl_ContractualPolicy()
                        {
                            Name=model.Name,
                            Description=model.Description,
                            Duration=model.Duration,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objglobal.getTimeZone()),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_ContractualPolicy.Add(objCnPolicy);
                        context.SaveChanges();
                        ViewBag.SuccessMsg = "Contractual Policy details added successfully !";
                        ModelState.Clear();
                        ViewData["DepartmentDrop"] = DepartmentList();
                    }
                    return View(model);
                }
                else
                {
                    ViewData["DepartmentDrop"] = DepartmentList();
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }
    }
}