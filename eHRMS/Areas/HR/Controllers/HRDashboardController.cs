﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Global;
using eHRMS.Areas.HR.Models;
using eHRMS.Repository;
using eHRMS.Models;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace eHRMS.Areas.HR.Controllers
{
    public class HRDashboardController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        string sqlQuery = string.Empty;

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public ActionResult Dashboard()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AjaxMethod()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

            string query = "select [CTCFrom]+'-'+[CTCTo] as ShipCity ,COUNT(PackageID) as TotalOrders from [tbl_PayslipTemplate] where [FK_CompanyRegistrationID]='"+objSubs.FK_CompanyRegistrationID+"' group by [CTCFrom]+'-'+[CTCTo] ";
            string constr = GlobalFunction.constr;
            List<object> chartData = new List<object>();
            chartData.Add(new object[]
                        {
                            "ShipCity", "TotalOrders"
                        });
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            chartData.Add(new object[]
                        {
                            sdr["ShipCity"], sdr["TotalOrders"]
                        });
                        }
                    }

                    con.Close();
                }
            }
          
            return Json(chartData);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult DashboardLeave(string filter)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<dashboardLeave> LeaveList = new List<dashboardLeave>();
            try
            {

                if (filter == "Today")
                {
                    DateTime filterString = DateTime.Today;
                    LeaveList = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.LeaveFrom <= filterString && x.LeaveTo >= filterString).Select(s => new dashboardLeave
                    {
                        ID = s.ID,
                        LeaveFrom = s.LeaveFrom,
                        LeaveTo = s.LeaveTo,
                        LeaveDays = s.LeaveDays,
                        AppliedFor = s.AppliedFor,
                        Reason = s.Reason,
                        IsApproved = s.IsApproved,
                        Employee_Code = s.Employee_Code,

                    }).ToList();
                }
                else if (filter == "This Week")
                {

                    sqlQuery = "SELECT tbl_Emp_Contact_Basic.Employee_Code, tbl_Emp_Leave.LeaveFrom, tbl_Emp_Leave.LeaveTo, tbl_Emp_Leave.AppliedFor, tbl_Emp_Leave.Reason, "
                     + " tbl_Emp_Leave.IsApproved"
                     + " FROM tbl_Emp_Leave INNER JOIN"
                     + " tbl_Emp_Contact_Basic ON tbl_Emp_Leave.Employee_Code = tbl_Emp_Contact_Basic.Employee_Code"
                     + " WHERE (DATEPART(ww, tbl_Emp_Leave.LeaveFrom) = DATEPART(ww, GETDATE()))  or (datepart(ww, tbl_emp_leave.leaveto) = datepart(ww, getdate()))AND "
                     + " tbl_Emp_Leave.FK_CompanyRegistrationID ='" + objSubs.FK_CompanyRegistrationID + "'";

                    LeaveList = context.Database.SqlQuery<dashboardLeave>(sqlQuery).ToList();

                }
                else if (filter == "This Month")
                {

                    sqlQuery = "SELECT tbl_Emp_Contact_Basic.Employee_Code, tbl_Emp_Leave.LeaveFrom, tbl_Emp_Leave.LeaveTo, tbl_Emp_Leave.AppliedFor, tbl_Emp_Leave.Reason, "
                   + " tbl_Emp_Leave.IsApproved"
                   + " FROM tbl_Emp_Leave INNER JOIN"
                   + " tbl_Emp_Contact_Basic ON tbl_Emp_Leave.Employee_Code = tbl_Emp_Contact_Basic.Employee_Code"
                   + " WHERE (DATEPART(mm, tbl_Emp_Leave.LeaveFrom) = DATEPART(mm, GETDATE())) AND "
                   + " tbl_Emp_Leave.FK_CompanyRegistrationID ='" + objSubs.FK_CompanyRegistrationID + "'";

                    LeaveList = context.Database.SqlQuery<dashboardLeave>(sqlQuery).ToList();
                }



            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(LeaveList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult DashboardAbsent()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<DashboardAbsent> AbsentList = new List<DashboardAbsent>();
            try
            {

                sqlQuery = "SELECT Employee_Code,Name,'Absent' as [Status]"
                + "from tbl_Emp_Contact_Basic "
                + " where (select InTime from tbl_Emp_Attendance where tbl_Emp_Attendance.Employee_Code=tbl_Emp_Contact_Basic.Employee_Code and tbl_Emp_Attendance.[date]='" + DateTime.Today + "') is null"
                + " and FK_CompanyRegistrationID='" + objSubs.FK_CompanyRegistrationID + "'";

                AbsentList = context.Database.SqlQuery<DashboardAbsent>(sqlQuery).ToList();


            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(AbsentList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult DashboardAnniversary()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<DashboardAnniversary> AnniversaryList = new List<DashboardAnniversary>();
            try
            {

                //sqlQuery = " SELECT 'Today is '+LOWER( [Name]+' Anniversary' ) as Notification FROM [tbl_Emp_Contact_Basic] "
                //        + " WHERE DATEPART(DAY,DOB)=DATEPART(DAY,GETDATE())"
                //        + " AND DATEPART(MONTH,DOB)=DATEPART(MONTH,GETDATE())"
                //        + " and FK_CompanyRegistrationID='" + objSubs.FK_CompanyRegistrationID + "'";

                //AnniversaryList = context.Database.SqlQuery<DashboardAnniversary>(sqlQuery).ToList();

                AnniversaryList = context.Database.SqlQuery<DashboardAnniversary>("usp_getAnniversary  @CompanyRegistrationID",
                    new SqlParameter("@CompanyRegistrationID", objSubs.FK_CompanyRegistrationID)).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(AnniversaryList);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.HR)]
        public PartialViewResult DashboardBirthday()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<DashboardAnniversary> BirthdayList = new List<DashboardAnniversary>();
            try
            {

                BirthdayList = context.Database.SqlQuery<DashboardAnniversary>("usp_getBirthday  @CompanyRegistrationID",
                    new SqlParameter("@CompanyRegistrationID", objSubs.FK_CompanyRegistrationID)).ToList();


            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(BirthdayList);
        }


    }
}