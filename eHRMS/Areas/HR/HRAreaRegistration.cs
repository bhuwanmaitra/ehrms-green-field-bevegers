﻿using System.Web.Mvc;

namespace eHRMS.Areas.HR
{
    public class HRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HR_default",
                "HR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "LocationEdit",
                "HR/{controller}/{action}/{id}",
                new { Controller = "HRCompany", action = "Company_Location_Create", id = UrlParameter.Optional }
            );
        }
    }
}