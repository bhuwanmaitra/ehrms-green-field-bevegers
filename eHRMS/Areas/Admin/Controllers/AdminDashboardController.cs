﻿using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;

namespace eHRMS.Areas.Admin.Controllers
{
    public class AdminDashboardController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult Dashboard()
        {

            return View();
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public PartialViewResult Subscription(SubscriptionModel model)
        {
            try
            {
                model = objglobal.GetSubscriptionDetails();


                int totalAdmin = 0;
                int totalHR = 0;
                int totalEmployee = 0;

                if (model != null)
                {
                    totalAdmin = context.tbl_Login.Count(m => m.FK_CompanyRegistrationID == model.FK_CompanyRegistrationID && m.UserRole == (int)AppsConstants.UserRole.Admin);
                    totalHR = context.tbl_Login.Count(m => m.FK_CompanyRegistrationID == model.FK_CompanyRegistrationID && m.UserRole == (int)AppsConstants.UserRole.HR);
                    totalEmployee = context.tbl_Login.Count(m => m.FK_CompanyRegistrationID == model.FK_CompanyRegistrationID && m.UserRole == (int)AppsConstants.UserRole.Employee);
                }
                ViewBag.TotalAdmin = totalAdmin;
                ViewBag.TotalHR = totalHR;
                ViewBag.TotalEmployee = totalEmployee;
                TimeSpan datediff = Convert.ToDateTime(model.ExpiredDate).Subtract(  Convert.ToDateTime(model.ActivatedOn));
                ViewBag.TotalDays = datediff.Days;
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Sorry! Please try again later !";
                return PartialView(model);
            }
            return PartialView(model);
        }
    }
}