﻿using eHRMS.Models;
using eHRMS.Areas.Admin.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;

namespace eHRMS.Areas.Admin.Controllers
{
    public class AdminConfigarationController : Controller
    {
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult ITSlab()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            if (objSubs.ExpiredDate > System.DateTime.Now)
            {                
                return View();
            }
            else
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public PartialViewResult ViewITSlab()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SlabMaster> ReviewList = new List<SlabMaster>();
            try
            {
                ReviewList = context.tbl_SlabMaster.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new SlabMaster
                {
                    SlabMasterID = s.SlabMasterID,
                    IncomeFrom = s.IncomeFrom,
                    IncomeTo = s.IncomeTo,
                    TAX = s.TAX,
                    Category = s.Category,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(ReviewList);
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult ITSlab(SlabMaster model)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                if (ModelState.IsValid)
                {
                    if (!context.tbl_SlabMaster.Any(x => x.IsActive == true && x.Category == model.Category && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        SaveDetails(model);
                        ViewBag.SuccessMsg = "Data saved successfully !";
                        ModelState.Clear();
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "Already exist in this category !";
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Saving details
        private void SaveDetails(SlabMaster model)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

                tbl_SlabMaster objSlab = new tbl_SlabMaster()
                {
                    IncomeFrom=model.IncomeFrom,
                    IncomeTo=model.IncomeTo,
                    TAX=model.TAX,
                    Category=model.Category,                    
                    FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                    IsActive=true,
                    CreatedOn = System.DateTime.Now,
                    CreatedBy = objSubs.Username
                };
                context.tbl_SlabMaster.Add(objSlab);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }
        #endregion       
        
        #region Delete DeleteSlabMaster
        public JsonResult DeleteSlabMaster(string SlabMasterID)
        {
            SlabMasterID = HttpUtility.UrlDecode(SlabMasterID);
            if (SlabMasterID != null)
            {
                int id = Convert.ToInt32(SlabMasterID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_SlabMaster objSlabMaster = context.tbl_SlabMaster.First(d => d.SlabMasterID == id);
                
                try
                {
                    // Update tbl_SlabMaster
                    objSlabMaster.IsActive = false;
                    objSlabMaster.UpdatedBy = objSubs.Username;
                    objSlabMaster.UpdatedOn = System.DateTime.Now;
                    context.Entry(objSlabMaster).State = EntityState.Modified;
                    context.SaveChanges();                    

                    ViewBag.SuccessMsg = "Data deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public PartialViewResult EmailConfigView()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<EmailSetting> Email_List = new List<EmailSetting>();
            try
            {
                Email_List = context.tbl_EmailSettings.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new EmailSetting
                {
                    SenderID = s.SenderID,
                    SMTPServer = s.SMTPServer,
                    SMTPPort = s.SMTPPort,
                    FromEmail = s.FromEmail,
                    UserID = s.UserID,
                    Password = s.Password,
                    FK_CompanyRegistrationID = s.FK_CompanyRegistrationID,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Email_List);
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult EmailConfig()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            if (objSubs.ExpiredDate > System.DateTime.Now)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult EmailConfig(EmailSetting model)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                if (ModelState.IsValid)
                {
                    if (!context.tbl_EmailSettings.Any(x =>x.IsActive==true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID))
                    {
                        SaveSettingDetails(model);
                        ViewBag.SuccessMsg = "Data saved successfully !";
                        ModelState.Clear();
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMsg = "Another setting already exist. Please delete it first!";
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank !";
                    return View();
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View();
            }
        }

        #region Saving details
        private void SaveSettingDetails(EmailSetting model)
        {
            try
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_EmailSettings objEmailStng = new tbl_EmailSettings()
                {
                    SMTPServer=model.SMTPServer,
                    SMTPPort=model.SMTPPort,
                    FromEmail=model.FromEmail,
                    UserID=model.UserID,
                    Password=model.Password,
                    IsActive=true,
                    FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                    CreatedOn = System.DateTime.Now,
                    CreatedBy = objSubs.Username
                };
                context.tbl_EmailSettings.Add(objEmailStng);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
        }
        #endregion

        #region Delete DeleteEmailSetting
        public JsonResult DeleteEmailSetting(string SenderID)
        {
            SenderID = HttpUtility.UrlDecode(SenderID);
            if (SenderID != null)
            {
                int id = Convert.ToInt32(SenderID);
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_EmailSettings objemailstng = context.tbl_EmailSettings.First(d => d.SenderID == id);

                try
                {
                    // Update tbl_EmailSettings
                    objemailstng.IsActive = false;
                    objemailstng.UpdatedBy = objSubs.Username;
                    objemailstng.UpdatedOn = System.DateTime.Now;
                    context.Entry(objemailstng).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "Data deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        
    }
}