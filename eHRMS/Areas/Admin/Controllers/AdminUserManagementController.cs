﻿using eHRMS.Areas.Admin.Models;
using eHRMS.Models;
using eHRMS.Repository;
using eHRMS.Global;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;

namespace eHRMS.Areas.Admin.Controllers
{
    public class AdminUserManagementController : Controller
    {
        
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult CreateUser()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            if (objSubs.NoOfUser == objSubs.UserLimit || objSubs.ExpiredDate < System.DateTime.Now)
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
            else if (objSubs.NoOfUser == objSubs.UserLimit || objSubs.ExpiredDate < System.DateTime.Now)
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
            else
            {
                ViewData["Employee_List"] = EmployeeCodeList();
                return View();
            }
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult CreateUser(CreateUser fc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!context.tbl_LoginContact.Any(x => x.IsActive == true && x.Email.Trim() == fc.Email.Trim()))
                    {
                        if (SaveDetails(fc))
                        {
                            ViewBag.SuccessMsg = "Invitation sent successfully with Login Credentials !";
                            ModelState.Clear();
                            ViewData["Employee_List"] = EmployeeCodeList();
                            return View();
                        }
                        else
                        {
                            ViewBag.ErrorMsg = "Inetrnal error !";
                            ViewData["Employee_List"] = EmployeeCodeList();
                            return View(fc);

                        }
                    }

                    ViewBag.ErrorMsg = "This email already registered with us !";
                    ViewData["Employee_List"] = EmployeeCodeList();
                    return View(fc);

                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["Employee_List"] = EmployeeCodeList();
                    return View(fc);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Employee_List"] = EmployeeCodeList();
                return View(fc);
            }

        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult ViewUsers()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            if (objSubs.ExpiredDate > System.DateTime.Now)
            {
                List<CreateUser> ReviewList = new List<CreateUser>();
                try
                {
                    ReviewList = context.tbl_UserDetails.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new CreateUser
                    {
                        FK_LoginID = s.FK_LoginID,
                        UserType = s.UserType,
                        Firstname = s.Firstname,
                        Lastname = s.Lastname,
                        UserID = context.tbl_Login.Where(x => x.IsActive == true && x.LoginID == s.FK_LoginID).Select(i => i.Username).FirstOrDefault(),
                        Number = context.tbl_LoginContact.Where(x => x.IsActive == true && x.FK_LoginID == s.FK_LoginID).Select(i => i.Number).FirstOrDefault().ToString(),
                    }).ToList();

                    //if (ReviewList.Count == 0)
                    //{
                    //    ViewBag.ErrorMsg = "No matching records found !";
                    //}
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
                return View(ReviewList);
            }
            else
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult EditUsers(string id)
        {
            id = HttpUtility.UrlDecode(id);
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            if (objSubs.ExpiredDate > System.DateTime.Now)
            {
                //ViewData["Employee_List"] = EmployeeCodeList();
                return View(GetUserDetails(id));
            }
            else
            {
                return RedirectToAction("Dashboard", "AdminDashboard");
            }
        }

        #region Get User Details
        private CreateUser GetUserDetails(string id)
        {
            tbl_UserDetails objUserdetails = context.tbl_UserDetails.Where(x => x.IsActive == true && x.FK_LoginID == id).FirstOrDefault();
            tbl_LoginContact objUsercontact = context.tbl_LoginContact.Where(x => x.IsActive == true && x.FK_LoginID == id).FirstOrDefault();
            tbl_Login objlogindetails = context.tbl_Login.Where(x => x.IsActive == true && x.LoginID == id).FirstOrDefault();

            CreateUser objdetails = new CreateUser();
            if (objUserdetails != null && objUsercontact != null && objlogindetails != null)
            {
                objdetails = new CreateUser()
                {
                    UserType = objUserdetails.UserType,
                    Employee_Code = objUserdetails.Employee_Code,
                    UserID = objlogindetails.Username,
                    Password = objglobal.Decrypt(objlogindetails.Password),
                    Firstname = objUserdetails.Firstname,
                    Lastname = objUserdetails.Lastname,
                    Email = objUsercontact.Email,
                    Number = objUsercontact.Number.ToString(),
                    Address = objUserdetails.Address
                };
                return objdetails;
            }
            else
            {
                ViewBag.ErrorMsg = "Something went wrong or User is not active !";
                return objdetails;
            }
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Admin)]
        public ActionResult EditUsers(CreateUser fc, string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    id = HttpUtility.UrlDecode(id);
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_UserDetails objUserdetails = context.tbl_UserDetails.Where(x => x.IsActive == true && x.FK_LoginID == id).FirstOrDefault();
                    tbl_LoginContact objUsercontact = context.tbl_LoginContact.Where(x => x.IsActive == true && x.FK_LoginID == id).FirstOrDefault();
                    tbl_Login objlogindetails = context.tbl_Login.Where(x => x.IsActive == true && x.LoginID == id).FirstOrDefault();
                    // Update Login Table
                    objlogindetails.Username = fc.UserID;
                    objlogindetails.Password = objglobal.Encrypt(fc.Password);
                    //objlogindetails.UserRole = Convert.ToInt32(fc.UserType);
                    objlogindetails.UpdatedOn = System.DateTime.Now;
                    objlogindetails.UpdatedBy = objSubs.Username;
                    context.Entry(objlogindetails).State = EntityState.Modified;
                    context.SaveChanges();
                    //Update LoginContact table
                    objUsercontact.ContactPerson = fc.Firstname + " " + fc.Lastname;
                    objUsercontact.Email = fc.Email;
                    objUsercontact.Number = Convert.ToDecimal(fc.Number);
                    objUsercontact.UpdatedOn = System.DateTime.Now;
                    objUsercontact.UpdatedBy = objSubs.Username;
                    context.Entry(objUsercontact).State = EntityState.Modified;
                    context.SaveChanges();
                    // Update UserDetails table
                    //objUserdetails.UserType = fc.UserType;
                    objUserdetails.Firstname = fc.Firstname;
                    objUserdetails.Lastname = fc.Lastname;
                    objUserdetails.Address = fc.Address;
                    objUserdetails.UpdatedOn = System.DateTime.Now;
                    objUserdetails.UpdatedBy = objSubs.Username;
                    context.Entry(objUserdetails).State = EntityState.Modified;
                    context.SaveChanges();
                    ViewBag.SuccessMsg = "Profile updated successfully !";
                    //ViewData["Employee_List"] = EmployeeCodeList();
                    return View(fc);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    //ViewData["Employee_List"] = EmployeeCodeList();
                    return View(fc);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                //ViewData["Employee_List"] = EmployeeCodeList();
                return View(fc);
            }
        }


        #region Saving Registration details and send Mail
        private bool SaveDetails(CreateUser fc)
        {
            using (DbContextTransaction dbTran = context.Database.BeginTransaction())
            {
                try
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();

                    
                    tbl_Login objLogin = new tbl_Login()
                    {

                        LoginID = System.Guid.NewGuid().ToString().ToUpper(),
                        Username = fc.Email,
                        Password = objglobal.Encrypt(fc.Password),
                        UserRole = Convert.ToInt32(fc.UserType),
                        IsActive = true,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_Login.Add(objLogin);
                    context.SaveChanges();

                    string LoginID = objLogin.LoginID;

                    tbl_LoginContact objLoginContact = new tbl_LoginContact()
                    {
                        LoginContactID = System.Guid.NewGuid().ToString().ToUpper(),
                        ContactPerson = fc.Firstname + " " + fc.Lastname,
                        Email = fc.Email,
                        Number = Convert.ToDecimal(fc.Number),
                        IsActive = true,
                        FK_LoginID = LoginID,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_LoginContact.Add(objLoginContact);
                    context.SaveChanges();

                    tbl_UserDetails objdtls = new tbl_UserDetails()
                    {
                        UserType = fc.UserType,
                        Employee_Code = fc.Employee_Code,
                        Firstname = fc.Firstname,
                        Lastname = fc.Lastname,
                        Address = fc.Address,
                        FK_LoginID = LoginID,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        CreatedOn = System.DateTime.Now,
                        CreatedBy = objSubs.Username
                    };
                    context.tbl_UserDetails.Add(objdtls);
                    context.SaveChanges();

                    // Update tbl_Subscription
                    tbl_Subscription objSubDetails = context.tbl_Subscription.First(d => d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID);
                    objSubDetails.NoOfUser = objSubDetails.NoOfUser + 1;
                    objSubDetails.UpdatedOn = System.DateTime.Now;
                    objSubDetails.UpdatedBy = objSubs.Username;
                    context.Entry(objSubDetails).State = EntityState.Modified;
                    context.SaveChanges();

                    dbTran.Commit();
                    return true;
                    // Send Email for Login Details
                    // objglobal.SendMailRegistration("eHRMS-Login Invitation", objLoginContact.ContactPerson, fc.Email, fc.Email, fc.Password, objSubs.FK_CompanyRegistrationID);
                }
                catch (Exception ex)
                {
                    dbTran.Rollback();
                    objError.LogError(ex);
                    return false;
                }
            }
        }
        public JsonResult getEmployeeDetial(string EmpID)
        {
            tbl_Emp_Contact_Basic user = new tbl_Emp_Contact_Basic();
            CreateUser createUser = new CreateUser();
            if (EmpID != null)
            {
               user=context.tbl_Emp_Contact_Basic.FirstOrDefault(m => m.Employee_Code == EmpID);
               if (user != null)
               {
                   createUser.UserID = user.Email;
                   createUser.Email = user.Email;
                   createUser.Number = user.Number.ToString();
                   string[] split = user.Name.Split(' ');
                   createUser.Firstname =split[0].ToString();
                   createUser.Lastname = split[1].ToString();

               }
            }
            return Json(createUser, JsonRequestBehavior.AllowGet);


        }
        #endregion


        #region Delete User
        public JsonResult DeleteUsers(string FK_LoginID)
        {
            FK_LoginID = HttpUtility.UrlDecode(FK_LoginID);
            if (FK_LoginID != null)
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_UserDetails objUserdetails = context.tbl_UserDetails.First(d => d.FK_LoginID == FK_LoginID);
                tbl_LoginContact objUsercontact = context.tbl_LoginContact.First(d => d.FK_LoginID == FK_LoginID);
                tbl_Login objlogindetails = context.tbl_Login.First(d => d.LoginID == FK_LoginID);
                try
                {
                    // Update Login Table
                    objlogindetails.IsActive = false;
                    objlogindetails.UpdatedBy = objSubs.Username;
                    objlogindetails.UpdatedOn = System.DateTime.Now;
                    context.Entry(objlogindetails).State = EntityState.Modified;
                    context.SaveChanges();
                    //Update LoginContact table
                    objUsercontact.IsActive = false;
                    objUsercontact.UpdatedBy = objSubs.Username;
                    objUsercontact.UpdatedOn = System.DateTime.Now;
                    context.Entry(objUsercontact).State = EntityState.Modified;
                    context.SaveChanges();
                    // Update UserDetails table
                    objUserdetails.IsActive = false;
                    objUserdetails.UpdatedBy = objSubs.Username;
                    objUserdetails.UpdatedOn = System.DateTime.Now;
                    context.Entry(objUserdetails).State = EntityState.Modified;
                    context.SaveChanges();

                    ViewBag.SuccessMsg = "User deleted successfully !";

                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        // Employee Code List which are not in tbl_UserDetails
        public List<SelectListItem> EmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Salary.Where(c => !context.tbl_UserDetails.Where(x => x.IsActive == true).Select(b => b.Employee_Code).Contains(c.Employee_Code) && c.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && c.IsActive == true).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Employee_Code, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }
    }
}