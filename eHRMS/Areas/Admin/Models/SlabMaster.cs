﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Admin.Models
{
    public class SlabMaster
    {
        public int SlabMasterID { get; set; }
        [Required]
        public string IncomeFrom { get; set; }
        [Required]
        public string IncomeTo { get; set; }
        [Required]
        public string TAX { get; set; }
        [Required]
        public string Category { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}