﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Admin.Models
{
    public class EmailSetting
    {
        public int SenderID { get; set; }
        [Required]
        public string SMTPServer { get; set; }
        [Required]
        public Nullable<decimal> SMTPPort { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")]
        public string FromEmail { get; set; }
        [Required]
        public string UserID { get; set; }
        [Required]
        public string Password { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
    }
}