﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class Emp_Sociallinks
    {
        public int SocialID { get; set; }
        public string Facebooklink { get; set; }
        public string Twitterlink { get; set; }
        public string Linkedinlink { get; set; }
        public string Googlelink { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}