﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class Emp_Attendance
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string WorkingHours { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public string Employee_Code { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}