﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class AttendanceRequest
    {
        public int RequestID { get; set; }
        [Required]
        public Nullable<System.DateTime> Date { get; set; }
        [Required]
        public string InTime { get; set; }
        //[Required]
        public string OutTime { get; set; }

        
        public string Remarks { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public string Employee_Code { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        [Required]
        public string Status { get; set; }
    }
}