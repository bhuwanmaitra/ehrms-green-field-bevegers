﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class Emp_TaskModel
    {
        [Required]
        public string TaskName { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }

    }
    public class Emp_TaskAssignModel
    {
        public long AssignTaskId { get; set; }
        [Required]
        public string TaskName { get; set; }
        [Required]
        public string Employee_Code { get; set; }
        public string Employee_Name{ get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string AssignBy { get; set; }
        public Nullable<System.DateTime> AssignOn { get; set; }
        public string TotalScore { get; set; }

    }
    public class Emp_TaskProgressModel
    {
        [Required]
        public long AssignTaskId { get; set; }
        [Required]
        public string Comment{ get; set; }
        [Required]
        public string ProgerssScore{ get; set; }
        public string TotalScore { get; set; }

        public Nullable<System.DateTime> CreatedOn { get; set; }

    }
}