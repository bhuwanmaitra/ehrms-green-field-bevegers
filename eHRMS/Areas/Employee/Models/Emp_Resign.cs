﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class Emp_Resign
    {
        public int ResignID { get; set; }
        [Required]
        public string Reason { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> Number { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public Nullable<int> NoticePeriod { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsAccepted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> ResignDate { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}