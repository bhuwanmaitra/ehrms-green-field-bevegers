﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class LeaveRequest
    {
        public int ID { get; set; }
        [Required]
        public Nullable<System.DateTime> LeaveFrom { get; set; }
        [Required]
        public Nullable<System.DateTime> LeaveTo { get; set; }
        [Required]
        public Nullable<int> LeaveDays { get; set; }
        [Required]
        public string AppliedFor { get; set; }
        [Required]
        public string Reason { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}