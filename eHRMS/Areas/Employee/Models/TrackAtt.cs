﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Areas.Employee.Models
{
    public class Emp_TrackAtt
    {
        public Nullable<System.DateTime> Date { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string WorkingHours { get; set; }
        public string FK_CompanyRegistrationID { get; set; }

        
        public string Employee_Code { get; set; }
        [Required]
        public string Month { get; set; }
        [Required]
        public string Year { get; set; }
    }
}