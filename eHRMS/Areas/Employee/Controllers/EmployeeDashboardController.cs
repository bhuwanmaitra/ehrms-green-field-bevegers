﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eHRMS.Global;
using eHRMS.Repository;
using eHRMS.Models;
using eHRMS.Areas.Employee.Models;
using System.Data.Entity;
using eHRMS.Areas.HR.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace eHRMS.Areas.Employee.Controllers
{

    public class EmployeeDashboardController : Controller
    {

        
        eHRMSEntities context = new eHRMSEntities();
        GlobalFunction objglobal = new GlobalFunction();
        ErrorHandlerClass objError = new ErrorHandlerClass();
        HR.Controllers.HRPayrollController objHRPayrollController = new HR.Controllers.HRPayrollController();
        // GET: Employee/EmployeeDashboard
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult Dashboard()
        {
            var getDetail = objglobal.GetEmployeeDetails();
            if (getDetail != null)
            {
                if (getDetail.IsAppraiser == true)
                {
                    ViewBag.GenerateTask = true;
                }
                else
                {
                    ViewBag.GenerateTask = false;
                }
            }

            if (ViewBag.GenerateTask == false)
            {

                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                
                List<Emp_TaskAssignModel> TaskList = new List<Emp_TaskAssignModel>();
                try
                {
                    TaskList = context.tbl_Emp_AssignTask.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == getDetail.Employee_Code).Select(s => new Emp_TaskAssignModel
                    {
                        AssignTaskId = s.AssignTaskId,
                        TaskName = s.TaskName,
                        AssignBy = context.tbl_Emp_Contact_Basic.FirstOrDefault(m => m.Employee_Code == s.AssignBy && m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Name,
                        CreatedBy = s.CreatedBy,
                        CreatedOn = s.CreatedOn,
                        TotalScore = context.tbl_Emp_Task_Progress.Where(o => o.FK_AssignTaskId == s.AssignTaskId).Sum(i => i.ProgressScore).Value.ToString()

                    }).ToList();
                    ViewBag.TaskList = TaskList;
                }

                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }

            return View();
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult GenerateTask()
        {
            getTaskList();

            return View();
        }

        private void getTaskList()
        {

            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_TaskModel> TaskList = new List<Emp_TaskModel>();
            try
            {
                TaskList = context.tbl_Emp_NewTask.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new Emp_TaskModel
                {
                    TaskName = s.Name,


                }).ToList();
                ViewBag.TaskList = TaskList;
            }

            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
        }
        private void getAssignTaskList()
        {

            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_TaskAssignModel> objtbl = new List<Emp_TaskAssignModel>();
            try
            {

                objtbl = (from s in context.tbl_Emp_AssignTask
                          join sa in context.tbl_Emp_Contact_Basic on s.Employee_Code equals sa.Employee_Code
                          where sa.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID &&
                          s.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID &&
                          s.AssignBy == objEmp.Employee_Code &&
                          s.IsActive == true
                          select new Emp_TaskAssignModel
                          {
                              AssignBy = context.tbl_Emp_Contact_Basic.FirstOrDefault(m => m.Employee_Code == s.AssignBy).Name,
                              CreatedOn = s.CreatedOn,
                              TaskName = s.TaskName,
                              Employee_Name = sa.Name
                          }).ToList();


                ViewBag.AssignTaskList = objtbl;
            }

            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        [HttpPost]
        public ActionResult GenerateTask(Emp_TaskModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    Emp_Details objEmp = objglobal.GetEmployeeDetails();
                    tbl_Emp_NewTask objtbl = new tbl_Emp_NewTask()
                    {
                        Employee_Code = objEmp.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        Name = model.TaskName,
                        CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                        CreatedBy = objSubs.Username,

                    };
                    context.tbl_Emp_NewTask.Add(objtbl);
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    if (ex.InnerException.InnerException.Message.Contains("UK_tbl_Emp_NewTask"))
                    {
                        ViewBag.ErrorMsg = "Task name allready taken.";
                    }
                }
            }
            getTaskList();
            return View(model);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult AssignTask()
        {
            ViewData["Code_List_Employee"] = getEmployeeCodeList();
            getTaskList();
            getAssignTaskList();
            return View();
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        [HttpPost]
        public ActionResult AssignTask(Emp_TaskAssignModel model)
        {

            if (string.IsNullOrEmpty(Request["FocusList"]))
            {
                ViewBag.ErrorMsg = "Something went wrong !";
                getTaskList();
                ViewData["Code_List_Employee"] = getEmployeeCodeList();
                getAssignTaskList();

                return View(model);
            }
            var chkboxValues = Request["FocusList"].Split(',').ToList();

            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            foreach (var item in chkboxValues)
            {
                tbl_Emp_AssignTask objtbl = new tbl_Emp_AssignTask()
                    {
                        Employee_Code = model.Employee_Code,
                        FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                        IsActive = true,
                        TaskName = item,
                        AssignBy = objEmp.Employee_Code,
                        CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                        CreatedBy = objSubs.Username,

                    };

                try
                {
                    context.tbl_Emp_AssignTask.Add(objtbl);
                    context.SaveChanges();
                    ViewBag.SuccessMsg = "Task assign succesfully. ";
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("UC_Person"))
                    {
                        ViewBag.ErrorMsg = "This data already exist !";
                    }
                }

            }
            getTaskList();
            ViewData["Code_List_Employee"] = getEmployeeCodeList();
            getAssignTaskList();
            return View(model);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult UpdateTaskProgress(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                id = "0";
            }
            Emp_TaskProgressModel objModel = new Emp_TaskProgressModel()
            {

                AssignTaskId = long.Parse(id),
            };
            if (context.tbl_Emp_Task_Progress.Where(o => o.FK_AssignTaskId == objModel.AssignTaskId).Sum(i => i.ProgressScore).HasValue)
            {
                objModel.TotalScore = (context.tbl_Emp_Task_Progress.Where(o => o.FK_AssignTaskId == objModel.AssignTaskId).Sum(i => i.ProgressScore).Value.ToString());
            }
            else
            {
                objModel.TotalScore = "0";
            }

            ViewBag.TaskList = getProgressDetail(objModel.AssignTaskId);

            return View(objModel);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        [HttpPost]
        public ActionResult UpdateTaskProgress(Emp_TaskProgressModel model)
        {
            if (ModelState.IsValid)
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                Emp_Details objEmp = objglobal.GetEmployeeDetails();
                tbl_Emp_Task_Progress objtbl = new tbl_Emp_Task_Progress()
                {
                    Comment = model.Comment,
                    ProgressScore = Convert.ToDecimal(model.ProgerssScore),
                    FK_AssignTaskId = model.AssignTaskId,
                    CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                    CreatedBy = objSubs.Username,

                };
                context.tbl_Emp_Task_Progress.Add(objtbl);
                context.SaveChanges();
                ViewBag.SuccessMsg = "Progress submitted succesfully. ";
                model.Comment = null; model.ProgerssScore = null;
            }
            else
            {
                //  model.AssignTaskId = 0;
                ViewBag.ErrorMsg = "All field are required !";
            }
            model.TotalScore = context.tbl_Emp_Task_Progress.Where(o => o.FK_AssignTaskId == model.AssignTaskId).Sum(i => i.ProgressScore).Value.ToString();
            ViewBag.TaskList = getProgressDetail(model.AssignTaskId);

            return View(model);
        }
        public List<Emp_TaskProgressModel> getProgressDetail(long id)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_TaskProgressModel> TaskList = new List<Emp_TaskProgressModel>();
            try
            {
                TaskList = context.tbl_Emp_Task_Progress.Where(x => x.FK_AssignTaskId == id).Select(s => new Emp_TaskProgressModel
                {
                    Comment = s.Comment,
                    ProgerssScore = s.ProgressScore.ToString(),
                    //TotalScore=context.tbl_Emp_Task_Progress.GroupBy(o => o.FK_AssignTaskId==id).Select(g => new { TotalScore  = g.Sum(i => i.ProgressScore) }).ToString()

                    //                 var result = db.pruchasemasters.GroupBy(o => o.membername)
                    //               .Select(g => new { membername = g.Key, total = g.Sum(i => i.cost) });

                    //foreach (var group in result)
                    //{
                    //    Console.WriteLine("Membername = {0} Totalcost={1}", group.membername, group.total);
                    //}

                }).ToList();

            }

            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return TaskList;
        }
        public List<SelectListItem> getEmployeeCodeList()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Emp_Contact_Basic.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Name, Value = item.Employee_Code.ToString() });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        public PartialViewResult Emp_Details(Emp_Details objEmp)
        {
            objEmp = objglobal.GetEmployeeDetails();
            return PartialView(objEmp);
        }

        public PartialViewResult Upload_Picture()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Upload_Picture(HttpPostedFileBase file)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            if (file != null)
            {
                // Upload Image
                bool flag = false;
                Stream stream = file.InputStream;
                string myfile = string.Empty;
                var allowedExtensions = new[] { ".JPG", ".PNG", ".GIFS", ".JPEG" };
                var ext = Path.GetExtension(file.FileName).ToUpper();
                if (allowedExtensions.Contains(ext))
                {
                    Bitmap sourceImage = new Bitmap(stream);
                    myfile = "Banner-" + objEmp.Employee_Code + ext;
                    //resize the image
                    int smallImageHeight = 245;
                    int smallImageWidth = 245;
                    System.Drawing.Image resizedImage = new System.Drawing.Bitmap(smallImageWidth, smallImageHeight, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
                    Graphics gr = Graphics.FromImage(resizedImage);
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    gr.DrawImage(sourceImage, 0, 0, smallImageWidth, smallImageHeight);
                    var path = Path.Combine(Server.MapPath("~/Content/hrms_images/UserImages"), myfile);
                    resizedImage.Save(path);
                    flag = true;
                }
                else
                {
                    TempData["ErrorMsg"] = "Please choose only Image file";
                }
                // End Image Upload
                if (flag)
                {
                    tbl_UserDetails objUser = context.tbl_UserDetails.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objEmp.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).FirstOrDefault();
                    if (objUser != null)
                    {
                        objUser.UserImagePath = myfile;
                        objUser.UpdatedOn = objglobal.TimeZone(objEmp.TimeZone);
                        objUser.UpdatedBy = objSubs.Username;
                        context.Entry(objUser).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.SuccessMsg = "Image Uploaded successfully !";
                    }
                    //ModelState.Clear();
                }
            }
            else
            {
                TempData["ErrorMsg"] = "Required fields cannot be blank !";
            }
            return RedirectToAction("Dashboard");
        }


        public PartialViewResult Emp_Social()
        {
            return PartialView(GetLinksDetails());
        }

        #region Get LinksDetails
        private Emp_Sociallinks GetLinksDetails()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            tbl_EmpSocialLink objdetails = context.tbl_EmpSocialLink.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).FirstOrDefault();

            Emp_Sociallinks objlinklist = new Emp_Sociallinks();
            if (objdetails != null)
            {
                objlinklist = new Emp_Sociallinks()
                {
                    Facebooklink = objdetails.Facebooklink,
                    Twitterlink = objdetails.Twitterlink,
                    Linkedinlink = objdetails.Linkedinlink,
                    Googlelink = objdetails.Googlelink
                };
                return objlinklist;
            }
            else
            {
                //ViewBag.ErrorMsg = "Something went wrong !";
                return objlinklist;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult Emp_Social(Emp_Sociallinks model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Emp_Details objEmp = objglobal.GetEmployeeDetails();
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    if (context.tbl_EmpSocialLink.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code))
                    {
                        tbl_EmpSocialLink objstng = context.tbl_EmpSocialLink.First(d => d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Employee_Code == objEmp.Employee_Code);
                        objstng.Facebooklink = model.Facebooklink;
                        objstng.Twitterlink = model.Twitterlink;
                        objstng.Linkedinlink = model.Linkedinlink;
                        objstng.Googlelink = model.Googlelink;
                        objstng.UpdatedOn = objglobal.TimeZone(objEmp.TimeZone);
                        objstng.UpdatedBy = objSubs.Username;
                        context.Entry(objstng).State = EntityState.Modified;
                        context.SaveChanges();
                        TempData["SuccessMsg"] = "Social profiles link updated successfully !";
                    }
                    else
                    {
                        tbl_EmpSocialLink objstng = new tbl_EmpSocialLink()
                        {
                            Facebooklink = model.Facebooklink,
                            Twitterlink = model.Twitterlink,
                            Linkedinlink = model.Linkedinlink,
                            Googlelink = model.Googlelink,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            Employee_Code = objEmp.Employee_Code,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_EmpSocialLink.Add(objstng);
                        context.SaveChanges();
                        TempData["SuccessMsg"] = "Social profiles link added successfully !";
                    }
                    return RedirectToAction("Dashboard");
                }
                else
                {
                    TempData["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Dashboard");
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return RedirectToAction("Dashboard");
            }
        }



        public PartialViewResult QuickLinks(Emp_Attendance SubsEmp)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            try
            {
                var today = objglobal.TimeZone(objEmp.TimeZone).Date;
                SubsEmp = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.Date == today).Select(s => new Emp_Attendance
                {
                    Date = s.Date,
                    InTime = s.InTime,
                    OutTime = s.OutTime,
                    WorkingHours = s.WorkingHours,
                    Employee_Code = s.Employee_Code,
                    FK_CompanyRegistrationID = s.FK_CompanyRegistrationID,
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return PartialView(SubsEmp);
        }

        #region Attendance
        public JsonResult InTime(string InTime)
        {
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            if (objEmp != null)
            {
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                try
                {
                    var today = objglobal.TimeZone(objEmp.TimeZone).Date;
                    if (context.tbl_Emp_Attendance.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.Date == today && x.InTime != null))
                    {

                    }
                    else
                    {
                        tbl_Emp_Attendance objloc = new tbl_Emp_Attendance()
                        {
                            //Date = System.DateTime.Now.Date,
                            Date = objglobal.TimeZone(objEmp.TimeZone).Date,
                            //InTime = InTime,
                            InTime = String.Format("{0:t}", objglobal.TimeZone(objEmp.TimeZone)),
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            Employee_Code = objEmp.Employee_Code,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Attendance.Add(objloc);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }


        public JsonResult OutTime(string OutTime)
        {
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            if (objEmp != null)
            {
                var today = objglobal.TimeZone(objEmp.TimeZone).Date;
                SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                tbl_Emp_Attendance objDetails = context.tbl_Emp_Attendance.First(d => d.Employee_Code == objEmp.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Date == today);
                try
                {
                    var duration = Convert.ToDateTime(String.Format("{0:t}", objglobal.TimeZone(objEmp.TimeZone))) - Convert.ToDateTime(objDetails.InTime);
                    if (context.tbl_Emp_Attendance.Any(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.Date == today && x.OutTime != null))
                    {

                    }
                    else
                    {
                        // Update Table
                        objDetails.OutTime = String.Format("{0:t}", objglobal.TimeZone(objEmp.TimeZone));
                        objDetails.WorkingHours = duration.ToString();
                        objDetails.UpdatedBy = objSubs.Username;
                        objDetails.UpdatedOn = objglobal.TimeZone(objEmp.TimeZone);
                        context.Entry(objDetails).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
            }
            else
            {
                ViewBag.ErrorMsg = "Something is wrong!";
            }
            return Json("Success");
        }

        #endregion


        public PartialViewResult AttendanceRequestStatus()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<AttendanceRequest> EmpARList = new List<AttendanceRequest>();
            try
            {
                EmpARList = context.tbl_Emp_AttRequest.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).Select(s => new AttendanceRequest
                {
                    RequestID = s.RequestID,
                    Date = s.Date,
                    InTime = s.InTime,
                    OutTime = s.OutTime,
                    Remarks = s.Remarks,
                    IsApproved = s.IsApproved,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(EmpARList);
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult AttendanceRequest()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AttendanceRequest(AttendanceRequest model)
        {
            try
            {
                Emp_Details objEmp = objglobal.GetEmployeeDetails();
                if (ModelState.IsValid)
                {
                    DateTime date = objglobal.TimeZone(objEmp.TimeZone).Date;
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    tbl_Emp_Attendance objDetails = context.tbl_Emp_Attendance.Where(d => d.Employee_Code == objEmp.Employee_Code && d.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && d.Date == date).FirstOrDefault();

                    if (context.tbl_Emp_AttRequest.Any(x => (x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.IsApproved == null && x.Date == model.Date) || (x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.IsApproved == true && x.Date == model.Date)))
                    {
                        ViewBag.ErrorMsg = "Attendance request already sent on this date !";
                    }
                    else if (objDetails == null)
                    {
                        ViewBag.ErrorMsg = "You were not present on this date !";
                    }
                    else
                    {
                        tbl_Emp_AttRequest objrsgn = new tbl_Emp_AttRequest()
                        {
                            Date = model.Date,
                            InTime = model.InTime,
                            OutTime = model.OutTime,
                            Remarks = model.Remarks,
                            Employee_Code = objEmp.Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_AttRequest.Add(objrsgn);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Attendance request sent successfully !";
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult LeaveRequest()
        {
            ViewData["List_LeaveCat"] = AllLeaveCat();
            return View();
        }


        public List<SelectListItem> AllLeaveCat()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                var Result = context.tbl_Settings_AssignLeave.Where(w => w.IsActive == true && w.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && w.FK_DepartmentID == objEmp.DepartmentID).ToList();
                foreach (var item in Result)
                {
                    objSelLst.Add(new SelectListItem { Text = item.Type, Value = item.Type });
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
            }
            return objSelLst;
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult LeaveRequest(Emp_Leave model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    Emp_Details objEmp = objglobal.GetEmployeeDetails();
                    if (context.tbl_Emp_Leave.Any(x => (x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.LeaveFrom == model.LeaveFrom && x.LeaveTo == model.LeaveTo && x.LeaveDays == model.LeaveDays && x.IsApproved == null) || (x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.LeaveFrom == model.LeaveFrom && x.LeaveTo == model.LeaveTo && x.LeaveDays == model.LeaveDays && x.IsApproved == true)))
                    {
                        ViewData["List_LeaveCat"] = AllLeaveCat();
                        ViewBag.ErrorMsg = "Leave already applied for selected date !";
                    }
                    else
                    {
                        tbl_Emp_Leave objlv = new tbl_Emp_Leave()
                        {
                            LeaveFrom = model.LeaveFrom,
                            LeaveTo = model.LeaveTo,
                            LeaveDays = model.LeaveDays,
                            AppliedFor = model.AppliedFor,
                            Reason = model.Reason,
                            Employee_Code = objEmp.Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Leave.Add(objlv);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Leave request sent successfully !";
                    }
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    ViewData["List_LeaveCat"] = AllLeaveCat();
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["List_LeaveCat"] = AllLeaveCat();
                return View(model);
            }
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult View_LeaveRequest()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_Leave> EmpLList = new List<Emp_Leave>();
            try
            {
                EmpLList = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).Select(s => new Emp_Leave
                {
                    ID = s.ID,
                    Employee_Code = s.Employee_Code,
                    LeaveFrom = s.LeaveFrom,
                    LeaveTo = s.LeaveTo,
                    LeaveDays = s.LeaveDays,
                    AppliedFor = s.AppliedFor,
                    Reason = s.Reason,
                    IsApproved = s.IsApproved,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpLList);
        }



        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult View_ResignStatus()
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            List<Emp_Resign> EmpRList = new List<Emp_Resign>();
            try
            {
                EmpRList = context.tbl_Emp_Resign.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).Select(s => new Emp_Resign
                {
                    ResignID = s.ResignID,
                    CreatedOn = s.CreatedOn,
                    NoticePeriod = s.NoticePeriod,
                    IsAccepted = s.IsAccepted,
                    UpdatedBy = s.UpdatedBy,
                    UpdatedOn = s.UpdatedOn,
                    ResignDate = s.ResignDate,
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return View(EmpRList);
        }

        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult ResignRequest()
        {
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult ResignRequest(Emp_Resign model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    Emp_Details objEmp = objglobal.GetEmployeeDetails();
                    if (context.tbl_Emp_Resign.Any(x => (x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code) && x.IsAccepted == null || x.IsAccepted == true))
                    {
                        ViewBag.ErrorMsg = "Resign request already sent !";
                    }
                    else
                    {
                        tbl_Emp_Resign objrsgn = new tbl_Emp_Resign()
                        {
                            Reason = model.Reason,
                            Name = objEmp.Name,
                            Email = objEmp.Email,
                            Number = objEmp.Number,
                            Department = objEmp.Department,
                            Designation = objEmp.Category,
                            NoticePeriod = context.tbl_JobLeavePolicy.Where(m => m.IsActive == true && m.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && m.FK_DepartmentID == objEmp.DepartmentID).FirstOrDefault().NoticePeriod,
                            Employee_Code = objEmp.Employee_Code,
                            FK_CompanyRegistrationID = objSubs.FK_CompanyRegistrationID,
                            IsActive = true,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                            CreatedBy = objSubs.Username
                        };
                        context.tbl_Emp_Resign.Add(objrsgn);
                        context.SaveChanges();
                        ModelState.Clear();
                        ViewBag.SuccessMsg = "Resign request sent successfully !";
                    }
                    return View(model);
                }
                else
                {
                    ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                return View(model);
            }
        }



        public PartialViewResult Salary_Slip_Components()
        {

            List<SalaryComponents_EarningAndDeduction> Salary_Component_List = new List<SalaryComponents_EarningAndDeduction>();
            return PartialView(Salary_Component_List);
        }
        [HttpPost]
        public PartialViewResult Salary_Slip_Components(EMP_Print_Payslip model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<SalaryComponents_EarningAndDeduction> Temp_Component_List = new List<SalaryComponents_EarningAndDeduction>();
            List<SalaryComponents_EarningAndDeduction> Salary_Component_List = new List<SalaryComponents_EarningAndDeduction>();
            List<TrackAtt> track_List = new List<TrackAtt>();
            List<PaySlipTemplate> Salary_Cmp_List = new List<PaySlipTemplate>();

            try
            {
                // eMPLOYEE sALRY dETAILS
                var EmpSallary = context.tbl_Emp_Salary.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault();
                // pAYROLL sETTING dETAILS
                var PayrollSettings = context.tbl_PayrollSettings.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).FirstOrDefault();
                // Get Employee Attendance Details
                var month = Convert.ToInt32(model.Month);
                var year = Convert.ToInt32(model.Year);
                track_List = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code && x.Date.Value.Month == month && x.Date.Value.Year == year).Select(s => new TrackAtt
                {
                    Employee_Code = s.Employee_Code,
                    Date = s.Date,
                    InTime = s.InTime,
                    OutTime = s.OutTime,
                    WorkingHours = s.WorkingHours,
                }).ToList();
                // Employee All Salary Components List for ComponentID
                Salary_Cmp_List = context.tbl_PayslipTemplate.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.PackageID == EmpSallary.FK_PackageID).Select(s => new PaySlipTemplate
                {
                    FK_TypeID = s.FK_TypeID,
                    ComponentType = s.ComponentType,
                }).ToList();
                // Per day Salary of the Employee
                int DaylySalary = (Convert.ToInt32(EmpSallary.Monthly) / Convert.ToInt32(PayrollSettings.MonthlyWorkingDays));
                // Monthly Salary of the Employee for this month
                int MonthlySalary = DaylySalary * track_List.Count();
                // Salary Components of the Employee for the Month
                foreach (var item in Salary_Cmp_List)
                {
                    // Percentage of the component
                    int prcntg = Convert.ToInt32(context.tbl_SlaryComponents.Where(x => x.TypeID == item.FK_TypeID && x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => s.Percentage).FirstOrDefault());
                    Temp_Component_List = context.tbl_SlaryComponents.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.TypeID == item.FK_TypeID).Select(s => new SalaryComponents_EarningAndDeduction
                    {
                        ComponentType = s.ComponentType,
                        Component = s.TypeName,
                        Amount = (MonthlySalary * prcntg) / 100,
                    }).ToList();
                    Salary_Component_List.AddRange(Temp_Component_List);

                    foreach (var var in Temp_Component_List)
                    {
                        // Total Deduction of the Employee of this month
                        if (var.ComponentType == "Deduction")
                        {
                            model.TotalDeduction += Convert.ToInt32(var.Amount);
                        }
                        else
                        {
                            // Total Earnings of the Employee of this month
                            int TE = Convert.ToInt32(var.Amount);
                            model.TotalEarnings += TE;
                        }
                    }
                }
                // Add value to Model
                model.NetAmount = model.TotalEarnings - model.TotalDeduction;
                model.AmountInWords = objglobal.NumberToWords(model.NetAmount) + " ONLY.";
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Salary_Component_List);
        }
        public PartialViewResult Track_Leave_Holidays(Track_Leave model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            Emp_Details objEmp = objglobal.GetEmployeeDetails();
            var year = objglobal.TimeZone(objEmp.TimeZone).Year;
            List<LeaveBalance> Bln_List = new List<LeaveBalance>();

            //var year = Convert.ToInt32(model.Year);
            var DepartmentID = context.tbl_Emp_Joining_Probation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code).FirstOrDefault().FK_DepartmentID;
            try
            {
                Bln_List = context.tbl_Settings_AssignLeave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.IsActive == true && x.FK_DepartmentID == objEmp.DepartmentID).Select(s => new LeaveBalance
                {
                    AppliedFor = s.Type,
                    LeaveAllowed = context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.FK_DepartmentID == DepartmentID && x.Type == s.Type).FirstOrDefault().NoOfLeave ?? 0,
                    LeaveTaken = context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == s.Type).Sum(x => x.LeaveDays ?? 0),
                    LeaveDue = (context.tbl_Settings_AssignLeave.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.FK_DepartmentID == DepartmentID && x.Type == s.Type).FirstOrDefault().NoOfLeave ?? 0) - (context.tbl_Emp_Leave.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == objEmp.Employee_Code && x.CreatedOn.Value.Year == year && x.IsApproved == true && x.AppliedFor == s.Type).Sum(x => x.LeaveDays ?? 0)),
                }).ToList();
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewBag.ErrorMsg = "Something went wrong !";
            }
            return PartialView(Bln_List);
        }
        //******************************************************************


        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult Payslip()
        {
            ViewData["Code_List_Employee"] = objHRPayrollController.AllEmployeeCodeList();
            return View();
        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult Payslip_Print(EMP_Print_Payslip model)
        {
            try
            {

                model.FK_Employee_Code = objglobal.GetEmployeeDetails().Employee_Code;
                if (ModelState.IsValid)
                {
                    SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
                    Emp_Details objEmp = objglobal.GetEmployeeDetails();
                    if (context.tbl_ProcessSalary.Any(x => x.Month == model.Month && x.Year == model.Year && x.FK_Employee_Code == model.FK_Employee_Code))
                    {
                        var m = model.Month; var y = model.Year;
                        var EmpDetails = context.tbl_Emp_Contact_Basic.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.FK_Employee_Code).FirstOrDefault();
                        model = context.tbl_CompanyProfile.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(s => new EMP_Print_Payslip
                        {
                            Month = m,
                            Year = y,
                            FK_Employee_Code = EmpDetails.Employee_Code,
                            SalarySlipID = System.Guid.NewGuid().ToString().Substring(0, 6).ToUpper(),
                            BannerImagePath = s.BannerImagePath,
                            CompanyName = s.CompanyName,
                            CompanyAddress = context.tbl_CompanyLocation.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID).Select(ls => ls.OfficeAddress).FirstOrDefault(),
                            CompanyWebsite = s.CompanyWebsite,
                            EmployeeName = EmpDetails.Name,
                            EmployeeEmail = EmpDetails.Email,
                            EmployeeNumber = EmpDetails.Number,
                            CreatedOn = objglobal.TimeZone(objEmp.TimeZone),
                        }).FirstOrDefault();
                        ViewData["Code_List_Employee"] = objHRPayrollController.AllEmployeeCodeList();
                        Session["ErrorMsg"] = null;
                        return View(model);
                        //return RedirectToAction("Salary_Slip_Print", "HRPayroll", new { area = "HR" });
                    }
                    else
                    {
                        ViewData["Code_List_Employee"] = objHRPayrollController.AllEmployeeCodeList();
                        Session["ErrorMsg"] = "Salary not processed for those combination !";
                        return RedirectToAction("Payslip");

                    }
                }
                else
                {
                    ViewData["Code_List_Employee"] = objHRPayrollController.AllEmployeeCodeList();
                    Session["ErrorMsg"] = "Required fields cannot be blank or Please enter valid credentials !";
                    return RedirectToAction("Payslip");

                }
            }
            catch (Exception ex)
            {
                objError.LogError(ex);
                ViewData["Code_List_Employee"] = objHRPayrollController.AllEmployeeCodeList();
                return RedirectToAction("Payslip");
            }

        }
        [SessionAuthorizeFilter((int)AppsConstants.UserRole.Employee)]
        public ActionResult AttendanceHistory()
        {

            //   ViewData["Code_List_Employee"] = objHRPayrollController. AllEmployeeCodeList();
            return View();
        }
        public PartialViewResult Track_Attendance_View()
        {
            List<Emp_TrackAtt> track_List = new List<Emp_TrackAtt>();
            return PartialView(track_List);
        }

        [HttpPost]
        public PartialViewResult Track_Attendance_View(Emp_TrackAtt model)
        {
            SubscriptionModel objSubs = objglobal.GetSubscriptionDetails();
            List<Emp_TrackAtt> track_List = new List<Emp_TrackAtt>();
            model.Employee_Code = objglobal.GetEmployeeDetails().Employee_Code;
            if (ModelState.IsValid)
            {
                var month = Convert.ToInt32(model.Month);
                var year = Convert.ToInt32(model.Year);
                try
                {
                    track_List = context.tbl_Emp_Attendance.Where(x => x.FK_CompanyRegistrationID == objSubs.FK_CompanyRegistrationID && x.Employee_Code == model.Employee_Code && x.Date.Value.Month == month && x.Date.Value.Year == year).Select(s => new Emp_TrackAtt
                    {
                        Employee_Code = s.Employee_Code,
                        Date = s.Date,
                        InTime = s.InTime,
                        OutTime = s.OutTime,
                        WorkingHours = s.WorkingHours,
                    }).ToList();
                    if (track_List.Count <= 0)
                    {
                        ViewBag.ErrorMsg = "No Record Found !";
                    }
                }
                catch (Exception ex)
                {
                    objError.LogError(ex);
                    ViewBag.ErrorMsg = "Something went wrong !";
                }
                return PartialView(track_List);
            }
            else
            {
                ViewBag.ErrorMsg = "Required fields cannot be blank or Please enter valid credentials !";
                return PartialView(track_List);
            }
        }
        //******************************************************************

    }

}