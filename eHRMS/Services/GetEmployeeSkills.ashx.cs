﻿using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;



namespace eHRMS.Services
{
    /// <summary>
    /// Summary description for GetCompanySkills
    /// </summary>
    public class GetEmployeeSkills : IHttpHandler
    {
        GlobalFunction objglobal = new GlobalFunction();
        eHRMSEntities dbcontext = new eHRMSEntities();
        public void ProcessRequest(HttpContext context)
        {
            List<JobSkill> SkillsList = new List<JobSkill>();
            var CompanyRegistrationID = context.Request.QueryString["CompanyRegistrationID"].ToString();
            if (CompanyRegistrationID != null)
            {
                SkillsList = dbcontext.tbl_JobSkill.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == CompanyRegistrationID)
                    .Select(s => new JobSkill
                    {
                        SkillID = s.SkillID,
                        Type = s.Type,
                    }).ToList();

                string Employee_Code = HttpContext.Current.Session["employeCode"].ToString();
                var collection = dbcontext.tbl_Emp_Skill_Employment.FirstOrDefault(x =>
                    x.IsActive == true &&
                    x.FK_CompanyRegistrationID == CompanyRegistrationID &&
                    x.Employee_Code == Employee_Code);


            }
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objglobal.JsonSerialize(SkillsList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}