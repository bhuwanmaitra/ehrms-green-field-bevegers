﻿using eHRMS.Areas.HR.Models;
using eHRMS.Global;
using eHRMS.Models;
using eHRMS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace eHRMS.Services
{
    /// <summary>
    /// Summary description for GetEarnComponent
    /// </summary>
    public class GetEarnComponent : IHttpHandler
    {
        GlobalFunction objglobal = new GlobalFunction();
        
        eHRMSEntities dbcontext = new eHRMSEntities();
        public void ProcessRequest(HttpContext context)
        {
            List<SalaryComponents> EarnList = new List<SalaryComponents>();
            var CompanyRegistrationID = context.Request.QueryString["CompanyRegistrationID"].ToString();
            var PackageID = context.Request.QueryString["PackageID"];
            if (CompanyRegistrationID != null)
            {
                EarnList = dbcontext.tbl_SlaryComponents.Where(x => x.IsActive == true && x.FK_CompanyRegistrationID == CompanyRegistrationID && x.ComponentType == "Earning")
                    .Select(s => new SalaryComponents
                    {
                        TypeID = s.TypeID,
                        TypeName = s.TypeName
                    }).ToList();

                if (PackageID != null)
                {
                    string sql = "select * from tbl_SlaryComponents where TypeID in(  select FK_TypeID from tbl_PayslipTemplate where PackageID=" + PackageID + " and IsActive=1 ) and tbl_SlaryComponents.IsActive=1 and tbl_SlaryComponents.ComponentType='Earning'";
                    var result = dbcontext.Database.SqlQuery<SalaryComponents>(sql);
                    foreach (var itemDeduct in EarnList)
                    {
                        foreach (var itemResult in result)
                        {
                            if (itemDeduct.TypeID == itemResult.TypeID)
                            {
                                itemDeduct.CreatedBy = "Checked";
                            }
                        }
                    }
                }
            }
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objglobal.JsonSerialize(EarnList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}