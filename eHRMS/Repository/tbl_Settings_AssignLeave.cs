//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eHRMS.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Settings_AssignLeave
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public Nullable<int> NoOfLeave { get; set; }
        public Nullable<int> FK_DepartmentID { get; set; }
        public string Department { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual tbl_CompanyRegistration tbl_CompanyRegistration { get; set; }
        public virtual tbl_JobDepartment tbl_JobDepartment { get; set; }
    }
}
