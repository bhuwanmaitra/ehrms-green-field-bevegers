﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eHRMS.Repository
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class eHRMSEntities : DbContext
    {
        public eHRMSEntities()
            : base("name=eHRMSEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_CompanyAssets> tbl_CompanyAssets { get; set; }
        public virtual DbSet<tbl_CompanyBank> tbl_CompanyBank { get; set; }
        public virtual DbSet<tbl_CompanyLocation> tbl_CompanyLocation { get; set; }
        public virtual DbSet<tbl_CompanyProfile> tbl_CompanyProfile { get; set; }
        public virtual DbSet<tbl_CompanyProject> tbl_CompanyProject { get; set; }
        public virtual DbSet<tbl_CompanyRegistration> tbl_CompanyRegistration { get; set; }
        public virtual DbSet<tbl_CompanyStatutory> tbl_CompanyStatutory { get; set; }
        public virtual DbSet<tbl_ContractualPolicy> tbl_ContractualPolicy { get; set; }
        public virtual DbSet<tbl_EmailSettings> tbl_EmailSettings { get; set; }
        public virtual DbSet<tbl_EmailTemplate> tbl_EmailTemplate { get; set; }
        public virtual DbSet<tbl_Emp_AssignTask> tbl_Emp_AssignTask { get; set; }
        public virtual DbSet<tbl_Emp_Attendance> tbl_Emp_Attendance { get; set; }
        public virtual DbSet<tbl_Emp_AttRequest> tbl_Emp_AttRequest { get; set; }
        public virtual DbSet<tbl_Emp_BankAccount> tbl_Emp_BankAccount { get; set; }
        public virtual DbSet<tbl_Emp_Contact_Basic> tbl_Emp_Contact_Basic { get; set; }
        public virtual DbSet<tbl_Emp_Documents> tbl_Emp_Documents { get; set; }
        public virtual DbSet<tbl_Emp_HouseRent> tbl_Emp_HouseRent { get; set; }
        public virtual DbSet<tbl_Emp_Joining_Probation> tbl_Emp_Joining_Probation { get; set; }
        public virtual DbSet<tbl_Emp_NewTask> tbl_Emp_NewTask { get; set; }
        public virtual DbSet<tbl_Emp_Qual_Employment> tbl_Emp_Qual_Employment { get; set; }
        public virtual DbSet<tbl_Emp_Resign> tbl_Emp_Resign { get; set; }
        public virtual DbSet<tbl_Emp_Salary> tbl_Emp_Salary { get; set; }
        public virtual DbSet<tbl_Emp_Skill_Employment> tbl_Emp_Skill_Employment { get; set; }
        public virtual DbSet<tbl_Emp_Task_Progress> tbl_Emp_Task_Progress { get; set; }
        public virtual DbSet<tbl_Emp_Visa_Passport> tbl_Emp_Visa_Passport { get; set; }
        public virtual DbSet<tbl_Employee_Investment> tbl_Employee_Investment { get; set; }
        public virtual DbSet<tbl_EmpSocialLink> tbl_EmpSocialLink { get; set; }
        public virtual DbSet<tbl_ExceptionErrors> tbl_ExceptionErrors { get; set; }
        public virtual DbSet<tbl_JobContractPolicy> tbl_JobContractPolicy { get; set; }
        public virtual DbSet<tbl_JobDepartment> tbl_JobDepartment { get; set; }
        public virtual DbSet<tbl_JobDestination> tbl_JobDestination { get; set; }
        public virtual DbSet<tbl_JobDocument> tbl_JobDocument { get; set; }
        public virtual DbSet<tbl_JobEmployeeCode> tbl_JobEmployeeCode { get; set; }
        public virtual DbSet<tbl_JobGrade> tbl_JobGrade { get; set; }
        public virtual DbSet<tbl_JobLanguage> tbl_JobLanguage { get; set; }
        public virtual DbSet<tbl_JobLeavePolicy> tbl_JobLeavePolicy { get; set; }
        public virtual DbSet<tbl_JobProbationPolicy> tbl_JobProbationPolicy { get; set; }
        public virtual DbSet<tbl_JobQualification> tbl_JobQualification { get; set; }
        public virtual DbSet<tbl_JobSkill> tbl_JobSkill { get; set; }
        public virtual DbSet<tbl_Login> tbl_Login { get; set; }
        public virtual DbSet<tbl_LoginContact> tbl_LoginContact { get; set; }
        public virtual DbSet<tbl_PayrollSettings> tbl_PayrollSettings { get; set; }
        public virtual DbSet<tbl_PayslipTemplate> tbl_PayslipTemplate { get; set; }
        public virtual DbSet<tbl_PreviousEmployee_TAX> tbl_PreviousEmployee_TAX { get; set; }
        public virtual DbSet<tbl_Settings_AssignLeave> tbl_Settings_AssignLeave { get; set; }
        public virtual DbSet<tbl_Settings_AttLeave> tbl_Settings_AttLeave { get; set; }
        public virtual DbSet<tbl_SettingsHolidays> tbl_SettingsHolidays { get; set; }
        public virtual DbSet<tbl_SettingsOffDays> tbl_SettingsOffDays { get; set; }
        public virtual DbSet<tbl_SlabMaster> tbl_SlabMaster { get; set; }
        public virtual DbSet<tbl_SlaryComponents> tbl_SlaryComponents { get; set; }
        public virtual DbSet<tbl_Subscription> tbl_Subscription { get; set; }
        public virtual DbSet<tbl_TAXComputation> tbl_TAXComputation { get; set; }
        public virtual DbSet<tbl_UserDetails> tbl_UserDetails { get; set; }
        public virtual DbSet<tbl_WorkShift> tbl_WorkShift { get; set; }
        public virtual DbSet<tbl_JobCategory> tbl_JobCategory { get; set; }
        public virtual DbSet<tbl_Emp_Leave> tbl_Emp_Leave { get; set; }
        public virtual DbSet<tbl_Leave_Category> tbl_Leave_Category { get; set; }
        public virtual DbSet<tbl_ProcessSalary> tbl_ProcessSalary { get; set; }
    
        public virtual ObjectResult<usp_getAnniversary_Result> usp_getAnniversary(string companyRegistrationID)
        {
            var companyRegistrationIDParameter = companyRegistrationID != null ?
                new ObjectParameter("CompanyRegistrationID", companyRegistrationID) :
                new ObjectParameter("CompanyRegistrationID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_getAnniversary_Result>("usp_getAnniversary", companyRegistrationIDParameter);
        }
    
        public virtual ObjectResult<string> usp_getBirthday(string companyRegistrationID)
        {
            var companyRegistrationIDParameter = companyRegistrationID != null ?
                new ObjectParameter("CompanyRegistrationID", companyRegistrationID) :
                new ObjectParameter("CompanyRegistrationID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("usp_getBirthday", companyRegistrationIDParameter);
        }
    }
}
