//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eHRMS.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Emp_AssignTask
    {
        public tbl_Emp_AssignTask()
        {
            this.tbl_Emp_Task_Progress = new HashSet<tbl_Emp_Task_Progress>();
        }
    
        public long AssignTaskId { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public string TaskName { get; set; }
        public string Status { get; set; }
        public string AssignBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual tbl_CompanyRegistration tbl_CompanyRegistration { get; set; }
        public virtual ICollection<tbl_Emp_Task_Progress> tbl_Emp_Task_Progress { get; set; }
    }
}
