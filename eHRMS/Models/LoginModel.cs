﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Models
{
    public class LoginModel
    {
        public string LoginID { get; set; }
        public string ContactPerson { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public int UserRole { get; set; }
        public bool IsActive { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
    }
}