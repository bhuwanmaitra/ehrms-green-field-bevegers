﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Models
{
    public class SubscriptionModel
    {
      public string Username { get; set; }
      public string CompanyName { get; set; }
      public Nullable<int> UserRole { get; set; }
      public Nullable<int> UserLimit { get; set; }
      public Nullable<int> SubscriptionID { get; set; }
      public Nullable<System.DateTime> ActivatedOn { get; set; }
      public Nullable<System.DateTime> ExpiredDate { get; set; }
      public Nullable<int> NoOfUser {get;set;}
      public Nullable<int> SubscriptionType { get; set; }
      public string FK_CompanyRegistrationID {get;set;}
      public Nullable<bool> IsActive { get; set; }
    }
}