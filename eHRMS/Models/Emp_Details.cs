﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eHRMS.Models
{
    public class Emp_Details
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> Number { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Sex { get; set; }
        public string UserImagePath { get; set; }
        public string Employee_Code { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
        public Nullable<System.DateTime> JoiningDate { get; set; }
        public Nullable<System.DateTime> ResignDate { get; set; }
        public Nullable<int> FK_ProbationPolicyID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public string Department { get; set; }
        public string Category { get; set; }
        public string Grade { get; set; }
        public string CTC { get; set; }
        public string Skills { get; set; }
        public Nullable<int> OfficeLocationID { get; set; }
        public string TimeZone { get; set; }
        //public Nullable<int> NoOfMadicalLeave { get; set; }
        //public string CanApplyMadicalLeave { get; set; }
        //public Nullable<int> NoOfCasualLeave { get; set; }
        //public string CanApplyCasualLeave { get; set; }
        public Nullable<int> ResignPolicyID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string facebooklink { get; set; }
        public string twitterlink { get; set; }
        public string linkedinlink { get; set; }
        public string googlelink { get; set; }
        public bool?  IsAppraiser { get; set; }

    }
}