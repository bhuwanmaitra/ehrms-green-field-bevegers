﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Models
{
    public class ForgotPassword
    {
        public string ContactPerson { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string FK_CompanyRegistrationID { get; set; }
    }
}