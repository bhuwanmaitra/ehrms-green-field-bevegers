﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eHRMS.Models
{   

    public class CompanyRegistration
    {        
        public string CompanyRegistrationID { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Confirm_Password { get; set; }
        [Required]
        public bool IsAgree { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

    }
}